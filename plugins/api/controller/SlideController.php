<?php

namespace plugins\api\controller;

use app\model\Slide;

/**
 * 幻灯片
 */
class SlideController extends BaseController
{
    public function index()
    {
        $where = [
            'seller_id' => $this->sellerId,
            'slide_cate_id' => (int)input('slide_cate_id'),
        ];
        $slide = new Slide();
        $slideList = $slide->getAllCustomData($where,'sort desc','*',['attachment'=>function($q){
            $q->field('id,name,url,type');
        }]);
        return json($slideList);
    }
}