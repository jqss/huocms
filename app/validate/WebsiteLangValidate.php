<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class WebsiteLangValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'lang|语言' => 'require|array',
        'site_id|网站ID' => 'require|number'
    ];

}
