SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for $prefix$admin
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$admin`;
CREATE TABLE `$prefix$admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `seller_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '商户ID',
  `group` tinyint(1) DEFAULT '2' COMMENT '管理员分组 super- admin 和 admin',
  `name` varchar(40) DEFAULT '' COMMENT '管理员名称',
  `account` varchar(80) DEFAULT '' COMMENT '登录账号',
  `last_login_time` int(10) unsigned DEFAULT NULL COMMENT '最后登录时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1 正常 2 禁用',
  `password` varchar(255) DEFAULT '' COMMENT '密码',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(10) unsigned DEFAULT NULL,
  `create_time` int(10) unsigned DEFAULT NULL,
  `update_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of $prefix$admin
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$admin_login_log
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$admin_login_log`;
CREATE TABLE `$prefix$admin_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '登录日志ID',
  `seller_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(80) DEFAULT '' COMMENT '内容',
  `admin_id` int(11) DEFAULT NULL COMMENT '管理员ID',
  `admin_name` varchar(120) DEFAULT '' COMMENT '管理员名称',
  `ip` varchar(80) DEFAULT NULL COMMENT '登录IP',
  `addr` varchar(80) DEFAULT '' COMMENT '登陆地点',
  `login_time` int(11) DEFAULT NULL COMMENT '登录时间',
  `agent` varchar(255) DEFAULT '' COMMENT '客户端',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='登录日志';

-- ----------------------------
-- Records of $prefix$admin_login_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$admin_menu`;
CREATE TABLE `$prefix$admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '后台菜单ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '菜单类型;1:有界面可访问菜单,2:无界面可访问菜单,3:只作为菜单',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:正常,2:禁用',
  `sort` smallint(6) NOT NULL DEFAULT '10000' COMMENT '排序',
  `controller` varchar(120) NOT NULL DEFAULT '' COMMENT '控制器名',
  `action` varchar(120) NOT NULL DEFAULT '' COMMENT '操作名称',
  `param` varchar(120) NOT NULL DEFAULT '' COMMENT '额外参数',
  `path` varchar(255) DEFAULT '' COMMENT '访问地址',
  `title` varchar(120) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(120) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `is_menu` tinyint(3) unsigned DEFAULT '1',
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `controller` (`controller`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COMMENT='后台菜单表';

-- ----------------------------
-- Records of $prefix$admin_menu
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (1, 1, 220, 1, 1, 30, '', '', '', '', '系统管理', 'icon-xitongshezhi', '', 1, 1, 0, 1633486619, 1633486619);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (2, 1, 0, 2, 1, 1, '', '', '', '', '控制面板', 'icon-kongzhimianban', '', 1, 1, 0, 1633486674, 1633486674);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (3, 1, 1, 2, 1, 12, 'SysSetting', 'index', '', '/SysSetting/index', '系统设置', 'icon-xitongshezhi', '', 1, 1, 0, 1633486794, 1633486794);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (4, 1, 3, 1, 1, 31, 'SysSetting', 'update', '', '/SysSetting/update', '保存设置', '', '', 2, 1, 0, 1633486902, 1633486902);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (7, 1, 1, 1, 1, 13, '', '', '', '', '管理员管理', 'icon-guanliyuanquanxian', '', 2, 1, 0, 1633487076, 1633487076);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (8, 1, 1, 2, 1, 13, 'Admin', 'index', '', '/Admin/index', '管理员管理', '', '', 1, 1, 0, 1633487110, 1633487110);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (9, 1, 8, 1, 1, 81, 'Admin', 'save', '', '/Admin/save', '管理员添加', '', '', 2, 1, 0, 1633487146, 1633487146);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (10, 1, 8, 1, 1, 82, 'Admin', 'update', '', '/Admin/update', '管理员编辑', '', '', 2, 1, 0, 1633487161, 1633487161);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (11, 1, 8, 1, 1, 83, 'Admin', 'delete', '', '/Admin/delete', '管理员删除', '', '', 2, 1, 0, 1633487171, 1633487171);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (12, 1, 1, 2, 1, 14, 'Role', 'index', '', '/Role/index', '角色管理', '', '', 1, 1, 0, 1633487327, 1633487327);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (13, 1, 12, 1, 1, 121, 'Role', 'save', '', '/Role/save', '角色添加', '', '', 2, 1, 0, 1633487354, 1633487354);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (14, 1, 12, 1, 1, 122, 'Role', 'update', '', '/Role/update', '角色编辑', '', '', 2, 1, 0, 1633487373, 1633487373);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (15, 1, 12, 1, 1, 123, 'Role', 'delete', '', '/Role/delete', '角色删除', '', '', 2, 1, 0, 1633487391, 1633487391);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (16, 1, 8, 1, 1, 84, 'Admin', 'read', '', '/Admin/read', '管理员详情', '', '', 2, 1, 0, 1633487423, 1633487423);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (17, 1, 144, 1, 1, 1445, 'Attachment', 'index', '', '/Attachment/index', '附件列表', 'icon-fujianguanli', '', 2, 1, 0, 1633487558, 1633487558);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (18, 1, 144, 1, 1, 1446, 'Attachment', 'save', '', '/Attachment/save', '添加网络附件', '', '', 2, 1, 0, 1633487586, 1633487586);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (19, 1, 144, 1, 1, 1447, 'Attachment', 'delete', '', '/Attachment/delete', '附件删除', '', '', 2, 1, 0, 1633487603, 1633487603);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (20, 1, 1, 1, 1, 150, '', '', '', '//', '日志管理', 'icon-rizhiguanli', '', 1, 1, 0, 1633487634, 1633487634);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (21, 1, 20, 2, 1, 201, 'AdminLoginLog', 'index', '', '/AdminLoginLog/index', '登录日志', '', '', 1, 1, 0, 1633487666, 1633487666);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (22, 1, 20, 2, 1, 202, 'AdminOptLog', 'index', '', '/AdminOptLog/index', '操作日志', '', '', 1, 1, 0, 1633487695, 1633487695);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (23, 1, 1, 2, 1, 16, 'RecycleBin', 'index', '', '/RecycleBin/index', '回收站管理', 'icon-huishouzhanguanli', '', 1, 1, 0, 1633487783, 1633487783);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (24, 1, 23, 1, 1, 231, 'RecycleBin', 'clean', '', '/RecycleBin/clean', '回收站清空', '', '', 2, 1, 0, 1633487824, 1633487824);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (25, 1, 23, 1, 1, 232, 'RecycleBin', 'delete', '', '/RecycleBin/delete', '回收站删除', '', '', 2, 1, 0, 1633487841, 1633487841);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (26, 1, 23, 1, 1, 233, 'RecycleBin', 'restore', '', '/RecycleBin/restore', '单条数据恢复', '', '', 2, 1, 0, 1633487920, 1633487920);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (27, 1, 23, 1, 1, 234, 'RecycleBin', 'allRestore', '', '/RecycleBin/allRestore', '多条数据恢复', '', '', 2, 1, 0, 1633487940, 1633487940);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (28, 1, 1, 2, 1, 17, 'Database', 'index', '', '/Database/index', '数据库备份', 'icon-shujukubeifen', '', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (29, 1, 28, 1, 1, 281, 'Database', 'backup', '', '/Database/backup', '备份数据库', '', '', 2, 1, 0, 1633488085, 1633488085);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (30, 1, 28, 1, 1, 282, 'Database', 'restore', '', '/Database/restore', '数据库还原', '', '', 2, 1, 0, 1633488174, 1633488174);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (31, 1, 28, 1, 1, 283, 'Database', 'delete', '', '/Database/delete', '备份文件删除', '', '', 2, 1, 0, 1633488191, 1633488191);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (32, 1, 218, 1, 1, 20, '', '', '', '//', '网站管理', 'icon-wangzhanguanli', '', 1, 1, 0, 1633488240, 1633488240);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (33, 1, 32, 2, 1, 321, 'Website', 'index', '', '/Website/index', '网站列表', 'icon-zhandianliebiao', '', 1, 1, 0, 1633488270, 1633488270);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (34, 1, 33, 1, 1, 331, 'Website', 'save', '', '/Website/save', '网站添加', '', '', 2, 1, 0, 1633488296, 1633488296);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (35, 1, 33, 1, 1, 332, 'Website', 'update', '', '/Website/update', '网站编辑', '', '', 2, 1, 0, 1633488317, 1633488317);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (36, 1, 33, 1, 1, 333, 'Website', 'delete', '', '/Website/delete', '网站删除', '', '', 2, 1, 0, 1633488416, 1633488416);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (37, 1, 32, 2, 1, 322, 'WebsiteSetting', 'index', '', '/WebsiteSetting/index', '网站设置', 'icon-wangzhanjibenshezhi', '', 1, 1, 0, 1633488480, 1633488480);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (38, 1, 37, 1, 1, 371, 'WebsiteSetting', 'update', '', '/WebsiteSetting/update', '基本设置保存', '', '', 2, 1, 0, 1633488542, 1633488542);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (39, 1, 37, 1, 1, 372, 'WebsiteLang', 'save', '', '/WebsiteLang/save', '语言设置保存', '', '', 2, 1, 0, 1633488558, 1633488558);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (40, 1, 37, 1, 1, 373, 'websiteServer', 'update', '', '/websiteServer/update', '服务器设置保存', '', '', 2, 1, 0, 1633488569, 1633488569);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (41, 1, 32, 1, 1, 327, '', '', '', '//', '模版管理', 'icon-wangzhanmobanguanli', '', 2, 2, 0, 1633488609, 1633488609);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (42, 1, 32, 2, 1, 411, 'TempleteLibrary', 'index', '', '/TempleteLibrary/index', '模版库', '', '', 1, 1, 0, 1633488696, 1633488696);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (43, 1, 32, 2, 1, 401, 'Theme', 'index', '', '/Theme/index', '模版管理', '', '', 1, 1, 0, 1633519385, 1633519385);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (44, 1, 43, 1, 2, 431, 'Theme', 'save', '', '/Theme/save', '模版下载', '', '', 2, 1, 0, 1633519412, 1633519412);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (45, 1, 43, 1, 1, 432, 'Theme', 'update', '', '/Theme/update', '模版更新', '', '', 2, 1, 0, 1633519424, 1633519424);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (46, 1, 43, 1, 1, 432, 'Theme', 'delete', '', '/Theme/delete', '模版删除', '', '', 2, 1, 0, 1633519439, 1633519439);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (47, 1, 43, 1, 1, 433, 'Theme', 'install', '', '/Theme/install', '系统模版', '', '', 2, 1, 0, 1633519520, 1633519520);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (48, 1, 43, 1, 1, 434, 'Theme', 'installTheme', '', '/Theme/installTheme', '模版安装', '', '', 2, 1, 0, 1633519564, 1633519564);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (49, 1, 43, 1, 1, 435, 'Theme', 'active', '', '/Theme/active', '模版启用', '', '', 2, 1, 0, 1633519627, 1633519627);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (50, 1, 43, 1, 1, 436, 'Theme', 'uninstall', '', '/Theme/uninstall', '模版卸载', '', '', 2, 1, 0, 1633519672, 1633519672);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (51, 1, 43, 1, 1, 437, 'Theme', 'allFilePath', '', '/Theme/allFilePath', '模版文件目录', '', '', 2, 1, 0, 1633520228, 1633520228);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (52, 1, 43, 1, 1, 438, 'Theme', 'allFiles', '', '/Theme/allFiles', '所有文件', '', '', 2, 1, 0, 1633520293, 1633520293);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (53, 1, 43, 1, 1, 439, 'Theme', 'tmpFile', '', '/Theme/tmpFile', '模版文件', '', '', 2, 1, 0, 1633520353, 1633520353);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (54, 1, 53, 1, 1, 531, 'ThemeFile', 'read', '', '/ThemeFile/read', '模版文件源码查看', '', '', 2, 1, 0, 1633520526, 1633520526);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (55, 1, 53, 1, 1, 532, 'ThemeFile', 'update', '', '/ThemeFile/update', '模版文件源码更新', '', '', 2, 1, 0, 1633520662, 1633520662);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (56, 1, 218, 1, 1, 30, '', '', '', '//', '内容管理', 'icon-neirongguanli', '', 1, 1, 0, 1633521143, 1633521143);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (57, 1, 56, 2, 1, 1561, 'Module', 'index', '', '/Module/index', '模型管理', 'icon-neirongguanli', '', 1, 1, 0, 1633521177, 1633521177);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (58, 1, 57, 1, 1, 571, 'Module', 'save', '', '/Module/save', '模型添加', '', '', 2, 1, 0, 1633521229, 1633521229);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (59, 1, 57, 1, 1, 572, 'Module', 'update', '', '/Module/update', '模型编辑', '', '', 2, 1, 0, 1633521245, 1633521245);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (60, 1, 57, 1, 1, 573, 'Module', 'delete', '', '/Module/delete', '模型删除', '', '', 2, 1, 0, 1633521264, 1633521264);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (61, 1, 57, 1, 1, 574, 'Module', 'read', '', '/Module/read', '模型详情', '', '', 2, 1, 0, 1633521275, 1633521275);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (62, 1, 56, 1, 1, 562, 'ModuleField', 'index', '', '/ModuleField/index', '模型字段', '', '', 2, 1, 0, 1633525422, 1633525422);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (63, 1, 62, 1, 1, 621, 'ModuleField', 'save', '', '/ModuleField/save', '模型字段添加', '', '', 2, 1, 0, 1633525442, 1633525442);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (64, 1, 62, 1, 1, 622, 'ModuleField', 'update', '', '/ModuleField/update', '模型字段编辑', '', '', 2, 1, 0, 1633525464, 1633525464);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (65, 1, 62, 1, 1, 623, 'ModuleField', 'delete', '', '/ModuleField/delete', '模型字段删除', '', '', 2, 1, 0, 1633525481, 1633525481);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (66, 1, 62, 1, 1, 624, 'ModuleField', 'read', '', '/ModuleField/read', '模型字段详情', '', '', 2, 1, 0, 1633525499, 1633525499);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (67, 1, 56, 2, 1, 563, 'Category', 'index', '', '/Category/index', '栏目管理', 'icon-neirongguanli', '', 1, 1, 0, 1633525698, 1633525698);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (68, 1, 67, 1, 1, 671, 'Category', 'save', '', '/Category/save', '栏目添加', '', '', 2, 1, 0, 1633525723, 1633525723);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (69, 1, 67, 1, 1, 672, 'Category', 'update', '', '/Category/update', '栏目编辑', '', '', 2, 1, 0, 1633525736, 1633525736);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (70, 1, 67, 1, 1, 673, 'Category', 'delete', '', '/Category/delete', '栏目删除', '', '', 2, 1, 0, 1633525786, 1633525786);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (71, 1, 133, 1, 1, 1331, 'Content', 'save', '', '/Content/save', '内容新增', '', '', 2, 1, 0, 1633526383, 1633526383);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (72, 1, 133, 1, 1, 1332, 'Content', 'update', '', '/Content/update', '内容编辑', '', '', 2, 1, 0, 1633526396, 1633526396);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (73, 1, 133, 1, 1, 1333, 'Content', 'delete', '', '/Content/delete', '内容删除', '', '', 2, 1, 0, 1633526428, 1633526428);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (74, 1, 56, 2, 1, 565, 'Tag', 'index', '', '/Tag/index', '标签管理', 'icon-neirongguanli', '', 1, 1, 0, 1633526488, 1633526488);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (75, 1, 74, 1, 1, 741, 'Tag', 'save', '', '/Tag/save', '标签添加', '', '', 2, 1, 0, 1633526511, 1633526511);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (76, 1, 74, 1, 1, 742, 'Tag', 'update', '', '/Tag/update', '标签编辑', '', '', 2, 1, 0, 1633526537, 1633526537);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (77, 1, 74, 1, 1, 743, 'Tag', 'delete', '', '/Tag/delete', '标签删除', '', '', 2, 1, 0, 1633526544, 1633526544);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (78, 1, 1, 1, 1, 10, 'Nav', 'index', '', '/Nav/index', '菜单列表', '', '', 1, 1, 0, 1633526626, 1633526626);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (79, 1, 78, 1, 1, 781, 'Nav', 'save', '', '/Nav/save', '菜单添加', '', '', 2, 1, 0, 1633526648, 1633526648);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (80, 1, 78, 1, 1, 782, 'Nav', 'update', '', '/Nav/update', '菜单编辑', '', '', 2, 1, 0, 1633526661, 1633526661);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (81, 1, 78, 1, 1, 783, 'Nav', 'delete', '', '/Nav/delete', '菜单删除', '', '', 2, 1, 0, 1633526675, 1633526675);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (82, 1, 32, 2, 1, 323, 'NavCate', 'index', '', '/NavCate/index', '前台菜单', 'icon-qiantaicaidanguanli', '', 1, 1, 0, 1633526770, 1633526770);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (83, 1, 82, 1, 1, 821, 'NavCate', 'save', '', '/NavCate/save', '分类添加', '', '', 2, 1, 0, 1633526884, 1633526884);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (84, 1, 82, 1, 1, 822, 'NavCate', 'update', '', '/NavCate/update', '分类编辑', '', '', 2, 1, 0, 1633526890, 1633526890);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (85, 1, 82, 1, 1, 823, 'NavCate', 'delete', '', '/NavCate/delete', '分类删除', '', '', 2, 1, 0, 1633526900, 1633526900);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (86, 1, 218, 1, 1, 40, '', '', '', '//', '询盘管理', 'icon-xunpanguanli', '', 1, 1, 0, 1633527118, 1633527118);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (87, 1, 86, 2, 1, 861, 'Inquiry', 'index', '', '/Inquiry/index', '询盘列表', 'icon-xunpanliebiao', '', 1, 1, 0, 1633527207, 1633527207);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (88, 1, 87, 1, 1, 871, 'Inquiry', 'update', '', '/Inquiry/update', '询盘更新', '', '', 2, 1, 0, 1633527303, 1633527303);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (89, 1, 87, 1, 1, 872, 'Inquiry', 'delete', '', '/Inquiry/delete', '询盘删除', '', '', 2, 1, 0, 1633527326, 1633527326);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (90, 1, 86, 2, 1, 862, 'InquiryCate', 'index', '', '/InquiryCate/index', '询盘分类', 'icon-xunpanfenlei', '', 1, 1, 0, 1633527908, 1633527908);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (91, 1, 90, 1, 1, 901, 'InquiryCate', 'save', '', '/InquiryCate/save', '询盘分类添加', '', '', 2, 1, 0, 1633527954, 1633527954);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (92, 1, 90, 1, 1, 902, 'InquiryCate', 'update', '', '/InquiryCate/update', '询盘分类编辑', '', '', 2, 1, 0, 1633528000, 1633528000);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (93, 1, 90, 1, 1, 903, 'InquiryCate', 'delete', '', '/InquiryCate/delete', '询盘分类删除', '', '', 2, 1, 0, 1633528023, 1633528023);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (94, 1, 86, 2, 1, 863, 'InquiryEmail', 'index', '', '/InquiryEmail/index', '询盘收件箱', 'icon-xunpanshoujianxiang', '', 1, 1, 0, 1633528118, 1633528118);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (95, 1, 94, 1, 1, 941, 'InquiryEmail', 'save', '', '/InquiryEmail/save', '询盘收件箱添加', '', '', 2, 1, 0, 1633528143, 1633528143);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (96, 1, 94, 1, 1, 942, 'InquiryEmail', 'update', '', '/InquiryEmail/update', '询盘收件箱编辑', '', '', 2, 1, 0, 1633528165, 1633528165);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (97, 1, 94, 1, 1, 943, 'InquiryEmail', 'delete', '', '/InquiryEmail/delete', '询盘收件箱删除', '', '', 2, 1, 0, 1633528173, 1633528173);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (98, 1, 0, 1, 2, 3, '', '', '', '', '营销管理', '', '', 1, 2, 0, 1633528319, 1633528319);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (99, 1, 219, 1, 1, 10, '', '', '', '', 'SEO', 'icon-seo', '', 1, 1, 0, 1633528488, 1633528488);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (100, 1, 99, 2, 1, 991, 'Keyword', 'index', '', '/Keyword/index', '关键词管理', '', '', 1, 1, 0, 1633528599, 1633528599);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (101, 1, 100, 1, 1, 1001, 'Keyword', 'save', '', '/Keyword/save', '关键词添加', '', '', 2, 1, 0, 1633528624, 1633528624);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (102, 1, 100, 1, 1, 1002, 'Keyword', 'update', '', '/Keyword/update', '关键词编辑', '', '', 2, 1, 0, 1633528636, 1633528636);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (103, 1, 100, 1, 1, 1003, 'Keyword', 'delete', '', '/Keyword/delete', '关键词删除', '', '', 2, 1, 0, 1633528648, 1633528648);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (104, 1, 99, 2, 1, 592, 'InnerChart', 'index', '', '/InnerChart/index', '内链管理', '', '', 1, 1, 0, 1633528703, 1633528703);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (105, 1, 104, 1, 1, 1041, 'InnerChart', 'save', '', '/InnerChart/save', '内链添加', '', '', 2, 1, 0, 1633528729, 1633528729);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (106, 1, 104, 1, 1, 1042, 'InnerChart', 'update', '', '/InnerChart/update', '内链编辑', '', '', 2, 1, 0, 1633528747, 1633528747);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (107, 1, 104, 1, 1, 1043, 'InnerChart', 'delete', '', '/InnerChart/delete', '内链删除', '', '', 2, 1, 0, 1633528759, 1633528759);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (108, 1, 132, 1, 1, 1321, 'Advertisement', 'save', '', '/Advertisement/save', '广告添加', '', '', 2, 1, 0, 1633528838, 1633528838);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (109, 1, 132, 1, 1, 1322, 'Advertisement', 'update', '', '/Advertisement/update', '广告编辑', '', '', 2, 1, 0, 1633528863, 1633528863);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (110, 1, 132, 1, 1, 1323, 'Advertisement', 'delete', '', '/Advertisement/delete', '广告删除', '', '', 2, 1, 0, 1633528914, 1633528914);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (111, 1, 99, 2, 1, 494, 'Link', 'index', '', '/Link/index', '友情链接管理', '', '', 1, 1, 0, 1633528988, 1633528988);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (112, 1, 111, 1, 1, 1111, 'Link', 'save', '', '/Link/save', '友情链接添加', '', '', 2, 1, 0, 1633529087, 1633529087);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (113, 1, 111, 1, 1, 1112, 'Link', 'update', '', '/Link/update', '友情链接编辑', '', '', 2, 1, 0, 1633529099, 1633529099);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (114, 1, 111, 1, 1, 1113, 'Link', 'delete', '', '/Link/delete', '友情链接删除', '', '', 2, 1, 0, 1633529111, 1633529111);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (115, 1, 111, 1, 1, 1114, 'Link', 'copy', '', '/Link/copy', '复制其他站点友链', '', '', 2, 1, 0, 1633529250, 1633529250);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (116, 1, 98, 1, 1, 982, '', '', '', '', '营销分析', 'icon-shujufenxi', '', 1, 1, 0, 1633529747, 1633529747);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (117, 1, 116, 2, 1, 1161, 'SEM', 'baidu', '', '/SEM/baidu', '百度', '', '', 1, 1, 0, 1633529780, 1633529780);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (118, 1, 116, 2, 1, 1161, 'SEM', 'google', '', '/SEM/google', '谷歌', '', '', 1, 1, 0, 1633529814, 1633529814);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (119, 1, 218, 1, 1, 50, '', '', '', '//', '招聘管理', 'icon-zhaopinguanli', '', 1, 1, 0, 1633529846, 1633529846);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (120, 1, 119, 2, 1, 1191, 'Job', 'index', '', '/Job/index', '岗位管理', 'icon-zhiweiguanli', '', 1, 1, 0, 1633529880, 1633529880);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (121, 1, 120, 1, 1, 1201, 'Job', 'save', '', '/Job/save', '岗位添加', '', '', 2, 1, 0, 1633529922, 1633529922);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (122, 1, 120, 1, 1, 1202, 'Job', 'update', '', '/Job/update', '岗位编辑', '', '', 2, 1, 0, 1633529940, 1633529940);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (123, 1, 120, 1, 1, 1203, 'Job', 'delete', '', '/Job/delete', '岗位删除', '', '', 2, 1, 0, 1633529954, 1633529954);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (124, 1, 119, 2, 1, 1192, 'JobCate', 'index', '', '/JobCate/index', '岗位分类', 'icon-gangweifenlei', '', 1, 1, 0, 1633530098, 1633530098);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (125, 1, 124, 1, 1, 1241, 'JobCate', 'save', '', '/JobCate/save', '岗位分类添加', '', '', 2, 1, 0, 1633530122, 1633530122);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (126, 1, 124, 1, 1, 1242, 'JobCate', 'update', '', '/JobCate/update', '岗位分类编辑', '', '', 2, 1, 0, 1633530134, 1633530134);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (127, 1, 124, 1, 1, 1243, 'JobCate', 'update', '', '/JobCate/update', '岗位分类删除', '', '', 2, 1, 0, 1633530143, 1633530143);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (128, 1, 119, 2, 1, 1193, 'JobCity', 'index', '', '/JobCity/index', '工作城市', 'icon-chengshiguanli', '', 1, 1, 0, 1633530203, 1633530203);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (129, 1, 128, 1, 1, 1281, 'JobCity', 'save', '', '/JobCity/save', '城市添加', '', '', 2, 1, 0, 1633530231, 1633530231);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (130, 1, 128, 1, 1, 1282, 'JobCity', 'update', '', '/JobCity/update', '城市编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (131, 1, 128, 1, 1, 1283, 'JobCity', 'delete', '', '/JobCity/delete', '城市删除', '', '', 2, 1, 0, 1633530261, 1633530261);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (132, 1, 56, 2, 1, 993, 'Advertisement', 'index', '', '/Advertisement/index', '广告管理', '', '', 1, 1, 0, 1633531310, 1633531310);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (133, 1, 56, 2, 1, 564, 'Content', 'index', '', '/Content/index', '栏目内容', 'icon-neirongguanli', '', 1, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (134, 1, 56, 2, 1, 565, 'SlideCate', 'index', '', '/SlideCate/index', '幻灯片管理', 'icon-neirongguanli', '', 1, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (135, 1, 56, 1, 1, 566, 'Slide', 'index', '', '/Slide/index', '幻灯片列表', 'icon-neirongguanli', '', 2, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (136, 1, 134, 1, 1, 1341, 'SlideCate', 'save', '', '/SlideCate/save', '分类添加', '', '', 2, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (137, 1, 134, 1, 1, 1342, 'SlideCate', 'update', '', '/SlideCate/update', '分类编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (138, 1, 134, 1, 1, 1343, 'SlideCate', 'read', '', '/SlideCate/read', '分类详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (139, 1, 134, 1, 1, 1344, 'SlideCate', 'delete', '', '/SlideCate/delete', '分类删除', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (140, 1, 135, 1, 1, 1351, 'Slide', 'save', '', '/Slide/save', '幻灯片添加', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (141, 1, 135, 1, 1, 1352, 'Slide', 'update', '', '/Slide/update', '幻灯片编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (142, 1, 135, 1, 1, 1353, 'Slide', 'read', '', '/Slide/read', '幻灯片详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (143, 1, 135, 1, 1, 1354, 'Slide', 'delete', '', '/Slide/delete', '幻灯片删除', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (144, 1, 56, 2, 1, 564, 'AttachmentCate', 'index', '', '/AttachmentCate/index', '附件管理', 'icon-fujianguanli', '', 1, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (145, 1, 134, 1, 1, 1441, 'AttachmentCate', 'save', '', '/AttachmentCate/save', '附件分类添加', '', '', 2, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (146, 1, 134, 1, 1, 1442, 'AttachmentCate', 'update', '', '/AttachmentCate/update', '附件分类编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (147, 1, 134, 1, 1, 1443, 'AttachmentCate', 'read', '', '/AttachmentCate/read', '附件分类详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (148, 1, 134, 1, 1, 1444, 'AttachmentCate', 'delete', '', '/AttachmentCate/delete', '附件分类删除', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (149, 1, 144, 1, 1, 1448, 'Attachment', 'uploadAndSave', '', '/Attachment/uploadAndSave', '附件上传', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (150, 1, 33, 1, 1, 334, 'Website', 'sitemap', '', '/Website/sitemap', '生成sitemap', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (151, 1, 33, 1, 1, 335, 'Website', 'setStatus', '', '/Website/setStatus', '设置网站状态', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (152, 1, 67, 1, 1, 674, 'Category', 'copy', '', '/Category/copy', '复制站点栏目', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (153, 1, 133, 1, 1, 1334, 'Content', 'topOpt', '', '/Content/topOpt', '设置置顶状态', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (154, 1, 133, 1, 1, 1335, 'Content', 'recommendAndStatus', '', '/Content/recommendAndStatus', '设置推荐和禁用状态', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (155, 1, 87, 1, 1, 873, 'inquiry', 'backendSave', '', '/inquiry/backendSave', '询盘添加', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (156, 1, 87, 1, 1, 874, 'inquiry', 'read', '', '/inquiry/read', '询盘详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (157, 1, 100, 1, 1, 1004, 'keyword', 'import', '', '/keyword/import', '导入关键词', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (158, 1, 111, 1, 1, 1115, 'link', 'read', '', '/link/read', '友情链接详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (159, 1, 120, 1, 1, 1204, 'job', 'read', '', '/job/read', '岗位详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (160, 1, 12, 1, 1, 124, 'role', 'read', '', '/role/read', '角色详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (161, 1, 116, 2, 1, 1160, 'SeoAccount', 'lists', '', '/SEM/account', '账号管理', '', '', 1, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (162, 1, 161, 2, 1, 1611, 'SeoAccount', 'add', '', '/SeoAccount/add', '账号添加', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (163, 1, 161, 2, 1, 1612, 'SeoAccount', 'edit', '', '/SeoAccount/edit', '账号编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (164, 1, 161, 2, 1, 1613, 'SeoAccount', 'info', '', '/SeoAccount/info', '账号详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (165, 1, 161, 2, 1, 1614, 'SeoAccount', 'del', '', '/SeoAccount/del', '账号删除', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (166, 1, 219, 1, 1, 20, 'form', 'index', '', '/Form/index', '表单管理', 'icon-biaodanguanli', '', 1, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (167, 1, 220, 1, 1, 985, '', '', '', '//', '活动管理', 'icon-huodongguanli', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (168, 1, 219, 1, 1, 30, 'poster', 'index', '', '/poster/index', '海报管理', 'icon-haibaoguanli', '', 1, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (169, 1, 99, 1, 1, 995, 'seoCheck', 'index', '', '/seoCheck/index', 'SEO分析诊断', '', '', 1, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (170, 1, 220, 2, 1, 10, 'Plugin', 'list', '', '/Plugin/index', '插件管理', 'icon-chajianguanli', '', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (171, 1, 220, 1, 2, 171, '', '', '', '//', '会员管理', 'icon-huiyuanguanli', 'plugin_user', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (172, 1, 171, 1, 2, 1712, 'User', 'list', '', '/user/index', '会员列表', 'icon-shujukubeifen', 'plugin_user', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (173, 1, 172, 1, 2, 1721, 'User', 'add', '', '/user/add', '添加会员', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (174, 1, 172, 1, 2, 1722, 'User', 'edit', '', '/user/add', '编辑会员', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (175, 1, 172, 1, 2, 1723, 'User', 'editStatus', '', '/user/editStatus', '编辑会员状态', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (176, 1, 171, 1, 2, 1711, 'UserGroup', 'list', '', '/UserGroup/index', '会员分类', 'icon-shujukubeifen', 'plugin_user', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (177, 1, 176, 1, 2, 1761, 'UserGroup', 'add', '', '/UserGroup/add', '添加会员分类', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (178, 1, 176, 1, 2, 1762, 'UserGroup', 'edit', '', '/UserGroup/edit', '编辑会员分类', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (179, 1, 176, 1, 2, 1762, 'UserGroup', 'del', '', '/UserGroup/del', '删除会员分类', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (180, 1, 171, 1, 2, 1713, 'UserGrade', 'list', '', '/UserGrade/index', '会员等级', 'icon-shujukubeifen', 'plugin_user', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (181, 1, 180, 1, 2, 1801, 'UserGrade', 'add', '', '/UserGrade/add', '添加会员等级', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (182, 1, 180, 1, 2, 1802, 'UserGrade', 'edit', '', '/UserGrade/edit', '编辑会员等级', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (183, 1, 180, 1, 2, 1803, 'UserGrade', 'del', '', '/UserGrade/del', '删除会员等级', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (184, 1, 171, 1, 2, 1714, 'UserLogin', 'list', '', '/userLoginLog/index', '登录日志', 'icon-shujukubeifen', 'plugin_user', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (185, 1, 171, 1, 2, 1715, 'UserLogin', 'del', '', '/userLoginLog/del', '删除登录日志', '', 'plugin_user', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (186, 1, 170, 1, 1, 1701, 'Plugin', 'install', '', '/Plugin/install', '安装插件', '', '', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (187, 1, 170, 1, 1, 1702, 'Plugin', 'uninstall', '', '/Plugin/uninstall', '卸载插件', '', '', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (188, 1, 220, 1, 2, 188, '', '', '', '//', '知产管理', 'icon-zichanguanli', 'plugin_assets', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (189, 1, 188, 1, 2, 1881, 'Domain', 'list', '', '/Domain/index', '域名管理', 'icon-shujukubeifen', 'plugin_assets', 1, 2, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (190, 1, 188, 1, 2, 1882, 'Server', 'list', '', '/Server/index', '主机管理', 'icon-shujukubeifen', 'plugin_assets', 1, 2, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (191, 1, 188, 1, 2, 1882, 'Trademark', 'list', '', '/Trademark/index', '商标管理', 'icon-shujukubeifen', 'plugin_assets', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (192, 1, 188, 1, 2, 1881, 'Copyright', 'list', '', '/Copyright/index', '版权管理', 'icon-shujukubeifen', 'plugin_assets', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (193, 1, 170, 1, 1, 1703, 'Plugin', 'editStatus', '', '/Plugin/editStatus', '修改插件状态', '', '', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (194, 1, 189, 1, 2, 1891, 'Domain', 'add', '', '/Domain/add', '添加域名', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (195, 1, 189, 1, 2, 1892, 'Domain', 'edit', '', '/Domain/edit', '编辑域名', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (196, 1, 189, 1, 2, 1893, 'Domain', 'del', '', '/Domain/del', '删除域名', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (197, 1, 190, 1, 2, 1901, 'Server', 'add', '', '/Server/add', '添加主机', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (198, 1, 190, 1, 2, 1902, 'Server', 'edit', '', '/Server/edit', '编辑主机', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (199, 1, 190, 1, 2, 1903, 'Server', 'del', '', '/Server/del', '删除主机', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (200, 1, 191, 1, 2, 1911, 'Trademark', 'add', '', '/Trademark/add', '添加商标', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (201, 1, 191, 1, 2, 1912, 'Trademark', 'edit', '', '/Trademark/edit', '编辑商标', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (202, 1, 191, 1, 2, 1913, 'Trademark', 'del', '', '/Trademark/del', '删除商标', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (203, 1, 192, 1, 2, 1921, 'Copyright', 'add', '', '/Copyright/add', '添加版权', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (204, 1, 192, 1, 2, 1922, 'Copyright', 'edit', '', '/Copyright/edit', '编辑版权', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (205, 1, 192, 1, 2, 1923, 'Copyright', 'del', '', '/Copyright/del', '删除版权', '', 'plugin_assets', 2, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (206, 1, 56, 2, 1, 2060, 'Content', 'checkList', '', '/Content/checkList', '内容审核', 'icon-neirongguanli', '', 1, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (207, 1, 206, 1, 1, 2061, 'Content', 'checkAll', '', '/Content/checkAll', '审核通过/拒绝', '', '', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (208, 1, 133, 1, 1, 1336, 'Content', 'delAll', '', '/Content/delAll', '批量删除', '', '', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (209, 1, 133, 1, 1, 1337, 'Content', 'editStatus', '', '/Content/editStatus', '批量禁用/启用', '', '', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (210, 1, 188, 1, 2, 1883, 'Soft', 'list', '', '/Soft/index', '软著管理', 'icon-shujukubeifen', 'plugin_assets', 1, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (211, 1, 210, 1, 2, 2101, 'Soft', 'add', '', '/Soft/add', '添加软著', '', 'plugin_assets', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (212, 1, 210, 1, 2, 2102, 'Soft', 'edit', '', '/Soft/edit', '编辑软著', '', 'plugin_assets', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (213, 1, 210, 1, 2, 2103, 'Soft', 'del', '', '/Soft/del', '删除软著', '', 'plugin_assets', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (214, 1, 99, 2, 1, 328, 'StaticFile', 'index', '', '/StaticFile/index', '网站静态化', 'icon-wangzhanmobanguanli', '', 1, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (215, 1, 214, 1, 1, 2141, 'StaticFile', 'createCategory', '', '/StaticFile/createCategory', '生成栏目页html', '', '', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (216, 1, 214, 1, 1, 2142, 'StaticFile', 'createContent', '', '/StaticFile/createContent', '生成文章页html', '', '', 2, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (218, 1, 0, 1, 1, 2, '', '', '', '', '网站', '', '', 1, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (219, 1, 0, 1, 1, 3, '', '', '', '', '营销', '', '', 1, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (220, 1, 0, 1, 1, 20, '', '', '', '', '设置', '', '', 1, 1, 0, 0, 0);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (221, 1, 2, 2, 1, 1, 'SysSetting', 'dashbord', '', '/', '控制面板', 'icon-kongzhimianban', '', 1, 1, 0, 1633486674, 1633486674);
INSERT INTO `$prefix$admin_menu` (`id`, `seller_id`, `parent_id`, `type`, `status`, `sort`, `controller`, `action`, `param`, `path`, `title`, `icon`, `remark`, `is_menu`, `is_del`, `delete_time`, `create_time`, `update_time`) VALUES (222, 1, 37, 1, 1, 372, 'WebsiteLang', 'changeLang', '', '/WebsiteLang/changeLang', '切换语言', '', '', 2, 1, 0, 1633488558, 1633488558);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$admin_opt_log
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$admin_opt_log`;
CREATE TABLE `$prefix$admin_opt_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `admin_id` int(11) DEFAULT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `title` varchar(155) DEFAULT NULL COMMENT '操作',
  `content` varchar(155) DEFAULT NULL COMMENT '操作详情',
  `ip` varchar(120) DEFAULT NULL COMMENT '操作人IP',
  `addr` varchar(255) DEFAULT NULL COMMENT '操作地点',
  `agent` varchar(255) DEFAULT NULL COMMENT '操作人客户端',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='操作日志';

-- ----------------------------
-- Records of $prefix$admin_opt_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$admin_role
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$admin_role`;
CREATE TABLE `$prefix$admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员角色关联表',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
  `admin_id` int(11) DEFAULT NULL COMMENT '管理员ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员角色关联表';

-- ----------------------------
-- Records of $prefix$admin_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$advertisement
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$advertisement`;
CREATE TABLE `$prefix$advertisement` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '广告ID',
  `seller_id` int(11) DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT '0' COMMENT '站点ID',
  `type` tinyint(2) DEFAULT NULL COMMENT '广告类型 1文字 2图片 3类型',
  `title` varchar(255) DEFAULT NULL COMMENT '广告标题',
  `content` varchar(255) DEFAULT NULL COMMENT '文字内容',
  `url` varchar(255) DEFAULT NULL COMMENT '广告链接',
  `attachment_id` int(11) DEFAULT NULL COMMENT '广告附件',
  `size` varchar(255) DEFAULT NULL COMMENT '文件尺寸',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态1 正常2 禁用',
  `expiration_date` date DEFAULT NULL COMMENT '有效期',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='广告表';

-- ----------------------------
-- Records of $prefix$advertisement
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$article
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$article`;
CREATE TABLE `$prefix$article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sub_title` varchar(255) DEFAULT '' COMMENT '文章副标题',
  `seller_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言标识',
  `lang_pid` int(11) DEFAULT '0' COMMENT '语言锚点',
  `sub_id` bigint(20) unsigned DEFAULT NULL COMMENT '副表ID',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='系统文章模型表';

-- ----------------------------
-- Records of $prefix$article
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$article` VALUES (6, '', 1, 'zh', 0, 1, 1635736625, 1635771952);
INSERT INTO `$prefix$article` VALUES (7, '', 1, 'zh', 0, 2, 1635737755, 1635771936);
INSERT INTO `$prefix$article` VALUES (8, '', 1, 'zh', 0, 3, 1635737900, 1635771911);
INSERT INTO `$prefix$article` VALUES (9, '', 1, 'zh', 0, 4, 1635738046, 1635771884);
INSERT INTO `$prefix$article` VALUES (10, '', 1, 'zh', 0, 5, 1635738144, 1635770316);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$article_test
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$article_test`;
CREATE TABLE `$prefix$article_test` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sub_title` varchar(255) DEFAULT '' COMMENT '副标题',
  `image` int(11) DEFAULT NULL COMMENT '单图',
  `more_image` varchar(255) DEFAULT '' COMMENT '多图',
  `file` int(11) DEFAULT NULL COMMENT '单视频',
  `more_video` varchar(255) DEFAULT '' COMMENT '多视频',
  `files` int(11) DEFAULT NULL COMMENT '单文件',
  `more_file` varchar(255) DEFAULT '' COMMENT '多文件',
  `seller_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言标识',
  `lang_pid` int(11) DEFAULT '0' COMMENT '语言锚点',
  `sub_id` bigint(20) unsigned DEFAULT NULL COMMENT '副表ID',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='文章模型表';

-- ----------------------------
-- Records of $prefix$article_test
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$article_test` VALUES (1, '222222', NULL, '', NULL, '', NULL, '', 1, 'zh', 0, 32, 1635735278, 1635735278);
INSERT INTO `$prefix$article_test` VALUES (2, '222222', NULL, '', NULL, '', NULL, '', 1, 'zh', 0, 33, 1635735304, 1635735304);
INSERT INTO `$prefix$article_test` VALUES (3, '2222', NULL, '', NULL, '', NULL, '', 1, 'zh', 0, 34, 1635735954, 1635747532);
INSERT INTO `$prefix$article_test` VALUES (4, '222222', NULL, '', NULL, '', NULL, '', 1, 'zh', 0, 35, 1635760324, 1635760324);
INSERT INTO `$prefix$article_test` VALUES (5, '1111', 67, '', NULL, '', NULL, '', 1, 'zh', 0, 36, 1635931656, 1635931656);
INSERT INTO `$prefix$article_test` VALUES (6, '232333', 67, '', NULL, '', NULL, '', 1, 'zh', 0, 37, 1635932155, 1635932155);
INSERT INTO `$prefix$article_test` VALUES (7, '1111', 62, '', NULL, '', NULL, '', 1, 'zh', 0, 38, 1635932316, 1635932843);
INSERT INTO `$prefix$article_test` VALUES (8, '222', 0, '', NULL, '', 66, '', 1, 'zh', 0, 39, 1635934786, 1635934786);
INSERT INTO `$prefix$article_test` VALUES (9, '1212', 0, '', 0, '', 74, '', 1, 'zh', 0, 40, 1635935395, 1635935395);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$attachment
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$attachment`;
CREATE TABLE `$prefix$attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '附件id',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `name` varchar(255) DEFAULT '' COMMENT '附件名称',
  `attachment_cate_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `hash` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT '' COMMENT '附件访问地址',
   `reduce_img` varchar(255)  DEFAULT '' COMMENT '缩减图片',
  `mime_type` varchar(255) DEFAULT '' COMMENT '文件类型',
  `type` int(2) DEFAULT NULL COMMENT '0 所有 1 压缩包 2 图片 3 视频 4 文件 5 音频',
  `storage` tinyint(2) DEFAULT '1' COMMENT '1 本地 2  阿里 3 腾讯 4 七牛',
  `size` double(10,2) DEFAULT NULL,
  `is_del` int(11) DEFAULT '1' COMMENT '删除 1 正常  2 已删除',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COMMENT='附件表';

-- ----------------------------
-- Records of $prefix$attachment
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$attachment` VALUES (3, 1, 'pailiu.png', NULL, NULL, NULL, 'storage/image/20211101/38be917083b9679d8e08e9f0b8a8ed78.png','storage/image/20211101/38be917083b9679d8e08e9f0b8a8ed78.png', 'image/png', 2, 1, 15.98, 1, NULL, 1635729944, 1635729944);
INSERT INTO `$prefix$attachment` VALUES (4, 1, 'news_info_content_img2.png', NULL, NULL, NULL, 'storage/image/20211101/fc5b4d70b3bbf7bd10da18c8678736be.png','storage/image/20211101/fc5b4d70b3bbf7bd10da18c8678736be.png', 'image/png', 2, 1, 136.02, 2, 1635758944, 1635733706, 1635758944);
INSERT INTO `$prefix$attachment` VALUES (5, 1, 'news_info_content_img1.png', NULL, NULL, NULL, 'storage/image/20211101/cf788372068a567c099afcff960fb847.png','storage/image/20211101/cf788372068a567c099afcff960fb847.png', 'image/png', 2, 1, 154.88, 2, 1635758946, 1635733730, 1635758946);
INSERT INTO `$prefix$attachment` VALUES (6, 1, 'news_info_content_img3.png', NULL, NULL, NULL, 'storage/image/20211101/805e475b1e9c78de80b9e861792542c2.png','storage/image/20211101/805e475b1e9c78de80b9e861792542c2.png', 'image/png', 2, 1, 137.87, 2, 1635758948, 1635733767, 1635758948);
INSERT INTO `$prefix$attachment` VALUES (7, 1, 'img1.jpg', NULL, NULL, NULL, 'storage/image/20211101/9563a1880c7b7bbc40dbe11fcbaf55ab.jpg','storage/image/20211101/9563a1880c7b7bbc40dbe11fcbaf55ab.jpg', 'image/jpeg', 2, 1, 3.79, 1, NULL, 1635738390, 1635738390);
INSERT INTO `$prefix$attachment` VALUES (8, 1, 'img2.jpg', NULL, NULL, NULL, 'storage/image/20211101/8c7ebbbe29124ed2c8dc5f42ba2beb1a.jpg','storage/image/20211101/8c7ebbbe29124ed2c8dc5f42ba2beb1a.jpg', 'image/jpeg', 2, 1, 4.02, 1, NULL, 1635738419, 1635738419);
INSERT INTO `$prefix$attachment` VALUES (9, 1, 'img3.jpg', NULL, NULL, NULL, 'storage/image/20211101/6ab69d312f43fa93184c22f968c8dbf7.jpg','storage/image/20211101/6ab69d312f43fa93184c22f968c8dbf7.jpg', 'image/jpeg', 2, 1, 5.39, 1, NULL, 1635738432, 1635738432);
INSERT INTO `$prefix$attachment` VALUES (10, 1, 'img4.jpg', NULL, NULL, NULL, 'storage/image/20211101/a9f496fce1907563ba93c3a401c87fde.jpg','storage/image/20211101/a9f496fce1907563ba93c3a401c87fde.jpg', 'image/jpeg', 2, 1, 3.77, 1, NULL, 1635738500, 1635738500);
INSERT INTO `$prefix$attachment` VALUES (11, 1, 'img5.jpg', NULL, NULL, NULL, 'storage/image/20211101/85483b6b0b69d20437f299d985927eaa.jpg','storage/image/20211101/85483b6b0b69d20437f299d985927eaa.jpg', 'image/jpeg', 2, 1, 4.04, 1, NULL, 1635738598, 1635738598);
INSERT INTO `$prefix$attachment` VALUES (12, 1, 'img6.jpg', NULL, NULL, NULL, 'storage/image/20211101/1736a949eca7ea1ddcfeae2ba136eae0.jpg','storage/image/20211101/1736a949eca7ea1ddcfeae2ba136eae0.jpg', 'image/jpeg', 2, 1, 3.83, 1, NULL, 1635738609, 1635738609);
INSERT INTO `$prefix$attachment` VALUES (13, 1, 'brand.png', NULL, NULL, NULL, 'storage/image/20211101/070b64627dff6822ebc2ff75f5038d27.png','storage/image/20211101/070b64627dff6822ebc2ff75f5038d27.png', 'image/png', 2, 1, 229.35, 1, NULL, 1635742889, 1635742889);
INSERT INTO `$prefix$attachment` VALUES (14, 1, 'news_banner.jpg', NULL, NULL, NULL, 'storage/image/20211101/6827d474d2af78a61180865fa30f21aa.jpg','storage/image/20211101/6827d474d2af78a61180865fa30f21aa.jpg', 'image/jpeg', 2, 1, 495.63, 1, NULL, 1635743060, 1635743060);
INSERT INTO `$prefix$attachment` VALUES (15, 1, 'news_bin1.jpg', NULL, NULL, NULL, 'storage/image/20211101/76dfb6004e2d6456629239a8d438d6b2.jpg','storage/image/20211101/76dfb6004e2d6456629239a8d438d6b2.jpg', 'image/jpeg', 2, 1, 642.78, 1, NULL, 1635753576, 1635753576);
INSERT INTO `$prefix$attachment` VALUES (16, 1, 'news_bin2.jpg', NULL, NULL, NULL, 'storage/image/20211101/fd80087cef6735223ada389c861d5335.jpg','storage/image/20211101/fd80087cef6735223ada389c861d5335.jpg', 'image/jpeg', 2, 1, 310.03, 1, NULL, 1635753595, 1635753595);
INSERT INTO `$prefix$attachment` VALUES (17, 1, 'news_bin3.jpg', NULL, NULL, NULL, 'storage/image/20211101/fbb92441d3bf85476e3080038933d68a.jpg','storage/image/20211101/fbb92441d3bf85476e3080038933d68a.jpg', 'image/jpeg', 2, 1, 164.24, 1, NULL, 1635753613, 1635753613);
INSERT INTO `$prefix$attachment` VALUES (18, 1, 'news_bin4.jpg', NULL, NULL, NULL, 'storage/image/20211101/06f865e23d0457152c2e7e97e9009e75.jpg','storage/image/20211101/06f865e23d0457152c2e7e97e9009e75.jpg', 'image/jpeg', 2, 1, 132.76, 1, NULL, 1635753627, 1635753627);
INSERT INTO `$prefix$attachment` VALUES (19, 1, 'news_bin5.jpg', NULL, NULL, NULL, 'storage/image/20211101/5797acedfc6bd4b91988ebab80e235d7.jpg', 'storage/image/20211101/5797acedfc6bd4b91988ebab80e235d7.jpg','image/jpeg', 2, 1, 115.81, 1, NULL, 1635753646, 1635753646);
INSERT INTO `$prefix$attachment` VALUES (20, 1, 'news_info_content_img1.jpg', NULL, NULL, NULL, 'storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg','storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg', 'jpg', 2, 1, 24.06, 1, NULL, 1635758964, 1635758964);
INSERT INTO `$prefix$attachment` VALUES (21, 1, 'news_info_content_img2.jpg', NULL, NULL, NULL, 'storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg','storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg', 'jpg', 2, 1, 20.59, 1, NULL, 1635758970, 1635758970);
INSERT INTO `$prefix$attachment` VALUES (22, 1, 'news_info_content_img3.jpg', NULL, NULL, NULL, 'storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg','storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg', 'jpg', 2, 1, 22.99, 1, NULL, 1635758976, 1635758976);
INSERT INTO `$prefix$attachment` VALUES (23, 1, 'product1.png', NULL, NULL, NULL, 'storage/image/20211101/d3353c538092e502cd10587342a4b0d5.png','storage/image/20211101/d3353c538092e502cd10587342a4b0d5.png', 'png', 2, 1, 23.38, 1, NULL, 1635761632, 1635761632);
INSERT INTO `$prefix$attachment` VALUES (24, 1, 'product2.png', NULL, NULL, NULL, 'storage/image/20211101/79f39cc3eb91146c531cde909803bb46.png','storage/image/20211101/79f39cc3eb91146c531cde909803bb46.png', 'png', 2, 1, 26.77, 1, NULL, 1635761640, 1635761640);
INSERT INTO `$prefix$attachment` VALUES (25, 1, 'product3.png', NULL, NULL, NULL, 'storage/image/20211101/95d76b393024b96a6de787e48b5b98e6.png','storage/image/20211101/95d76b393024b96a6de787e48b5b98e6.png', 'png', 2, 1, 27.93, 1, NULL, 1635761647, 1635761647);
INSERT INTO `$prefix$attachment` VALUES (26, 1, 'product4.png', NULL, NULL, NULL, 'storage/image/20211101/5d517907fe3ce0e8266cac5d037ad11c.png','storage/image/20211101/5d517907fe3ce0e8266cac5d037ad11c.png', 'png', 2, 1, 50.63, 1, NULL, 1635761662, 1635761662);
INSERT INTO `$prefix$attachment` VALUES (27, 1, 'product5.png', NULL, NULL, NULL, 'storage/image/20211101/f2b9323a1ff901e9f5e533bee53c3c63.png','storage/image/20211101/f2b9323a1ff901e9f5e533bee53c3c63.png', 'png', 2, 1, 45.69, 1, NULL, 1635761669, 1635761669);
INSERT INTO `$prefix$attachment` VALUES (28, 1, 'product6.png', NULL, NULL, NULL, 'storage/image/20211101/e20877b7e303960bbc5918d93c849db1.png','storage/image/20211101/e20877b7e303960bbc5918d93c849db1.png', 'png', 2, 1, 43.43, 1, NULL, 1635761679, 1635761679);
INSERT INTO `$prefix$attachment` VALUES (29, 1, 'product_banner1.png', NULL, NULL, NULL, 'storage/image/20211101/3d8e28716f77eaa5e2a85ce0cb93fbc5.png','storage/image/20211101/3d8e28716f77eaa5e2a85ce0cb93fbc5.png', 'png', 2, 1, 287.15, 1, NULL, 1635761695, 1635761695);
INSERT INTO `$prefix$attachment` VALUES (30, 1, 'logo.png', NULL, NULL, NULL, 'storage/image/20211101/ff29e6d545ecf34bf129a668e401f003.png','storage/image/20211101/ff29e6d545ecf34bf129a668e401f003.png', 'png', 2, 1, 1.54, 1, NULL, 1635761707, 1635761707);
INSERT INTO `$prefix$attachment` VALUES (31, 1, 'productInfo_banner.jpg', NULL, NULL, NULL, 'storage/image/20211101/20a5f48d8c042741a8ed5a1812c6b392.jpg','storage/image/20211101/20a5f48d8c042741a8ed5a1812c6b392.jpg', 'jpg', 2, 1, 115.57, 1, NULL, 1635761728, 1635761728);
INSERT INTO `$prefix$attachment` VALUES (32, 1, 'productInfo1.jpg', NULL, NULL, NULL, 'storage/image/20211101/6475ebedbe99ffa3d4828f28e4945ec1.jpg','storage/image/20211101/6475ebedbe99ffa3d4828f28e4945ec1.jpg', 'jpg', 2, 1, 305.89, 1, NULL, 1635761747, 1635761747);
INSERT INTO `$prefix$attachment` VALUES (33, 1, 'productInfo2.jpg', NULL, NULL, NULL, 'storage/image/20211101/978528f42b2f604f0333739aae75d84f.jpg','storage/image/20211101/978528f42b2f604f0333739aae75d84f.jpg', 'jpg', 2, 1, 361.55, 1, NULL, 1635761753, 1635761753);
INSERT INTO `$prefix$attachment` VALUES (34, 1, 'QRcode.png', NULL, NULL, NULL, 'storage/image/20211101/6c9f46f521b86c0c7277fbefea9dae40.png','storage/image/20211101/6c9f46f521b86c0c7277fbefea9dae40.png', 'png', 2, 1, 5.19, 1, NULL, 1635761763, 1635761763);
INSERT INTO `$prefix$attachment` VALUES (35, 1, 'productInfo1.jpg', NULL, NULL, NULL, 'storage/image/20211101/b076303318fa2b4c714b67642363e004.jpg','storage/image/20211101/b076303318fa2b4c714b67642363e004.jpg', 'image/jpeg', 2, 1, 305.89, 1, NULL, 1635762210, 1635762210);
INSERT INTO `$prefix$attachment` VALUES (36, 1, 'productInfo2.jpg', NULL, NULL, NULL, 'storage/image/20211101/81017b911fa59c08d1930eec90558547.jpg','storage/image/20211101/81017b911fa59c08d1930eec90558547.jpg', 'image/jpeg', 2, 1, 361.55, 1, NULL, 1635762219, 1635762219);
INSERT INTO `$prefix$attachment` VALUES (37, 1, 'brand.png', NULL, NULL, NULL, 'storage/image/20211101/aab7356088f9812bc368f74dd875aab2.png','storage/image/20211101/aab7356088f9812bc368f74dd875aab2.png', 'image/png', 2, 1, 229.35, 1, NULL, 1635764664, 1635764664);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$attachment_cate
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$attachment_cate`;
CREATE TABLE `$prefix$attachment_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件分类ID',
  `seller_id` int(11) unsigned DEFAULT '1' COMMENT '商户ID',
  `parent_id` int(11) DEFAULT '0' COMMENT '父栏目ID',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `title` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='附件分类表';

-- ----------------------------
-- Records of $prefix$attachment_cate
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$big_field
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$big_field`;
CREATE TABLE `$prefix$big_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `theme_id` int(11) DEFAULT NULL COMMENT '模版ID',
  `theme_file_id` int(11) NOT NULL DEFAULT '0' COMMENT '模板文件ID',
  `content` longtext COMMENT '源代码',
  `version` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '版本标识',
  `more` mediumtext COMMENT '初始配置',
  `draft_more` mediumtext COMMENT '用户配置',
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作人',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模板文件表大内容字段';

-- ----------------------------
-- Records of $prefix$big_field
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$category
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$category`;
CREATE TABLE `$prefix$category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `parent_id` int(11) DEFAULT '0' COMMENT '父分类ID',
  `parent_map` int(11) DEFAULT '0' COMMENT '附站点栏目ID映射',
  `parent_path` varchar(255) DEFAULT NULL COMMENT '附站点栏目ID映射',
  `has_map` tinyint(2) DEFAULT '2' COMMENT '是否映射父栏目 1 是 2 否',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `module_id` int(11) DEFAULT NULL COMMENT '模型ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言 ',
  `lang_pid` int(11) DEFAULT '0' COMMENT '多语言锚点 0 默认语言',
  `type` tinyint(1) DEFAULT '1' COMMENT '栏目类型 1 列表栏目  2 频道封面 3 跳转链接 4 单页面',
  `title` varchar(255) DEFAULT '' COMMENT '分类名称',
  `sub_title` varchar(255) DEFAULT '' COMMENT '分类名称',
  `content` text COMMENT '内容',
  `path` varchar(255) DEFAULT '' COMMENT '分类路径(到最上一层父级路径)',
  `desc` varchar(255) DEFAULT '' COMMENT '分类简介',
  `description` varchar(255) DEFAULT '' COMMENT '分类描述',
  `sort` int(11) DEFAULT '100' COMMENT '分类排序',
  `alias` varchar(255) DEFAULT '' COMMENT '栏目别名',
  `banner` int(11) DEFAULT NULL COMMENT 'banner图',
  `thumbnail` int(11) DEFAULT NULL COMMENT '缩略图',
  `list_tpl` varchar(255) DEFAULT '' COMMENT '分类列表模板',
  `detail_tpl` varchar(255) DEFAULT '' COMMENT '详情页模版',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 正常 2 禁用',
  `is_search` tinyint(2) DEFAULT '2' COMMENT '栏目是否搜索',
  `seo_title` varchar(100) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COMMENT='栏目表';

-- ----------------------------
-- Records of $prefix$category
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$category` VALUES (1, 1, 0, 0, NULL, 2, 1, 1, 'zh', 0, 4, '首页', '', NULL, '0-', '', '', 100, '/', NULL, NULL, 'index', '', 1, 2, '网站首页', '网站首页', '网站首页', 1635478353, 1641526807);
INSERT INTO `$prefix$category` VALUES (5, 1, 0, 0, NULL, 2, 1, 1, 'zh', 0, 1, '新闻资讯', '', NULL, '0-', '', '我们的产品遍布全国31个省、630多座城市', 110, '/new', 14, 13, 'list_news', 'detail_news_info', 1, 2, '', '', '', 1635731380, 1641526807);
INSERT INTO `$prefix$category` VALUES (6, 1, 0, 0, NULL, 2, 1, 2, 'zh', 0, 4, '客户', '', NULL, '0-', '', '', 120, '/customer', NULL, NULL, 'list_news', '', 1, 2, '', '', '', 1635738289, 1641526807);
INSERT INTO `$prefix$category` VALUES (7, 1, 0, 0, NULL, 2, 1, 1, 'zh', 0, 4, '品牌介绍', '', NULL, '0-', '', '得事家电品牌正式进入中国，基于对中国市场的深刻洞察和准确定位，得事家电品牌在中国完成了全产业链布局，严格而先进的全球统一标准，贯穿了研发、采购、生产、销售直至售后服务全部家电产业生态系统的所有环节，为该品牌提供支持的投资公司、产品研发中心、原材料采购中心、销售公司、服务公司等机构遍布全国各地。', 130, '/introduction', 0, 37, 'news', '', 1, 2, '', '', '', 1635738972, 1641526807);
INSERT INTO `$prefix$category` VALUES (9, 1, 5, 0, NULL, 2, 1, 1, 'zh', 0, 1, '公司新闻', '', NULL, '0-5-', '', '', 100, '/company_new', 14, 0, 'list_news', 'detail_news_info', 1, 2, '', '', '', 1635748867, 1641526807);
INSERT INTO `$prefix$category` VALUES (10, 1, 5, 0, NULL, 2, 1, 1, 'zh', 0, 1, '行业资讯', '', NULL, '0-5-', '', '', 110, '/industry_new', 14, 0, 'list_news', 'detail_news_info', 1, 2, '', '', '', 1635748895, 1641526807);
INSERT INTO `$prefix$category` VALUES (11, 1, 0, 0, NULL, 2, 1, 3, 'zh', 0, 1, '产品', '', NULL, '0-', '', '得事家电品牌正式进入中国，基于对中国市场的深刻洞察和准确定位，得事家电品牌在中国完成了全产业链布局，严格而先进的全球统一标准，贯穿了研发、采购、生产、销售直至售后服务全部家电产业生态系统的所有环节，为该品牌提供支持的投资公司、产品研发中心、原材料采购中心、销售公司、服务公司等机构遍布全国各地。', 140, '/product', 37, 0, 'list_product', 'detail_product_info', 1, 2, '', '', '', 1635761247, 1641526807);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$category_sub_content
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$category_sub_content`;
CREATE TABLE `$prefix$category_sub_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `seller_id` int(11) DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `category_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `sub_content_id` int(10) unsigned DEFAULT NULL COMMENT '内容副表ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COMMENT='分类内容关联表';

-- ----------------------------
-- Records of $prefix$category_sub_content
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$category_sub_content` VALUES (6, 1, NULL, 6, 6);
INSERT INTO `$prefix$category_sub_content` VALUES (7, 1, NULL, 6, 7);
INSERT INTO `$prefix$category_sub_content` VALUES (8, 1, NULL, 6, 8);
INSERT INTO `$prefix$category_sub_content` VALUES (9, 1, NULL, 6, 9);
INSERT INTO `$prefix$category_sub_content` VALUES (10, 1, NULL, 6, 10);
INSERT INTO `$prefix$category_sub_content` VALUES (11, 1, NULL, 6, 11);
INSERT INTO `$prefix$category_sub_content` VALUES (12, 1, NULL, 6, 12);
INSERT INTO `$prefix$category_sub_content` VALUES (13, 1, NULL, 6, 13);
INSERT INTO `$prefix$category_sub_content` VALUES (50, 1, NULL, 11, 14);
INSERT INTO `$prefix$category_sub_content` VALUES (52, 1, NULL, 11, 16);
INSERT INTO `$prefix$category_sub_content` VALUES (59, 1, NULL, 11, 21);
INSERT INTO `$prefix$category_sub_content` VALUES (60, 1, NULL, 11, 20);
INSERT INTO `$prefix$category_sub_content` VALUES (61, 1, NULL, 11, 19);
INSERT INTO `$prefix$category_sub_content` VALUES (62, 1, NULL, 11, 18);
INSERT INTO `$prefix$category_sub_content` VALUES (63, 1, NULL, 11, 17);
INSERT INTO `$prefix$category_sub_content` VALUES (65, 1, NULL, 11, 15);
INSERT INTO `$prefix$category_sub_content` VALUES (66, 1, NULL, 9, 5);
INSERT INTO `$prefix$category_sub_content` VALUES (67, 1, NULL, 10, 5);
INSERT INTO `$prefix$category_sub_content` VALUES (68, 1, NULL, 9, 4);
INSERT INTO `$prefix$category_sub_content` VALUES (69, 1, NULL, 10, 4);
INSERT INTO `$prefix$category_sub_content` VALUES (70, 1, NULL, 9, 3);
INSERT INTO `$prefix$category_sub_content` VALUES (71, 1, NULL, 10, 3);
INSERT INTO `$prefix$category_sub_content` VALUES (72, 1, NULL, 9, 2);
INSERT INTO `$prefix$category_sub_content` VALUES (73, 1, NULL, 10, 2);
INSERT INTO `$prefix$category_sub_content` VALUES (74, 1, NULL, 9, 1);
INSERT INTO `$prefix$category_sub_content` VALUES (75, 1, NULL, 10, 1);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$customer
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$customer`;
CREATE TABLE `$prefix$customer` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `seller_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言标识',
  `lang_pid` int(11) DEFAULT '0' COMMENT '语言锚点',
  `sub_id` bigint(20) unsigned DEFAULT NULL COMMENT '副表ID',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COMMENT='客户模型表';

-- ----------------------------
-- Records of $prefix$customer
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$customer` VALUES (1, 1, 'zh', 0, 6, 1635738391, 1635738391);
INSERT INTO `$prefix$customer` VALUES (2, 1, 'zh', 0, 7, 1635738420, 1635738420);
INSERT INTO `$prefix$customer` VALUES (3, 1, 'zh', 0, 8, 1635738433, 1635738433);
INSERT INTO `$prefix$customer` VALUES (4, 1, 'zh', 0, 9, 1635738501, 1635738501);
INSERT INTO `$prefix$customer` VALUES (5, 1, 'zh', 0, 10, 1635738599, 1635738599);
INSERT INTO `$prefix$customer` VALUES (6, 1, 'zh', 0, 11, 1635738610, 1635738610);
INSERT INTO `$prefix$customer` VALUES (7, 1, 'zh', 0, 12, 1635748035, 1635748035);
INSERT INTO `$prefix$customer` VALUES (8, 1, 'zh', 0, 13, 1635748056, 1635748056);
INSERT INTO `$prefix$customer` VALUES (9, 1, 'zh', 0, 22, 1635779954, 1635779954);
INSERT INTO `$prefix$customer` VALUES (10, 1, 'zh', 0, 23, 1635779986, 1635779986);
INSERT INTO `$prefix$customer` VALUES (11, 1, 'zh', 0, 24, 1635779998, 1635779998);
INSERT INTO `$prefix$customer` VALUES (12, 1, 'zh', 0, 25, 1635780009, 1635780009);
INSERT INTO `$prefix$customer` VALUES (13, 1, 'zh', 0, 26, 1635780027, 1635780027);
INSERT INTO `$prefix$customer` VALUES (14, 1, 'zh', 0, 27, 1635780044, 1635780044);
INSERT INTO `$prefix$customer` VALUES (15, 1, 'zh', 0, 28, 1635780064, 1635780064);
INSERT INTO `$prefix$customer` VALUES (16, 1, 'zh', 0, 29, 1635780080, 1635780080);
INSERT INTO `$prefix$customer` VALUES (17, 1, 'zh', 0, 30, 1635780102, 1635780102);
INSERT INTO `$prefix$customer` VALUES (18, 1, 'zh', 0, 31, 1635780119, 1635780119);
INSERT INTO `$prefix$customer` VALUES (19, 1, 'zh', 0, 32, 1635780152, 1635780152);
INSERT INTO `$prefix$customer` VALUES (20, 1, 'zh', 0, 33, 1635780204, 1635780204);
INSERT INTO `$prefix$customer` VALUES (21, 1, 'zh', 0, 34, 1635780217, 1635780217);
INSERT INTO `$prefix$customer` VALUES (22, 1, 'zh', 0, 35, 1635780229, 1635780229);
INSERT INTO `$prefix$customer` VALUES (23, 1, 'zh', 0, 36, 1635780239, 1635780239);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$database
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$database`;
CREATE TABLE `$prefix$database` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '0' COMMENT '商户ID',
  `title` varchar(255) DEFAULT '' COMMENT '文件名称',
  `size` double(11,2) DEFAULT NULL COMMENT '文件大小',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据库备份表';

-- ----------------------------
-- Records of $prefix$database
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$demo_product
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$demo_product`;
CREATE TABLE `$prefix$demo_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `intro_img` varchar(255) DEFAULT '' COMMENT '产品图集',
  `design_style` varchar(255) DEFAULT '' COMMENT '设计风格',
  `seller_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言标识',
  `lang_pid` int(11) DEFAULT '0' COMMENT '语言锚点',
  `sub_id` bigint(20) unsigned DEFAULT NULL COMMENT '副表ID',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='demo产品模型表';

-- ----------------------------
-- Records of $prefix$demo_product
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$demo_product` VALUES (2, '[\"35\",\"36\"]', '曼海姆SAP竞技场是运动竞技场演化的终极例子。', 1, 'zh', 0, 15, 1635762223, 1635765649);
INSERT INTO `$prefix$demo_product` VALUES (3, '[\"35\",\"36\"]', '家能够完美彰显房子主人的风格特点。在家里，一切风格和功能都按照你的喜好和需求设计', 1, 'zh', 0, 16, 1635762272, 1635762866);
INSERT INTO `$prefix$demo_product` VALUES (4, '[\"35\",\"36\"]', '家能够完美彰显房子主人的风格特点。在家里，一切风格和功能都按照你的喜好和需求设计', 1, 'zh', 0, 17, 1635762327, 1635765612);
INSERT INTO `$prefix$demo_product` VALUES (5, '[\"35\",\"36\"]', '设计优秀', 1, 'zh', 0, 18, 1635762347, 1635765601);
INSERT INTO `$prefix$demo_product` VALUES (6, '[\"35\",\"36\"]', '设计优秀', 1, 'zh', 0, 19, 1635762367, 1635765571);
INSERT INTO `$prefix$demo_product` VALUES (7, '[\"35\",\"36\"]', '设计优秀', 1, 'zh', 0, 20, 1635762385, 1635765556);
INSERT INTO `$prefix$demo_product` VALUES (8, '[\"35\",\"36\"]', '设计优秀', 1, 'zh', 0, 21, 1635762415, 1635765544);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$inner_chart
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$inner_chart`;
CREATE TABLE `$prefix$inner_chart` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '外链ID',
  `seller_id` int(11) DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `type` tinyint(2) DEFAULT '1' COMMENT '内链类型 1 站点标签 2 通用标签',
  `count` tinyint(3) DEFAULT NULL COMMENT '内链类型 1 站点标签 2 通用标签',
  `keyword` varchar(255) DEFAULT NULL COMMENT '关键词',
  `url` varchar(255) DEFAULT NULL COMMENT '内链',
  `weight` int(11) DEFAULT '10' COMMENT '权重 权重越高替换优先级越高',
  `new_page_open` tinyint(2) DEFAULT '1' COMMENT '新窗口打开 1 是 2 否',
  `status` int(11) DEFAULT '1' COMMENT '启用 1 是 2 否',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='内链表';

-- ----------------------------
-- Records of $prefix$inner_chart
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$inner_chart_sub_content
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$inner_chart_sub_content`;
CREATE TABLE `$prefix$inner_chart_sub_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `seller_id` int(10) unsigned DEFAULT NULL COMMENT '商户ID',
  `inner_chart_id` int(10) unsigned DEFAULT NULL COMMENT '内链 id',
  `sub_content_id` int(10) unsigned DEFAULT NULL COMMENT '内容 id',
  `total` tinyint(3) DEFAULT NULL COMMENT '内容内链数量',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章内链关联表';

-- ----------------------------
-- Records of $prefix$inner_chart_sub_content
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$inquiry
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$inquiry`;
CREATE TABLE `$prefix$inquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) DEFAULT '1' COMMENT '所属商户的id',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `company_name` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `contacts_name` varchar(55) DEFAULT NULL COMMENT '联系人姓名',
  `content` varchar(255) DEFAULT NULL COMMENT '询盘内容',
  `telphone` varchar(50) DEFAULT NULL COMMENT '固定电话',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系人手机号',
  `wechat` varchar(120) DEFAULT NULL COMMENT '微信号',
  `email` varchar(80) DEFAULT NULL COMMENT '邮箱',
  `qq` varchar(255) DEFAULT NULL COMMENT 'QQ',
  `inquiry_type` varchar(255) DEFAULT NULL COMMENT '询盘类型',
  `company_address` varchar(200) DEFAULT NULL COMMENT '公司地址',
  `company_site` varchar(155) DEFAULT NULL COMMENT '企业网址',
  `referer` varchar(50) DEFAULT NULL COMMENT '询盘来源',
  `referer_web` varchar(200) DEFAULT NULL COMMENT '来源网址',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip',
  `expect_amount` decimal(13,2) DEFAULT NULL COMMENT '预算金额',
  `explain` text COMMENT '需求说明',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态1默认2转线索',
  `is_del` tinyint(2) DEFAULT '1' COMMENT '1默认2删除',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户询盘表';

-- ----------------------------
-- Records of $prefix$inquiry
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$inquiry_category
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$inquiry_category`;
CREATE TABLE `$prefix$inquiry_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '询盘分类表',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `name` varchar(120) DEFAULT '' COMMENT '分类名称',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='询盘分类表';

-- ----------------------------
-- Records of $prefix$inquiry_category
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$inquiry_category` VALUES (1, 1, '普通询盘', 1635251132, 1635251132);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$inquiry_email
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$inquiry_email`;
CREATE TABLE `$prefix$inquiry_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '邮箱ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1 正常 2 禁用',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='邮箱设置表';

-- ----------------------------
-- Records of $prefix$inquiry_email
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$job
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$job`;
CREATE TABLE `$prefix$job` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '职位ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT '0' COMMENT '网站ID 默认值 0 表示属于所有站点',
  `job_cate_id` int(11) DEFAULT NULL COMMENT '职位分类ID',
  `job_city_id` int(11) DEFAULT NULL COMMENT '职位城市',
  `title` varchar(255) DEFAULT NULL COMMENT '职位名称',
  `description` varchar(255) DEFAULT NULL COMMENT '职位描述',
  `duty` varchar(255) DEFAULT NULL COMMENT '职责',
  `demand` varchar(255) DEFAULT NULL COMMENT '任职要求',
  `salary` varchar(255) DEFAULT NULL COMMENT '薪水',
  `schedule` varchar(255) DEFAULT NULL COMMENT '招聘日期',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态 1 进行中 2 截止',
  `email` varchar(255) DEFAULT '1' COMMENT '邮件发送地址',
  `is_del` varchar(255) DEFAULT '1',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='招聘岗位表';

-- ----------------------------
-- Records of $prefix$job
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$job_cate
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$job_cate`;
CREATE TABLE `$prefix$job_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '职位分类ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `title` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `is_del` tinyint(4) DEFAULT '1',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='岗位分类表';

-- ----------------------------
-- Records of $prefix$job_cate
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$job_cate` VALUES (1, 1, '日常招聘', 1, NULL, 1635406156, 1635406156);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$job_city
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$job_city`;
CREATE TABLE `$prefix$job_city` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '城市ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT ' 商户id',
  `title` varchar(255) DEFAULT '' COMMENT '城市名称',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) unsigned DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='招聘城市表';

-- ----------------------------
-- Records of $prefix$job_city
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$job_city` VALUES (1, 1, '上海', 1, NULL, 1635406156, 1635406156);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$keyword
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$keyword`;
CREATE TABLE `$prefix$keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '关键词ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `type` varchar(255) DEFAULT '' COMMENT '关键词类型',
  `name` varchar(120) DEFAULT '' COMMENT '关键词明名称',
  `url` varchar(255) DEFAULT '' COMMENT '域名地址',
  `position` varchar(255) DEFAULT NULL COMMENT '关键词分布位置',
  `baidu_pc` int(11) DEFAULT NULL COMMENT '百度PC',
  `baidu_mob` int(11) DEFAULT NULL COMMENT '百度移动端',
  `three_pc` int(11) DEFAULT NULL COMMENT '360PC',
  `sougou_mob` int(11) DEFAULT NULL COMMENT '搜狗MOB',
  `baidu_index` int(11) DEFAULT NULL COMMENT '百度指数',
  `baidu_mob_index` int(11) DEFAULT NULL COMMENT '百度移动指数',
  `three_index` int(11) DEFAULT NULL COMMENT '360指数',
  `bidding_intensity` int(11) DEFAULT NULL COMMENT '竞价激烈程度',
  `baidu_search` int(11) DEFAULT NULL COMMENT '百度PC检索量',
  `baidu_mob_search` int(11) DEFAULT NULL COMMENT '百度移动检索量',
  `baidu_included` int(11) DEFAULT NULL COMMENT '百度收录量',
  `long_tail_word` int(11) DEFAULT NULL COMMENT '长尾词个数',
  `sem_price` decimal(10,0) DEFAULT NULL COMMENT 'sem点击价格',
  `bidding_company` int(11) DEFAULT NULL COMMENT '竞价公司数量',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态 1 正常 2 暂停 ',
  `sort` int(11) DEFAULT '1000' COMMENT '排序',
  `is_del` tinyint(4) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='关键词表';

-- ----------------------------
-- Records of $prefix$keyword
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$keyword_query
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$keyword_query`;
CREATE TABLE `$prefix$keyword_query` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '关键词ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `keyword` varchar(120) DEFAULT '' COMMENT '关键词名称',
  `search_engine` varchar(255) DEFAULT NULL COMMENT '搜索引擎',
  `collect_count` int(11) DEFAULT NULL COMMENT '关键词收录量',
  `ip` varchar(255) DEFAULT NULL COMMENT '采集IP',
  `area` varchar(255) DEFAULT NULL COMMENT '采集区域',
  `network` varchar(255) DEFAULT NULL COMMENT '采集网络',
  `top_rank` tinyint(3) DEFAULT NULL COMMENT '最高排名',
  `ranks` text COMMENT '排名数据',
  `page_title` varchar(255) DEFAULT NULL COMMENT '页面标题',
  `page_url` varchar(255) DEFAULT NULL COMMENT '页面链接地址',
  `top100` int(11) DEFAULT NULL COMMENT '网站域名在5118中的Top100关键词排名数量',
  `site_weight` varchar(255) DEFAULT NULL COMMENT '网站权重',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='关键词表';

-- ----------------------------
-- Records of $prefix$keyword_query
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$keyword_website
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$keyword_website`;
CREATE TABLE `$prefix$keyword_website` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `keyword_id` int(11) DEFAULT '0' COMMENT '关键词ID',
  `website_id` int(11) DEFAULT '0' COMMENT '网站ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='关键词网站关联表';

-- ----------------------------
-- Records of $prefix$keyword_website
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$link
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$link`;
CREATE TABLE `$prefix$link` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '友情链接ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(10) unsigned DEFAULT NULL COMMENT '网站ID',
  `name` varchar(120) DEFAULT '' COMMENT '友情链接名称',
  `url` varchar(150) DEFAULT '' COMMENT '友情链接地址',
  `type` tinyint(1) DEFAULT NULL COMMENT '链接类型 1 导入链接 2 导出链接',
  `url_type` tinyint(1) DEFAULT NULL COMMENT '链接类型 1 友情交换 2 链接出售 3 链接购买 4 版权链接',
  `site_type` tinyint(2) DEFAULT NULL COMMENT '网站类型 1 企业站 2 新闻站 3 博客站 4 论坛站 5 门户站',
  `sort` smallint(5) unsigned DEFAULT '10' COMMENT '排序',
  `baidu_weight` int(11) DEFAULT NULL COMMENT '百度权重',
  `position` varchar(255) DEFAULT NULL COMMENT '链接位置',
  `export_link` int(11) DEFAULT NULL COMMENT '导出链接',
  `check_log` tinyint(2) DEFAULT NULL COMMENT '检查记录 1 存在 2 不存在',
  `add_person` varchar(255) DEFAULT NULL COMMENT '添加人',
  `pr_value` int(11) DEFAULT NULL COMMENT 'PR值',
  `contact_person` varchar(255) DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `add_time` date DEFAULT NULL COMMENT '添加时间',
  `end_time` date DEFAULT NULL COMMENT '截止日期',
  `duration` varchar(255) DEFAULT NULL COMMENT '时长',
  `price` varchar(255) DEFAULT NULL COMMENT '价格',
  `exchange_platform` varchar(255) DEFAULT NULL COMMENT '交换平台',
  `target` int(11) NOT NULL DEFAULT '2' COMMENT '链接打开方式 1 本页打开 2 新标签打开',
  `is_del` tinyint(4) NOT NULL DEFAULT '1' COMMENT '删除 1 正常',
  `delete_time` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='友情链接表';

-- ----------------------------
-- Records of $prefix$link
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$link` VALUES (1, 1, 1, '智火营销', 'http://zhco.com', 1, 1, 1, 1, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635476083, 1635476083);
INSERT INTO `$prefix$link` VALUES (2, 1, 1, '导入链接', 'http://yocm.com', 2, 4, 1, 2, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635476308, 1635476308);
INSERT INTO `$prefix$link` VALUES (3, 1, 1, '导出链接2', 'http://pailiu.com', 2, 4, 1, 1, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635476377, 1635476377);
INSERT INTO `$prefix$link` VALUES (4, 1, 2, '智火', 'http://zhco.com', 2, 1, 1, 1, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635782409, 1635782409);
INSERT INTO `$prefix$link` VALUES (5, 1, 2, '派流', 'http://pailiu.com', 2, 1, 1, 2, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635782438, 1635782438);
INSERT INTO `$prefix$link` VALUES (6, 1, 2, '智火传媒', 'http://chuanmeibu.com', 2, 1, 1, 3, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635782510, 1635782510);
INSERT INTO `$prefix$link` VALUES (7, 1, 2, '言鸥', 'http://yocm.com', 2, 1, 1, 4, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635782549, 1635782549);
INSERT INTO `$prefix$link` VALUES (8, 1, 2, '数旗', 'http://suq.com', 2, 1, 1, 5, NULL, '', 0, 1, 'reid', 0, '', '', NULL, NULL, NULL, NULL, '', 2, 1, 0, 1635782602, 1635782602);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$module
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$module`;
CREATE TABLE `$prefix$module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `title` varchar(80) DEFAULT '' COMMENT '模型名称',
  `table` varchar(80) DEFAULT '' COMMENT '用户输入表名',
  `database_table` varchar(120) DEFAULT '' COMMENT '数据库模型对应附表',
  `description` varchar(255) DEFAULT '' COMMENT '描述',
  `type` tinyint(1) DEFAULT '2' COMMENT '模型类型 1 独立模型 2 主表附表模型',
  `is_system` tinyint(1) DEFAULT '2' COMMENT '是否系统模型 1 是 2 不是',
  `status` tinyint(1) DEFAULT '1' COMMENT '1 正常 2 禁用',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='模型表';

-- ----------------------------
-- Records of $prefix$module
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$module` VALUES (1, 1, '系统文章模型', 'article', '$prefix$article', '系统自带的文章模型', 2, 2, 1, 1635478012, 1635478012);
INSERT INTO `$prefix$module` VALUES (2, 1, '客户模型', 'customer', '$prefix$customer', '客户模型', 2, 2, 1, 1635738234, 1635738234);
INSERT INTO `$prefix$module` VALUES (3, 1, 'demo产品模型', 'demo_product', '$prefix$demo_product', '演示站点一的产品模型', 2, 2, 1, 1635760192, 1635760192);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$module_field
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$module_field`;
CREATE TABLE `$prefix$module_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `form_title` varchar(80) DEFAULT '' COMMENT '表单标题',
  `table_field` varchar(80) DEFAULT '' COMMENT '数据表字段',
  `module_id` int(11) DEFAULT '0' COMMENT '模型ID',
  `order` varchar(60) DEFAULT '' COMMENT '数据表排序字段',
  `validate_rule` varchar(120) DEFAULT '' COMMENT '验证规格',
  `form_validate` varchar(255) DEFAULT '' COMMENT '表单验证规则',
  `type` varchar(80) DEFAULT '' COMMENT '字段类型',
  `db_type` varchar(80) DEFAULT '' COMMENT '字段类型',
  `form_type` varchar(80) DEFAULT '' COMMENT '表单类型',
  `attach_data` varchar(255) DEFAULT '' COMMENT '列表数据源',
  `settings` varchar(255) DEFAULT '' COMMENT '其他配置',
  `length` varchar(40) DEFAULT '' COMMENT '字段长度',
  `default` varchar(255) DEFAULT '' COMMENT '默认值',
  `placeholder` varchar(255) DEFAULT '' COMMENT '表单提示信息',
  `status` tinyint(4) DEFAULT '1' COMMENT '字段状态 1 正常 2 隐藏',
  `is_null` tinyint(4) DEFAULT '1' COMMENT '字段是否允许为空 1 允许 其他 不允许',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='模型字段表';

-- ----------------------------
-- Records of $prefix$module_field
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$module_field` VALUES (1, 1, '文章副标题', 'sub_title', 1, 'id', '', '', '单行文本', 'string', 'input', '', '', '', '', '', 1, 1, 1635478171, 1635478171);
INSERT INTO `$prefix$module_field` VALUES (2, 1, '产品图集', 'intro_img', 3, 'id', '', '', '多图', 'string', 'multiImage', '', '', '', '', '', 1, 1, 1635760350, 1635760350);
INSERT INTO `$prefix$module_field` VALUES (3, 1, '设计风格', 'design_style', 3, 'intro_img', '', '', '单行文本', 'string', 'input', '', '', '', '', '', 1, 1, 1635760379, 1635760379);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$nav
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$nav`;
CREATE TABLE `$prefix$nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '导航ID',
  `seller_id` int(11) DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT '0' COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh',
  `parent_id` int(11) DEFAULT '0' COMMENT '父级导航ID',
  `title` varchar(60) DEFAULT '' COMMENT '导航名称',
  `type` tinyint(1) DEFAULT '2' COMMENT '导航类型 1 普通 2 关联栏目',
  `href` varchar(120) DEFAULT NULL COMMENT '导航地址',
  `category_id` int(11) DEFAULT '0' COMMENT '栏目分类ID',
  `nav_cate_id` int(11) DEFAULT '0' COMMENT '导航分类ID',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `target` varchar(255) DEFAULT NULL COMMENT '导航打开方式',
  `no_follow` tinyint (2) DEFAULT 2 COMMENT '是否添加no follow属性 1 是 2 否',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 1 正常 2 禁用',
  `seo_title` varchar(120) DEFAULT '' COMMENT 'seo标题',
  `seo_keyword` varchar(120) DEFAULT '' COMMENT 'seo关键词',
  `seo_descrition` varchar(255) DEFAULT '' COMMENT 'seo描述',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='网站导航表';

-- ----------------------------
-- Records of $prefix$nav
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$nav` VALUES (1, 1, 1, 'zh', 0, '首页', 2, '/', 1, 1, 1, '_self', 1, 1, '', '', '', 1635748149, 1635748149);
INSERT INTO `$prefix$nav` VALUES (2, 1, 1, 'zh', 0, '新闻资讯', 2, '/new', 5, 1, 3, '_self', 1, 1, '', '', '', 1635748198, 1635748198);
INSERT INTO `$prefix$nav` VALUES (3, 1, 1, 'zh', 0, '产品', 2, '/product', 11, 1, 2, '_blank', 1, 1, '', '', '', 1635762933, 1635762933);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$nav_cate
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$nav_cate`;
CREATE TABLE `$prefix$nav_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '导航分类ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh',
  `title` varchar(120) DEFAULT NULL COMMENT '分类名称',
  `description` varchar(255) DEFAULT NULL COMMENT '分类简介',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='导航分类';

-- ----------------------------
-- Records of $prefix$nav_cate
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$nav_cate` VALUES (1, 1, 1, 'zh', '顶部导航', '公共顶部导航', 1635748140, 1635748140);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$recycle_bin
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$recycle_bin`;
CREATE TABLE `$prefix$recycle_bin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `object_id` int(10) unsigned DEFAULT '0' COMMENT '删除内容 id',
  `module_id` int(10) unsigned DEFAULT NULL COMMENT '模型ID',
  `sub_id` int(11) DEFAULT NULL COMMENT '副表ID',
  `table_name` varchar(255) DEFAULT '' COMMENT '删除内容所在表名',
  `title` varchar(255) DEFAULT '' COMMENT '删除内容名称',
  `admin_id` bigint(20) unsigned DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) DEFAULT '' COMMENT '删除人名称',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='回收站';

-- ----------------------------
-- Records of $prefix$recycle_bin
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$role
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$role`;
CREATE TABLE `$prefix$role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `group` tinyint(11) NOT NULL DEFAULT '2' COMMENT '角色分组 1 系统默认 2 用户添加',
  `title` varchar(255) DEFAULT '' COMMENT '角色名称',
  `auth` varchar(500) DEFAULT '' COMMENT '权限',
  `kid_auth` varchar(500) DEFAULT NULL COMMENT '子权限',
  `status` tinyint(1) DEFAULT '1' COMMENT '角色状态 1 正常。2 删除',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='后台角色表';

-- ----------------------------
-- Records of $prefix$role
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$role` VALUES (1, 1, 1, '系统管理员', '*','', 1, 1635406156, 1635406156);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$route
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$route`;
CREATE TABLE `$prefix$route` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `seller_id` int(11) NOT NULL COMMENT '商户ID',
  `category_id` int(11) DEFAULT NULL COMMENT '栏目ID',
  `website_id` int(10) unsigned DEFAULT '1' COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言',
  `sort` int(11) DEFAULT '1000' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态;1:启用,2:不启用',
  `type` tinyint(4) DEFAULT '1' COMMENT 'URL规则类型;1:用户自定义;2:别名添加',
  `full_url` varchar(255) DEFAULT '' COMMENT '完整url',
  `url` varchar(255) DEFAULT '' COMMENT '实际显示的url',
  `pattern` varchar(255) DEFAULT NULL COMMENT '参数正则验证',
  `category_title` varchar(255) DEFAULT '' COMMENT '分类名称',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COMMENT='url路由表';

-- ----------------------------
-- Records of $prefix$route
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$route` VALUES (1, 1, 1, 1, 'zh', 1000, 1, 2, 'List/index?id=1', '/', '','首页', 1635478353, 1635478353);
INSERT INTO `$prefix$route` VALUES (5, 1, 5, 1, 'zh', 1000, 1, 1, 'List/index?id=5', '/new', '','新闻中心', 1635731380, 1635731380);
INSERT INTO `$prefix$route` VALUES (6, 1, 6, 1, 'zh', 1000, 1, 2, 'List/index?id=6', '/customer', '','客户', 1635738289, 1635738289);
INSERT INTO `$prefix$route` VALUES (7, 1, 7, 1, 'zh', 1000, 1, 2, 'List/index?id=7', '/introduction', '','品牌介绍', 1635738972, 1635738972);
INSERT INTO `$prefix$route` VALUES (9, 1, 5, 1, 'zh', 1000, 1, 1, 'Detail/index?cid=5', '/new/:id', '{\"id\":\"\\\\d+\",\"cid\":\"\\\\d+\"}','新闻中心详情页', 1635745148, 1635745148);
INSERT INTO `$prefix$route` VALUES (10, 1, 9, 1, 'zh', 1000, 1, 1, 'List/index?id=9', '/company_new', '','公司新闻', 1635748867, 1635748867);
INSERT INTO `$prefix$route` VALUES (11, 1, 10, 1, 'zh', 1000, 1, 1, 'List/index?id=10', '/industry_new','', '行业资讯', 1635748895, 1635748895);
INSERT INTO `$prefix$route` VALUES (12, 1, 11, 1, 'zh', 1000, 1, 1, 'List/index?id=11', '/product', '','产品', 1635761247, 1635761247);
INSERT INTO `$prefix$route` VALUES (13, 1, 11, 1, 'zh', 1000, 1, 1, 'Detail/index?cid=11', '/product/:id','{\"id\":\"\\\\d+\",\"cid\":\"\\\\d+\"}', '产品详情页', 1635766162, 1635766162);
INSERT INTO `$prefix$route` VALUES (14, 1, 9, 1, 'zh', 1000, 1, 1, 'Detail/index?cid=9', '/company_new/:id','{\"id\":\"\\\\d+\",\"cid\":\"\\\\d+\"}', '公司新闻详情页', 1635771256, 1635771256);
INSERT INTO `$prefix$route` VALUES (15, 1, 10, 1, 'zh', 1000, 1, 1, 'Detail/index?cid=10', '/industry_new/:id','{\"id\":\"\\\\d+\",\"cid\":\"\\\\d+\"}', '行业资讯详情页', 1635771276, 1635771276);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$seller
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$seller`;
CREATE TABLE `$prefix$seller` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商户ID',
  `username` varchar(80) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(120) NOT NULL DEFAULT '' COMMENT '密码',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(80) NOT NULL DEFAULT '' COMMENT '邮箱',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of $prefix$seller
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$seller` VALUES (1, 'default', '040bd08a4290267535cd247b8ba2eca129d9fe9f', '18989898989', 'admin@admin.com', '', 1623295338, 1623295338);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$server
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$server`;
CREATE TABLE `$prefix$server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `server_ip` varchar(255) DEFAULT '' COMMENT '服务器IP地址',
  `ssh_acount` varchar(255) DEFAULT '' COMMENT 'ssh账号',
  `ssh_password` varchar(255) DEFAULT '' COMMENT 'ssh密码',
  `ssh_port` int(11) DEFAULT '22' COMMENT 'ssh端口',
  `sort` int(11) DEFAULT '1000' COMMENT '分类排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '1 正常 2 禁用',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='栏目表';

-- ----------------------------
-- Records of $prefix$server
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$site_map
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$site_map`;
CREATE TABLE `$prefix$site_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'sitemap ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `loc` varchar(255) DEFAULT NULL COMMENT '域名',
  `priority` varchar(255) DEFAULT '0.1' COMMENT '优先级',
  `changefreq` varchar(255) DEFAULT 'weekly' COMMENT '变更周期',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='sitemap 表';

-- ----------------------------
-- Records of $prefix$site_map
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$sitemap_info
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$sitemap_info`;
CREATE TABLE `$prefix$sitemap_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'sitemap ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商家ID',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `sitemap_name` varchar(100) DEFAULT NULL COMMENT 'sitmap文件名称',
  `silian_name` varchar(100) DEFAULT NULL COMMENT '死链文件名称',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='sitemap 文件表';

-- ----------------------------
-- Records of $prefix$sitemap_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$slide
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$slide`;
CREATE TABLE `$prefix$slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '幻灯片ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `slide_cate_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言',
  `title` varchar(120) DEFAULT NULL COMMENT '幻灯片标题',
  `sub_title` varchar(120) DEFAULT NULL COMMENT '幻灯片副标题',
  `url` varchar(120) DEFAULT NULL COMMENT '幻灯片地址',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 1 正常 2 禁用',
  `sort` int(11) DEFAULT '100' COMMENT '排序',
  `attachment` int(11) DEFAULT NULL COMMENT '幻灯片图片',
  `description` varchar(255) DEFAULT NULL COMMENT '分类简介',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='幻灯片列表';

-- ----------------------------
-- Records of $prefix$slide
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$slide_cate
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$slide_cate`;
CREATE TABLE `$prefix$slide_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '幻灯片分类',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh',
  `title` varchar(120) DEFAULT NULL COMMENT '分类名称',
  `description` varchar(255) DEFAULT NULL COMMENT '分类简介',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='幻灯片分类';

-- ----------------------------
-- Records of $prefix$slide_cate
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$social_account
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$social_account`;
CREATE TABLE `$prefix$social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '社交账号ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `name` varchar(255) DEFAULT '' COMMENT '账号用户名',
  `account` varchar(255) DEFAULT '' COMMENT '账号ID',
  `type` tinyint(4) DEFAULT NULL COMMENT '账号类型 1 抖音 2 快手 3 头条 4百家号 5 微信公众号 6 Fackbook 7 Twitter 8 Linkedin 9 YouTube',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交账号表';

-- ----------------------------
-- Records of $prefix$social_account
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$social_marketing
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$social_marketing`;
CREATE TABLE `$prefix$social_marketing` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '社交营销ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `image_id` int(11) DEFAULT NULL COMMENT '二维码/头像存放ID',
  `media_id` varchar(255) DEFAULT NULL COMMENT '媒体账号ID',
  `name` varchar(255) DEFAULT NULL COMMENT '媒体账号名称',
  `url` varchar(255) DEFAULT NULL COMMENT '主页链接',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交营销表';

-- ----------------------------
-- Records of $prefix$social_marketing
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$sub_content
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$sub_content`;
CREATE TABLE `$prefix$sub_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '内容主表ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `module_id` int(11) DEFAULT NULL,
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言',
  `main_id` int(11) DEFAULT NULL COMMENT '主表ID',
  `title` varchar(500) DEFAULT NULL COMMENT '标题',
  `sub_title` varchar(500) DEFAULT NULL COMMENT '副标题',
  `description` varchar(500) DEFAULT NULL,
  `content` longtext,
  `thumbnail` int(11) DEFAULT NULL COMMENT '缩略图',
  `cover` int(11) DEFAULT NULL COMMENT '详情页banner',
  `hits` int(11) DEFAULT '0' COMMENT '浏览数',
  `admin_id` int(11) DEFAULT '0' COMMENT '作者ID',
  `author` varchar(60) DEFAULT '' COMMENT '作者名称',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 1 正常 2 禁用',
  `url` varchar(255) DEFAULT '' COMMENT '访问链接',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `is_top` tinyint(1) DEFAULT '2' COMMENT '置顶 1 置顶 2 否',
  `top_time` int(11) DEFAULT NULL COMMENT '置顶时间',
  `is_recommend` tinyint(1) DEFAULT '2' COMMENT '推荐 1 推荐 2 否',
  `sort` int(11) DEFAULT '10000' COMMENT '排序',
  `detail_tpl` varchar(255) DEFAULT NULL COMMENT '模版',
  `seo_title` varchar(500) DEFAULT '' COMMENT 'seo标题',
  `seo_keyword` varchar(500) DEFAULT '' COMMENT 'seo关键词',
  `seo_description` varchar(500) DEFAULT '' COMMENT 'seo描述',
  `is_del` tinyint(4) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) DEFAULT 0,
  `create_time` int(11) DEFAULT 0,
  `update_time` int(11) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COMMENT='模型副表';

-- ----------------------------
-- Records of $prefix$sub_content
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$sub_content` VALUES (1, 1, 1, 'zh', 6, '哪种材质更适合做定制品', NULL, '近几年，家装界悄然刮起一股定制风，而且这股风潮有愈刮愈烈之势，那么定制品有何优势魅力能征服越来越多的消费者?面对市场上琳琅满目的定制品，哪种材质更适合做定制品呢?', '<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">近几年，家装界悄然刮起一股定制风，而且这股风潮有愈刮愈烈之势，那么定制品有何优势魅力能征服越来越多的消费者?面对市场上琳琅满目的定制品，哪种材质更适合做定制品呢?</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">从经营模式的角度，传统模式是企业追求利润最大化，通过大规模生产来降低产品成本，最终把压力和风险转嫁给经销商，让经销商压库存，搞促销，最后资金压力越来越大，销售利润越来越低，被企业套得越来越深。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">而定制品彻底颠覆了这种模式，因为是一对一定制，所以库存极少，项目不会过多占用经销商资金，通过个性化设计、定制化产品反而会保证经销商的利润，经销商只要做好服务，做好口碑，有企业的强大支撑，生意只会越来越轻松、越来越好做。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">目前市面上的定制品以木质为主，其他材质的发展也很快，如：不锈钢、铝合金等，现在我们就来聊聊近几年发展很快的不锈钢定制品。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">不锈钢材质由于其硬度高加工难度大，表面上色工艺早期无突破导致颜色单一等原因，在5年前采用不锈钢制作定制品的是非常少的。近年来，随着科学技术不断突破，不锈钢的加工工艺及上色难题已经得到非常好的解决了，因此，目前采用不锈钢进行全屋定制品制作已经成为可能并且因为不锈钢天生的一些无法比拟的优势，使得不锈钢定制品越来越得到市场的认可，消费者的青睐。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg\" alt=\"\" width=\"279\" height=\"186\" />&nbsp; &nbsp; &nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg\" alt=\"\" width=\"279\" height=\"186\" /> &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://demo.huocms.com/storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg\" alt=\"\" width=\"278\" height=\"186\" /></p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">我们可以发现，不锈钢由于其天然的材质特性而具备了许多与生俱来无可比拟的优势，同时随着科技的进步各种工艺技术、加工设备的成熟运用，不锈钢早已摆脱传统印象中冰冷笨重、傻大黑粗的大汉形象，变身为俏丽温雅、身怀绝招的邻家小哥，成为追求品质个性的90后、00后客户的首先。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>', 19, 14, 89, 13, 'admin', 1, '', '2021-11-01 14:43:00', 2, 1635736700, 2, 0, NULL, '', '', '', 1, NULL, 1635736625, 1635775276);
INSERT INTO `$prefix$sub_content` VALUES (2, 1, 1, 'zh', 7, '厨房装修五大事项，做好了给你一个舒适的厨房', NULL, '从经营模式的角度，传统模式是企业追求利润最大化，通过大规模生产来降低产品成本，最终把压力和风险转嫁给经销商，让经销商压库存，搞促销，最后资金压力越来越大，销售利润越来越低，被企业套得越来越深。', '<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">近几年，家装界悄然刮起一股定制风，而且这股风潮有愈刮愈烈之势，那么定制品有何优势魅力能征服越来越多的消费者?面对市场上琳琅满目的定制品，哪种材质更适合做定制品呢?</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">从经营模式的角度，传统模式是企业追求利润最大化，通过大规模生产来降低产品成本，最终把压力和风险转嫁给经销商，让经销商压库存，搞促销，最后资金压力越来越大，销售利润越来越低，被企业套得越来越深。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">而定制品彻底颠覆了这种模式，因为是一对一定制，所以库存极少，项目不会过多占用经销商资金，通过个性化设计、定制化产品反而会保证经销商的利润，经销商只要做好服务，做好口碑，有企业的强大支撑，生意只会越来越轻松、越来越好做。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">目前市面上的定制品以木质为主，其他材质的发展也很快，如：不锈钢、铝合金等，现在我们就来聊聊近几年发展很快的不锈钢定制品。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">不锈钢材质由于其硬度高加工难度大，表面上色工艺早期无突破导致颜色单一等原因，在5年前采用不锈钢制作定制品的是非常少的。近年来，随着科学技术不断突破，不锈钢的加工工艺及上色难题已经得到非常好的解决了，因此，目前采用不锈钢进行全屋定制品制作已经成为可能并且因为不锈钢天生的一些无法比拟的优势，使得不锈钢定制品越来越得到市场的认可，消费者的青睐。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg\" alt=\"\" width=\"279\" height=\"186\" />&nbsp; &nbsp; &nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg\" alt=\"\" width=\"279\" height=\"186\" /> &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://demo.huocms.com/storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg\" alt=\"\" width=\"278\" height=\"186\" /></p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">我们可以发现，不锈钢由于其天然的材质特性而具备了许多与生俱来无可比拟的优势，同时随着科技的进步各种工艺技术、加工设备的成熟运用，不锈钢早已摆脱传统印象中冰冷笨重、傻大黑粗的大汉形象，变身为俏丽温雅、身怀绝招的邻家小哥，成为追求品质个性的90后、00后客户的首先。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>', 18, 14, 20, 13, 'admin', 1, '', '2021-11-01 14:42:42', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635737755, 1635775260);
INSERT INTO `$prefix$sub_content` VALUES (3, 1, 1, 'zh', 8, '做不锈钢厨柜就要选有前景的企业', NULL, '现今，家居建材行业竞争激烈，在发展过程中，注定了辉煌与“乱象丛生”并存，调查证明，三四线城市...', '<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">现今，家居建材行业竞争激烈，在发展过程中，注定了辉煌与&ldquo;乱象丛生&rdquo;并存，调查证明，三四线城市...</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">从经营模式的角度，传统模式是企业追求利润最大化，通过大规模生产来降低产品成本，最终把压力和风险转嫁给经销商，让经销商压库存，搞促销，最后资金压力越来越大，销售利润越来越低，被企业套得越来越深。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">而定制品彻底颠覆了这种模式，因为是一对一定制，所以库存极少，项目不会过多占用经销商资金，通过个性化设计、定制化产品反而会保证经销商的利润，经销商只要做好服务，做好口碑，有企业的强大支撑，生意只会越来越轻松、越来越好做。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">目前市面上的定制品以木质为主，其他材质的发展也很快，如：不锈钢、铝合金等，现在我们就来聊聊近几年发展很快的不锈钢定制品。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">不锈钢材质由于其硬度高加工难度大，表面上色工艺早期无突破导致颜色单一等原因，在5年前采用不锈钢制作定制品的是非常少的。近年来，随着科学技术不断突破，不锈钢的加工工艺及上色难题已经得到非常好的解决了，因此，目前采用不锈钢进行全屋定制品制作已经成为可能并且因为不锈钢天生的一些无法比拟的优势，使得不锈钢定制品越来越得到市场的认可，消费者的青睐。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg\" alt=\"\" width=\"279\" height=\"186\" />&nbsp; &nbsp; &nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg\" alt=\"\" width=\"279\" height=\"186\" /> &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://demo.huocms.com/storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg\" alt=\"\" width=\"278\" height=\"186\" /></p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">我们可以发现，不锈钢由于其天然的材质特性而具备了许多与生俱来无可比拟的优势，同时随着科技的进步各种工艺技术、加工设备的成熟运用，不锈钢早已摆脱传统印象中冰冷笨重、傻大黑粗的大汉形象，变身为俏丽温雅、身怀绝招的邻家小哥，成为追求品质个性的90后、00后客户的首先。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>', 17, 0, 7, 13, 'admin', 1, '', '2021-11-01 14:42:24', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635737900, 1635775264);
INSERT INTO `$prefix$sub_content` VALUES (4, 1, 1, 'zh', 9, '舒适智能马桶的定义', NULL, '智能马桶具有齐备的功能很重要，但一个好的智能马桶，质量的优劣也很重要！', '<p>&nbsp;</p>\n<div class=\"news_box_link_in\" style=\"margin: 0px; padding: 0px; border: 0px; font-weight: normal; vertical-align: baseline;\">\n<p style=\"margin: 0px; padding: 0px; border: 0px; font-weight: normal; vertical-align: baseline; font-size: 14px; line-height: 24px; color: #666666; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; font-family: MicrosoftYaHei;\">智能马桶具有齐备的功能很重要，但一个好的智能马桶，质量的优劣也很重要！</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">近几年，家装界悄然刮起一股定制风，而且这股风潮有愈刮愈烈之势，那么定制品有何优势魅力能征服越来越多的消费者?面对市场上琳琅满目的定制品，哪种材质更适合做定制品呢?</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">从经营模式的角度，传统模式是企业追求利润最大化，通过大规模生产来降低产品成本，最终把压力和风险转嫁给经销商，让经销商压库存，搞促销，最后资金压力越来越大，销售利润越来越低，被企业套得越来越深。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">而定制品彻底颠覆了这种模式，因为是一对一定制，所以库存极少，项目不会过多占用经销商资金，通过个性化设计、定制化产品反而会保证经销商的利润，经销商只要做好服务，做好口碑，有企业的强大支撑，生意只会越来越轻松、越来越好做。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">目前市面上的定制品以木质为主，其他材质的发展也很快，如：不锈钢、铝合金等，现在我们就来聊聊近几年发展很快的不锈钢定制品。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">不锈钢材质由于其硬度高加工难度大，表面上色工艺早期无突破导致颜色单一等原因，在5年前采用不锈钢制作定制品的是非常少的。近年来，随着科学技术不断突破，不锈钢的加工工艺及上色难题已经得到非常好的解决了，因此，目前采用不锈钢进行全屋定制品制作已经成为可能并且因为不锈钢天生的一些无法比拟的优势，使得不锈钢定制品越来越得到市场的认可，消费者的青睐。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg\" alt=\"\" width=\"279\" height=\"186\" />&nbsp; &nbsp; &nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg\" alt=\"\" width=\"279\" height=\"186\" /> &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://demo.huocms.com/storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg\" alt=\"\" width=\"278\" height=\"186\" /></p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">我们可以发现，不锈钢由于其天然的材质特性而具备了许多与生俱来无可比拟的优势，同时随着科技的进步各种工艺技术、加工设备的成熟运用，不锈钢早已摆脱传统印象中冰冷笨重、傻大黑粗的大汉形象，变身为俏丽温雅、身怀绝招的邻家小哥，成为追求品质个性的90后、00后客户的首先。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>\n</div>', 16, 0, 80, 13, 'admin', 1, '', '2021-11-01 14:42:04', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738046, 1635822232);
INSERT INTO `$prefix$sub_content` VALUES (5, 1, 1, 'zh', 10, '向好生活提案，你需要一个更健康厕所环境', NULL, '你是不是也经历过这样的苦恼？一边着急上班，一边还要排队等着家里人上厕所', '<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\"><span style=\"color: #666666; font-family: MicrosoftYaHei;\"><span style=\"font-size: 15px;\">你是不是也经历过这样的苦恼？一边着急上班，一边还要排队等着家里人上厕所。。</span></span></p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">近几年，家装界悄然刮起一股定制风，而且这股风潮有愈刮愈烈之势，那么定制品有何优势魅力能征服越来越多的消费者?面对市场上琳琅满目的定制品，哪种材质更适合做定制品呢?</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">从经营模式的角度，传统模式是企业追求利润最大化，通过大规模生产来降低产品成本，最终把压力和风险转嫁给经销商，让经销商压库存，搞促销，最后资金压力越来越大，销售利润越来越低，被企业套得越来越深。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">而定制品彻底颠覆了这种模式，因为是一对一定制，所以库存极少，项目不会过多占用经销商资金，通过个性化设计、定制化产品反而会保证经销商的利润，经销商只要做好服务，做好口碑，有企业的强大支撑，生意只会越来越轻松、越来越好做。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">目前市面上的定制品以木质为主，其他材质的发展也很快，如：不锈钢、铝合金等，现在我们就来聊聊近几年发展很快的不锈钢定制品。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">不锈钢材质由于其硬度高加工难度大，表面上色工艺早期无突破导致颜色单一等原因，在5年前采用不锈钢制作定制品的是非常少的。近年来，随着科学技术不断突破，不锈钢的加工工艺及上色难题已经得到非常好的解决了，因此，目前采用不锈钢进行全屋定制品制作已经成为可能并且因为不锈钢天生的一些无法比拟的优势，使得不锈钢定制品越来越得到市场的认可，消费者的青睐。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/5d4c96927de514cec71c984f6b97e083.jpg\" alt=\"\" width=\"279\" height=\"186\" />&nbsp; &nbsp; &nbsp; <img src=\"http://demo.huocms.com/storage/image/20211101/47f987cf9af227a2d9b18ad8cf6d127d.jpg\" alt=\"\" width=\"279\" height=\"186\" /> &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://demo.huocms.com/storage/image/20211101/17003e2dc2243a5c38fe9ae4a8c23735.jpg\" alt=\"\" width=\"278\" height=\"186\" /></p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">我们可以发现，不锈钢由于其天然的材质特性而具备了许多与生俱来无可比拟的优势，同时随着科技的进步各种工艺技术、加工设备的成熟运用，不锈钢早已摆脱传统印象中冰冷笨重、傻大黑粗的大汉形象，变身为俏丽温雅、身怀绝招的邻家小哥，成为追求品质个性的90后、00后客户的首先。</p>\n<p style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; color: #666666; font-family: MicrosoftYaHei; font-size: 15px; background-color: #ffffff;\">&nbsp;</p>', 15, 14, 111, 13, 'admin', 1, '', '2021-11-01 14:41:48', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738144, 1635822252);
INSERT INTO `$prefix$sub_content` VALUES (6, 1, 2, 'zh', 1, '客户一', NULL, NULL, '', 7, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:21:23', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738391, 1635738391);
INSERT INTO `$prefix$sub_content` VALUES (7, 1, 2, 'zh', 2, '客户二', NULL, NULL, '', 8, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:21:27', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738420, 1635738420);
INSERT INTO `$prefix$sub_content` VALUES (8, 1, 2, 'zh', 3, '客户三', NULL, NULL, '', 9, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:21:31', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738433, 1635738433);
INSERT INTO `$prefix$sub_content` VALUES (9, 1, 2, 'zh', 4, '客户四', NULL, NULL, '', 10, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:21:34', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738501, 1635738501);
INSERT INTO `$prefix$sub_content` VALUES (10, 1, 2, 'zh', 5, '客户五', NULL, NULL, '', 11, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:21:38', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738599, 1635738599);
INSERT INTO `$prefix$sub_content` VALUES (11, 1, 2, 'zh', 6, '客户六', NULL, NULL, '', 12, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:21:41', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635738610, 1635738610);
INSERT INTO `$prefix$sub_content` VALUES (12, 1, 2, 'zh', 7, '客户七', NULL, NULL, '', 8, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:27:15', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635748035, 1635748035);
INSERT INTO `$prefix$sub_content` VALUES (13, 1, 2, 'zh', 8, '客户八', NULL, NULL, '', 10, 0, 0, 13, 'admin', 1, '', '2021-11-01 14:27:36', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635748056, 1635748056);
INSERT INTO `$prefix$sub_content` VALUES (15, 1, 3, 'zh', 2, '高端浴缸', NULL, '在古代文化中，竞技场，作为力量比拼的场所，承载着狂热的宗教意义。尽管这类观赏性运动的传统还保留至今，但现在再也不会为过去简单的沙竞技场提供充足的空间。 ', '', 23, 29, 10, 13, 'admin', 1, '', '2021-11-01 18:23:43', 1, 1635762895, 2, 0, NULL, '', '', '', 1, NULL, 1635762223, 1635775040);
INSERT INTO `$prefix$sub_content` VALUES (16, 1, 3, 'zh', 3, '龙头组合', NULL, NULL, '', 0, 0, 0, 13, 'admin', 1, '', '2021-11-01 18:24:32', 2, NULL, 2, 0, NULL, '', '', '', 2, 1635762866, 1635762272, 1635762866);
INSERT INTO `$prefix$sub_content` VALUES (17, 1, 3, 'zh', 4, '龙头组合', NULL, '一体式置物架 / 出水口配置:1 个右边单孔 / 安装类型:下沉式', '', 27, 29, 2, 13, 'admin', 1, '', '2021-11-01 18:25:27', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635762327, 1635773230);
INSERT INTO `$prefix$sub_content` VALUES (18, 1, 3, 'zh', 5, '老式混水阀', NULL, '一体式置物架 / 出水口配置:1 个右边单孔 / 安装类型:下沉式', '', 28, 29, 1, 13, 'admin', 1, '', '2021-11-01 18:25:47', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635762347, 1635773233);
INSERT INTO `$prefix$sub_content` VALUES (19, 1, 3, 'zh', 6, '面盆龙头', NULL, '现代简约圆形艺术洗手盆，卫生间陶瓷', '', 24, 29, 0, 13, 'admin', 1, '', '2021-11-01 18:26:07', 1, 1635762861, 2, 0, NULL, '', '', '', 1, NULL, 1635762367, 1635765571);
INSERT INTO `$prefix$sub_content` VALUES (20, 1, 3, 'zh', 7, '菜盆龙头', NULL, 'B.1系列线条圆润，设计优雅经典，于功能之上呈献舒适体验，操作简易并拥有通用于各个卫浴功能的适配龙头，包括盥洗，淋浴，沐浴以及便洗。', '', 25, 29, 1, 13, 'admin', 1, '', '2021-11-01 18:26:25', 2, NULL, 2, 0, NULL, '', '', '', 1, NULL, 1635762385, 1635766294);
INSERT INTO `$prefix$sub_content` VALUES (21, 1, 3, 'zh', 8, '酒店马桶', NULL, '适配私人浴室或是客用浴室的柔和造型：B.1系列线条圆润，设计优雅经典，于功能之上呈献舒适体验，操作简易并拥有通用于各个卫浴功能的适配龙头，包括盥洗，淋浴，沐浴以及便洗。', '', 26, 29, 0, 13, 'admin', 1, '', '2021-11-01 18:26:55', 1, 1635762853, 2, 0, NULL, '', '', '', 1, NULL, 1635762415, 1635765544);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$sys_setting
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$sys_setting`;
CREATE TABLE `$prefix$sys_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统设置ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `parent_id` int(10) unsigned DEFAULT '0' COMMENT '分组ID',
  `group` varchar(255) DEFAULT '' COMMENT '配置分组',
  `form_title` varchar(255) DEFAULT '' COMMENT '表单名称',
  `title` varchar(255) DEFAULT '' COMMENT '配置名称',
  `value` varchar(255) DEFAULT '' COMMENT '配置值',
  `remark` varchar(255) DEFAULT '' COMMENT '描述',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态',
  `sort` int(11) DEFAULT '100' COMMENT '排序',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COMMENT='系统配置';

-- ----------------------------
-- Records of $prefix$sys_setting
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$sys_setting` VALUES (1, 1, 0, 'upload', '文件上传设置', 'upload_setting', '', '', 1, 2, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (2, 1, 1, 'upload', '最大上传个数', 'max_files', '4', '多文件上传，单次可上传个数', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (3, 1, 1, 'upload', '分片上传，文件大小', 'chunk_size', '1024', '设置分片文件大小(单位kb)', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (4, 1, 1, 'upload', '图片大小设置', 'image_size', '10240', '设置图片上传大小(单位kb)', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (5, 1, 1, 'upload', '图片格式设置', 'image_ext', 'webp,jpg,png,ico,bmp,gif,jpeg', '设置上传图片类型', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (6, 1, 1, 'upload', '视频大小设置', 'video_size', '10240', '设置视频上传大小(单位kb)', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (7, 1, 1, 'upload', '视频类型设置', 'video_ext', 'mp4,avi,3gp,rmvb,mov', '设置视频上传类型', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (8, 1, 1, 'upload', '音频大小设置', 'audio_size', '10240', '设置音频上传大小(单位kb)', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (9, 1, 1, 'upload', '音频类型设置', 'audio_ext', 'mp3,mpeg,wma,mid', '设置音频上传类型', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (10, 1, 1, 'upload', '压缩包设置', 'zip_size', '10240', '设置上传文件大小(单位kb)', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (11, 1, 1, 'upload', '压缩包类型设置', 'zip_ext', 'rar,zip', '设置文件上传类型', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (12, 1, 1, 'upload', '分片上传触发', 'chunk_condition', '51200', '触发分片上传大小(单位kb)', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (13, 1, 1, 'upload', '文件存储设置', 'storage', 'local', '', 1, 20, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (14, 1, 13, 'upload', '本地存储', 'local', 'local', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (15, 1, 13, 'upload', '阿里云', 'aliyun', 'aliyun', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (16, 1, 13, 'upload', '七牛云', 'qiniu', 'qiniu', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (17, 1, 0, 'email', '系统邮箱设置', 'sys_email_setting', '', '', 1, 1, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (18, 1, 17, 'email', '发件箱账号', 'email_send_account', 'test@admin.com', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (19, 1, 17, 'email', '发件箱密码', 'email_password', 'Zhihuo2020', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (20, 1, 17, 'email', '发件箱类型', 'email_type', 'ali', '163，新浪，阿里，腾讯，QQ', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (21, 1, 17, 'email', '收件箱账号', 'email_get_account', 'test@admin.com', '', 1, 12, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (23, 1, 0, 'sem', 'SEM账号设置', 'sem_setting', '', '', 2, 3, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (25, 1, 23, 'sem', '百度', 'baidu_sem', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (26, 1, 25, 'sem', '账号', 'baidu_sem_account', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (27, 1, 25, 'sem', '密码', 'baidu_sem_passowrd', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (28, 1, 25, 'sem', 'Token', 'baidu_sem_token', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (29, 1, 23, 'sem', '谷歌', 'google_sem', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (30, 1, 29, 'sem', '账号', 'google_sem_account', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (31, 1, 29, 'sem', '密码', 'google_sem_passowrd', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (32, 1, 29, 'sem', 'Token', 'google_sem_token', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (33, 1, 23, 'sem', '搜狗', 'sougou_sem', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (34, 1, 33, 'sem', '账号', 'sougou_sem_account', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (35, 1, 33, 'sem', '密码', 'sougou_sem_password', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (36, 1, 33, 'sem', 'Token', 'sougou_sem_token', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (37, 1, 23, 'sem', '360', '360_sem', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (38, 1, 37, 'sem', '账号', '360_sem_account', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (39, 1, 37, 'sem', '密码', '360_sem_password', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (40, 1, 37, 'sem', 'Token', '360_sem_token', '', '', 2, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (45, 1, 0, 'analysis', '统计分析账号', 'analysis_setting', '', '', 1, 4, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (46, 1, 45, 'analysis', '百度统计', 'baidu_analysis', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (47, 1, 46, 'analysis', '账号', 'baidu_analysis_account', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (48, 1, 46, 'analysis', '密码', 'baidu_analysis_password', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (49, 1, 46, 'analysis', 'token', 'baidu_analysis_token', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (50, 1, 45, 'analysis', '谷歌统计', 'google_analysis', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (51, 1, 50, 'analysis', '账号', 'google_analysis_account', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (52, 1, 50, 'analysis', '密码', 'google_analysis_password', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (53, 1, 50, 'analysis', 'token', 'google_analysis_token', '', '', 1, 100, 1625731449, 1625731449);
INSERT INTO `$prefix$sys_setting` VALUES (54, 1, 16, 'upload', '接收密钥ID', 'qiniu_accessKeyId', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (55, 1, 16, 'upload', '接收密钥密码', 'qiniu_accessKeySecret', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (56, 1, 16, 'upload', '根地址', 'qiniu_storageRoot', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (57, 1, 16, 'upload', '容器名', 'qiniu_bucket', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (58, 1, 16, 'upload', '协议头', 'qiniu_protocolHeader', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (59, 1, 13, 'upload', '腾讯云', 'tencent', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (60, 1, 59, 'upload', '接收密钥ID', 'tencent_accessKeyId', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (61, 1, 59, 'upload', '接收密钥密码', 'tencent_accessKeySecret', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (62, 1, 59, 'upload', '地区', 'tencent_region', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (63, 1, 59, 'upload', '容器名', 'tencent_bucket', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (64, 1, 59, 'upload', '协议头', 'tencent_protocolHeader', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (65, 1, 15, 'upload', '接收密钥ID', 'aliyun_accessKeyId', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (66, 1, 15, 'upload', '接收密钥密码', 'aliyun_accessKeySecret', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (67, 1, 15, 'upload', '根地址', 'aliyun_storageRoot', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (68, 1, 15, 'upload', '容器名', 'aliyun_bucket', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (69, 1, 20, 'sys_email', '阿里企业邮箱', 'ali', 'ali', '', 1, 201, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (70, 1, 20, 'sys_email', '新浪邮箱', 'sina', 'sina', '', 1, 202, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (71, 1, 20, 'sys_email', '163邮箱', '163', '163', '', 1, 203, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (72, 1, 20, 'sys_email', 'QQ邮箱', 'qq', 'qq', '', 1, 204, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (73, 1, 20, 'sys_email', 'office365邮箱', '365', '365', '', 1, 205, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (75, 1, 1, 'upload', '文件大小', 'file_size', '10240', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (76, 1, 1, 'upload', '文件类型', 'file_ext', 'exe,doc,ppt,xls,wps,txt,json,xlsx', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (78, 1, 0, 'others', '第三方配置', 'other_settings', '', '其他第三方配置', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (79, 1, 78, 'others', 'Suwork配置', 'suwork', '', '', 1, 781, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (80, 1, 79, 'others', 'Suwork APPID', 'suwork_appid', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (81, 1, 79, 'others', 'Suwork 密钥', 'suwork_secret', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (82, 1, 78, 'others', '关键词排名查询配置', 'keywords_search', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (83, 1, 82, 'others', '站长工具', 'toolz', '', '', 2, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (84, 1, 82, 'others', '5118', 'five_one_one', '', '', 1, 783, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (85, 1, 78, 'others', '谷歌翻译配置', 'google_trans_token', '', '', 2, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (86, 1, 84, 'others', '百度PC', 'five_baidu_pc', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (87, 1, 84, 'others', '百度移动', 'five_baidu_mob', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (88, 1, 84, 'others', '360PC', 'haosou_pc', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (89, 1, 84, 'others', '搜狗移动', 'sougou_mob', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (90, 1, 84, 'others', '自动查询频率', 'keywords_search_time', 'day', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (91, 1, 0, 'company', '公司配置', 'company_setting', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (92, 1, 91, 'company', '公司名称', 'company_name', '智火信息科技有限公司', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (93, 1, 91, 'company', '公司地址', 'company_address', '上海市隆昌路城市概念8号楼南区C07', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (94, 1, 91, 'company', '公司电话', 'company_phone', '4000-700360', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (95, 1, 91, 'company', '公司邮箱', 'company_email', 'vip@zhco.com', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (96, 1, 78, 'others', '词爪配置', 'cizhua', '', '', 1, 782, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (97, 1, 96, 'others', '词爪 APPID', 'cizhua_appid', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (98, 1, 96, 'others', '词爪 密钥', 'cizhua_secret', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (99, 1, 78, 'others', '百度翻译', 'baidu_translate', '', '', 1, 784, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (100, 1, 99, 'others', 'APP ID', 'baidu_translate_appid', '', '', 1, 991, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (101, 1, 99, 'others', 'Token', 'baidu_translate_token', '', '', 1, 992, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (102, 1, 78, 'others', 'sitemap密钥', 'sitemap_token', '', '', 1, 785, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (103, 1, 102, 'others', '百度密钥', 'baidu_sitemap_token', '', '', 1, 1021, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (104, 1, 102, 'others', '谷歌密钥', 'google_sitemap_token', '', '', 1, 1022, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (105, 1, 102, 'others', '360密钥', 'three_sitemap_token', '', '', 1, 1023, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (106, 1, 102, 'others', '搜狗密钥', 'sogou_sitemap_token', '', '', 1, 1024, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (107, 1, 79, 'others', 'Suwork Api', 'suwork_api', '', '', 1, 99, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$tag
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$tag`;
CREATE TABLE `$prefix$tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `seller_id` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `lang` varchar(255) DEFAULT NULL COMMENT '语言',
  `title` varchar(255) DEFAULT '' COMMENT '标签名称',
  `unique_tag` varchar(255) DEFAULT NULL COMMENT '标签唯一标识',
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '状态,1:正常,2:隐藏',
  `first_letter` char(1) DEFAULT NULL COMMENT '排序',
  `sort` tinyint(3) DEFAULT NULL COMMENT '排序',
  `is_recommend` tinyint(3) unsigned DEFAULT '2' COMMENT '是否推荐;1:推荐;2:不推荐',
  `article_count` bigint(20) unsigned DEFAULT '0' COMMENT '标签文章数',
  `tag_hit` int(10) unsigned DEFAULT '0' COMMENT '标签点击数',
  `seo_title` varchar(255) DEFAULT '' COMMENT 'title',
  `seo_keywords` varchar(255) DEFAULT '' COMMENT 'keywords',
  `seo_description` varchar(255) DEFAULT '' COMMENT 'description',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='文章标签表';

-- ----------------------------
-- Records of $prefix$tag
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$tag` VALUES (1, 1, 2, 'zh', '用户体验', NULL, 1, NULL, NULL, 2, 0, 0, '', '', '', 1, 0, 1635780788, 1635780788);
INSERT INTO `$prefix$tag` VALUES (2, 1, 2, 'zh', '交互设计', NULL, 1, NULL, NULL, 2, 0, 0, '', '', '', 1, 0, 1635780797, 1635780797);
INSERT INTO `$prefix$tag` VALUES (3, 1, 2, 'zh', '移动平台开发', NULL, 1, NULL, NULL, 2, 0, 0, '', '', '', 1, 0, 1635780808, 1635780808);
INSERT INTO `$prefix$tag` VALUES (4, 1, 2, 'zh', '自适应', NULL, 1, NULL, NULL, 2, 0, 0, '', '', '', 1, 0, 1635780814, 1635780814);
COMMIT;

-- ----------------------------
-- Table structure for $prefix$tag_sub_content
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$tag_sub_content`;
CREATE TABLE `$prefix$tag_sub_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `seller_id` int(10) unsigned DEFAULT NULL COMMENT '商户ID',
  `tag_id` int(10) unsigned DEFAULT NULL COMMENT '标签 id',
  `sub_content_id` int(10) unsigned DEFAULT NULL COMMENT '内容 id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章标签关联表';

-- ----------------------------
-- Records of $prefix$tag_sub_content
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$test_example
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$test_example`;
CREATE TABLE `$prefix$test_example` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `seller_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言标识',
  `lang_pid` int(11) DEFAULT '0' COMMENT '语言锚点',
  `sub_id` bigint(20) unsigned DEFAULT NULL COMMENT '副表ID',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='测试模型表';

-- ----------------------------
-- Records of $prefix$test_example
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$theme
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$theme`;
CREATE TABLE `$prefix$theme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `seller_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(10) unsigned DEFAULT '1' COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh',
  `is_active` tinyint(3) unsigned DEFAULT '2' COMMENT '模板状态 1 正在使用 2 未使用 ',
  `theme` varchar(255) DEFAULT '' COMMENT '主题目录名，用于主题的唯一标识',
  `name` varchar(120) DEFAULT '' COMMENT '主题名称',
  `description` varchar(255) DEFAULT '' COMMENT '主题介绍',
  `keywords` varchar(255) DEFAULT '' COMMENT '主题关键词',
  `author` varchar(255) DEFAULT '' COMMENT '主题作者',
  `url` varchar(255) DEFAULT '' COMMENT '预览地址',
  `thumb` varchar(60) DEFAULT '' COMMENT '缩略图',
  `sort` tinyint(3) unsigned DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主题表';

-- ----------------------------
-- Records of $prefix$theme
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$theme_file
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$theme_file`;
CREATE TABLE `$prefix$theme_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板文件ID',
  `seller_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT '1' COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh',
  `theme_id` int(11) DEFAULT NULL COMMENT '模板ID',
  `file` varchar(255) DEFAULT NULL COMMENT '模板路径，相对根目录',
  `real_path` varchar(255) DEFAULT NULL COMMENT '模版路径',
  `name` varchar(255) DEFAULT '' COMMENT '模板文件名称',
  `description` varchar(255) DEFAULT '' COMMENT '文件描述',
  `action` varchar(255) DEFAULT '' COMMENT '操作',
  `theme_name` varchar(255) DEFAULT NULL COMMENT '模板名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `is_public` tinyint(3) unsigned DEFAULT '2' COMMENT '公共模块 1 是 2 否',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模板文件配置表';

-- ----------------------------
-- Records of $prefix$theme_file
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$visit_log
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$visit_log`;
CREATE TABLE `$prefix$visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1',
  `referrer` varchar(255) DEFAULT NULL COMMENT '来源网址',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `domain` varchar(255) DEFAULT NULL COMMENT '域名',
  `ip` varchar(255) DEFAULT NULL COMMENT '访问IP',
  `url` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `visited_time` date DEFAULT NULL COMMENT '访问日期',
  `visited_area` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `visited_device` varchar(255) DEFAULT NULL COMMENT '访问设备',
  `agent` varchar(1000) NOT NULL DEFAULT '',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='前台用户访问日志表';

-- ----------------------------
-- Records of $prefix$visit_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$website
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$website`;
CREATE TABLE `$prefix$website` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '网站ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `title` varchar(255) DEFAULT '1' COMMENT '网站名称',
  `domain` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `parent_id` int(11) DEFAULT '0' COMMENT '父站点',
  `sitemap_url` varchar(255) DEFAULT NULL COMMENT 'sitemap 地址',
  `silian_url` varchar(255) DEFAULT NULL COMMENT '死链地址',
  `status` tinyint(4) DEFAULT '1' COMMENT '网站状态 1 正常 2 禁用',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='网站表';

-- ----------------------------
-- Records of $prefix$website
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$website_inquiry_email
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$website_inquiry_email`;
CREATE TABLE `$prefix$website_inquiry_email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1',
  `website_id` int(11) DEFAULT NULL,
  `inquiry_email_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='收件箱网站关联表';

-- ----------------------------
-- Records of $prefix$website_inquiry_email
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$website_lang
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$website_lang`;
CREATE TABLE `$prefix$website_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '网站语言关联表ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT '0' COMMENT '网站ID',
  `title` varchar(255) DEFAULT '' COMMENT '语言名称',
  `lang` varchar(30) DEFAULT '' COMMENT '语言标识',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='网站语言关联表';

-- ----------------------------
-- Records of $prefix$website_lang
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$website_server
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$website_server`;
CREATE TABLE `$prefix$website_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '1 本地服务器 2 自定义服务器',
  `server_ip` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器IP地址',
  `ssh_account` varchar(255) NOT NULL DEFAULT '' COMMENT 'ssh账号',
  `ssh_password` varchar(255) NOT NULL DEFAULT '' COMMENT 'ssh密码',
  `ssh_port` int(11) NOT NULL DEFAULT '22' COMMENT 'ssh端口',
  `root_path` varchar(255) DEFAULT NULL,
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='栏目表';

-- ----------------------------
-- Records of $prefix$website_server
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$website_setting
-- ----------------------------
DROP TABLE IF EXISTS `$prefix$website_setting`;
CREATE TABLE `$prefix$website_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '网站设置ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh' COMMENT '语言',
  `setting` text NOT NULL COMMENT '网站设置',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='网站设置表';

-- ----------------------------
-- Records of $prefix$website_setting
-- ----------------------------
BEGIN;
COMMIT;


-- 角色站点关联表
DROP TABLE IF EXISTS `$prefix$role_website`;
CREATE TABLE `$prefix$role_website` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id` int(11) DEFAULT NULL COMMENT '商户ID',
    `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
    `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
    PRIMARY KEY (`id`) USING BTREE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色站点关联表';

DROP TABLE IF EXISTS `$prefix$seo_account`;
CREATE TABLE `$prefix$seo_account` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账号id',
    `type` tinyint(2) DEFAULT NULL COMMENT '账号类型 1:百度 2:谷歌',
    `account` varchar(255) DEFAULT NULL COMMENT '账号',
    `password` varchar(255) DEFAULT NULL COMMENT '密码',
    `token` varchar(255) DEFAULT NULL COMMENT 'token',
    `status` tinyint(2) DEFAULT NULL COMMENT '状态 1:启用 2:禁用',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='seo分析账号';

DROP TABLE IF EXISTS `$prefix$baidu_tj_gather`;
CREATE TABLE `$prefix$baidu_tj_gather` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `pv_count` int(11) DEFAULT NULL COMMENT '总pv数',
    `uv_count` int(11) DEFAULT NULL COMMENT '总uv数',
    `ip_count` int(11) DEFAULT NULL COMMENT '总ip数',
    `avg_visit_time` int(11) DEFAULT NULL COMMENT '平均访问时长(秒)',
    `count_date` date DEFAULT NULL COMMENT '统计日期',
    PRIMARY KEY (`id`),
    KEY `idx_date` (`count_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='百度统计汇总';

DROP TABLE IF EXISTS `$prefix$seo_check_task`;
CREATE TABLE `$prefix$seo_check_task` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务id',
    `seller_id` int(11) DEFAULT '1' COMMENT '商户id',
    `website_id` int(11) DEFAULT NULL COMMENT '站点id',
    `website_url` varchar(155) DEFAULT NULL COMMENT '站点url',
    `lang` varchar(55) DEFAULT NULL COMMENT '语言',
    `theme` varchar(155) DEFAULT NULL COMMENT '模板',
    `status` tinyint(2) DEFAULT '1' COMMENT '状态 1:检测中 2:已完成',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='seo诊断任务表';

DROP TABLE IF EXISTS `$prefix$seo_check_task_detail`;
CREATE TABLE `$prefix$seo_check_task_detail` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '详情id',
    `seller_id` int(11) DEFAULT '1' COMMENT '所属商户id',
    `task_id` int(11) DEFAULT NULL COMMENT '所属任务id',
    `status` tinyint(2) DEFAULT NULL COMMENT '状态 1:诊断中 2:已完成',
    `code` varchar(15) DEFAULT NULL COMMENT '检测类型',
    `remark` text COMMENT '检测信息',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='seo诊断任务详情表';

DROP TABLE IF EXISTS `$prefix$poster_material`;
CREATE TABLE `$prefix$poster_material` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '素材id',
    `name` varchar(155) DEFAULT NULL COMMENT '素材名称',
    `source` varchar(255) DEFAULT NULL COMMENT '素材地址',
    `width` int(11) DEFAULT NULL COMMENT '素材宽度px',
    `height` int(11) DEFAULT NULL COMMENT '素材高度px',
    `type` varchar(55) DEFAULT NULL COMMENT '素材格式类型',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='海报素材';

DROP TABLE IF EXISTS `$prefix$poster`;
CREATE TABLE `$prefix$poster` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '海报id',
    `name` varchar(155) DEFAULT NULL COMMENT '海报的名字',
    `preview` varchar(255) DEFAULT NULL COMMENT '海报封面',
    `status` tinyint(2) DEFAULT '1' COMMENT '状态1:启用 2:禁用',
    `design_content` longtext COMMENT '海报设计详情',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='营销海报管理';

DROP TABLE IF EXISTS `$prefix$diy_form`;
CREATE TABLE `$prefix$diy_form` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表单id',
    `seller_id` int(11) DEFAULT '0' COMMENT '商户id',
    `code` varchar(32) DEFAULT NULL COMMENT '表单唯一标识',
    `table` varchar(255) DEFAULT NULL COMMENT '该表单生成的表名',
    `name` varchar(155) DEFAULT NULL COMMENT '表单名称',
    `desc` varchar(255) DEFAULT NULL COMMENT '表单描述',
    `design_content` longtext COMMENT '设计内容',
    `design_option` longtext COMMENT '表单配置数据',
    `status` tinyint(2) DEFAULT '1' COMMENT '1:未发布  2:已发布',
    `type` tinyint(2) DEFAULT '1' COMMENT '1:表单  2:问卷',
    `is_del` tinyint(2) DEFAULT '1' COMMENT '是否被删除 1:正常 2:删除',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`),
    KEY `idx_code` (`code`) USING BTREE COMMENT '表单标识',
    KEY `idx_table` (`table`) USING BTREE COMMENT '关联的表'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='营销表单';

DROP TABLE IF EXISTS `$prefix$down`;
CREATE TABLE `$prefix$down` (
   `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `form_id` int(11) DEFAULT NULL COMMENT '表单id',
   `file` varchar(255) DEFAULT '' COMMENT '文件',
   `is_form` varchar(255) DEFAULT '否' COMMENT '填表单下载？',
   `seller_id` bigint(20) unsigned DEFAULT NULL COMMENT '商户ID',
   `lang` varchar(255) DEFAULT 'zh' COMMENT '语言标识',
   `lang_pid` int(11) DEFAULT '0' COMMENT '语言锚点',
   `sub_id` bigint(20) unsigned DEFAULT NULL COMMENT '副表ID',
   `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
   `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载表';

INSERT INTO `$prefix$module` (`id`, `seller_id`, `title`, `table`, `database_table`, `description`, `type`, `is_system`, `status`, `create_time`, `update_time`) VALUES (5, 1, '下载', 'down', '$prefix$down', '下载', 2, 2, 1, 1656640092, 1656640092);

INSERT INTO `$prefix$module_field` (`id`, `seller_id`, `form_title`, `table_field`, `module_id`, `order`, `validate_rule`, `form_validate`, `type`, `db_type`, `form_type`, `attach_data`, `settings`, `length`, `default`, `placeholder`, `status`, `is_null`, `create_time`, `update_time`) VALUES (16, 1, '关联表单', 'form_id', 5, 'id', '', '', '关联字段', 'int', 'reference', '', '{\"relation\":\"has_one\",\"table\":\"diy_form\",\"foreign_key\":\"id\",\"local_key\":\"form_id\",\"table_name\":\"$prefix$diy_form\",\"local_table\":\"down\"}', '', '', '', 1, 1, 1656644805, 1656644805);
INSERT INTO `$prefix$module_field` (`id`, `seller_id`, `form_title`, `table_field`, `module_id`, `order`, `validate_rule`, `form_validate`, `type`, `db_type`, `form_type`, `attach_data`, `settings`, `length`, `default`, `placeholder`, `status`, `is_null`, `create_time`, `update_time`) VALUES (17, 1, '文件', 'file', 5, 'form_id', '', '', '多文件', 'string', 'multiFile', '', '', '', '', '', 1, 1, 1656645238, 1656645238);
INSERT INTO `$prefix$module_field` (`id`, `seller_id`, `form_title`, `table_field`, `module_id`, `order`, `validate_rule`, `form_validate`, `type`, `db_type`, `form_type`, `attach_data`, `settings`, `length`, `default`, `placeholder`, `status`, `is_null`, `create_time`, `update_time`) VALUES (18, 1, '填表单下载？', 'is_form', 5, 'file', 'require', '', '单选', 'string', 'radio', '是,否', '', '', '否', '请选择是否填表单下载', 1, 1, 1656645323, 1656645323);

ALTER TABLE `$prefix$theme_file` ADD COLUMN `is_design` tinyint(2) NULL DEFAULT 0 AFTER `is_public`;
ALTER TABLE `$prefix$theme_file` ADD COLUMN `design_path` varchar(255) NULL DEFAULT '' COMMENT '设计页面路径' AFTER `is_design`;

DROP TABLE IF EXISTS `$prefix$plugin`;
CREATE TABLE `$prefix$plugin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `seller_id` int(11) unsigned DEFAULT '0' COMMENT '商户ID',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '插件类型;1:网站;',
  `has_admin` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台管理,0:没有;1:有',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:开启;2:禁用',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '插件安装时间',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '插件标识名,英文字母(惟一)',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件名称',
  `demo_url` varchar(50) NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `event` varchar(255) NOT NULL DEFAULT '' COMMENT '实现的事件;以“,”分隔',
  `author` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件作者',
  `author_url` varchar(50) NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '插件版本号',
  `description` varchar(255) NOT NULL COMMENT '插件描述',
  `config` text COMMENT '插件配置',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='插件表';

-- 默认安装api接口
INSERT INTO `$prefix$plugin` (`id`, `seller_id`, `type`, `has_admin`, `status`, `create_time`, `name`, `title`, `demo_url`, `event`, `author`, `author_url`, `version`, `description`, `config`) VALUES (3, 0, 1, 0, 1, 1662091232, 'api', 'api接口', 'http://www.demo_huocms.com', '', 'huocms', 'http://www.huocms.com', '1.0.0', 'app/小程序 api接口', '{\"custom_config\":\"0\",\"text\":\"hello,ThinkCMF!\",\"password\":\"\",\"number\":\"1.0\",\"select\":\"1\",\"checkbox\":1,\"radio\":\"1\",\"radio2\":\"1\",\"textarea\":\"\\u8fd9\\u91cc\\u662f\\u4f60\\u8981\\u586b\\u5199\\u7684\\u5185\\u5bb9\",\"date\":\"2017-05-20\",\"datetime\":\"2017-05-20\",\"color\":\"#103633\",\"image\":\"\",\"file\":\"\",\"location\":\"\"}');
-- 默认安装会员模块和虚拟资产模块
INSERT INTO `$prefix$plugin` (`id`, `type`, `has_admin`, `status`, `create_time`, `name`, `title`, `demo_url`, `event`, `author`, `author_url`, `version`, `description`, `config`) VALUES (1, 1, 1, 0, 1662081231, 'user', '会员模块', 'http://www.demo_huocms.com', '', 'huocms', 'http://www.huocms.com', '1.0.0', '用户会员模块管理，支持小程序/公众号登录', '[]');
INSERT INTO `$prefix$plugin` (`id`, `type`, `has_admin`, `status`, `create_time`, `name`, `title`, `demo_url`, `event`, `author`, `author_url`, `version`, `description`, `config`) VALUES (2, 1, 1, 0, 1662081587, 'assets', '虚拟资产模块', 'http://www.demo_huocms.com', '', 'huocms', 'http://www.huocms.com', '1.0.0', '管理虚拟资产：域名、主机、商标、版权', '[]');

DROP TABLE IF EXISTS `$prefix$user_group`;
CREATE TABLE `$prefix$user_group` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title` varchar(120) NOT NULL DEFAULT '' COMMENT '分类名称',
    `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户分类表';

DROP TABLE IF EXISTS `$prefix$user_grade`;
CREATE TABLE `$prefix$user_grade` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title` varchar(120) NOT NULL DEFAULT '' COMMENT '等级名称',
    `icon` int(11) NOT NULL DEFAULT 0 COMMENT '等级图标',
    `min_score` int(11) NOT NULL DEFAULT '0' COMMENT '最低积分值',
    `max_score` int(11) NOT NULL DEFAULT '0' COMMENT '最大积分值',
    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户等级表';


DROP TABLE IF EXISTS `$prefix$user`;
CREATE TABLE `$prefix$user` (
    `id`                  int(11) unsigned NOT NULL AUTO_INCREMENT,
    `seller_id`           int(11) unsigned DEFAULT '0' COMMENT '商户ID',
    `user_group`          int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属用户分类',
    `user_grade`          int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员等级',
    `sex`                 tinyint(2) NOT NULL DEFAULT '0' COMMENT '性别;0:保密,1:男,2:女',
    `birthday`            varchar(20)                                                 NOT NULL DEFAULT '' COMMENT '生日',
    `score`               int(11) NOT NULL DEFAULT '0' COMMENT '用户积分',
    `coin`                int(10) unsigned NOT NULL DEFAULT '0' COMMENT '金币',
    `balance`             decimal(10, 2)                                               NOT NULL DEFAULT '0.00' COMMENT '余额',
    `status`              tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '用户状态;1:正常,2:未验证,3:禁用',
    `username`            varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
    `password`            varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '登录密码;加密',
    `nickname`            varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
    `email`               varchar(100)                                                 NOT NULL DEFAULT '' COMMENT '用户登录邮箱',
    `user_url`            varchar(100)                                                 NOT NULL DEFAULT '' COMMENT '用户个人网址',
    `avatar`              varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '用户头像',
    `signature`           varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '个性签名',
    `last_login_ip`       varchar(15)                                                  NOT NULL DEFAULT '' COMMENT '最后登录ip',
    `last_login_time`     datetime                                                              DEFAULT NULL COMMENT '最后登录时间',
    `user_activation_key` varchar(60)                                                  NOT NULL DEFAULT '' COMMENT '激活码',
    `mobile`              varchar(20)                                                  NOT NULL DEFAULT '' COMMENT '中国手机不带国家代码，国际手机号格式为：国家代码-手机号',
    `more`                text COMMENT '扩展属性',
    `address`             varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '省市区',
    `job`                 varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '职业',
    `create_time`         datetime                                                              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`         datetime                                                              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`),
    KEY                   `username` (`username`),
    KEY                   `nickname` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

DROP TABLE IF EXISTS `$prefix$user_collection`;
CREATE TABLE `$prefix$user_collection` (
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(11) unsigned DEFAULT '0' COMMENT '用户id',
    `article_id`  int(11) unsigned DEFAULT '0' COMMENT '文章的id',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户的收藏列表';

DROP TABLE IF EXISTS `$prefix$user_login`;
CREATE TABLE `$prefix$user_login` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '登录日志ID',
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `website_id` int(11) DEFAULT NULL COMMENT '站点id',
    `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '站点用户ID',
    `username` varchar(120) NOT NULL DEFAULT '' COMMENT '用户名称',
    `ip` varchar(120) NOT NULL DEFAULT '' COMMENT '登录IP',
    `login_address` varchar(255) NOT NULL DEFAULT '' COMMENT '登录地址',
    `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
    `agent` varchar(255) NOT NULL DEFAULT '' COMMENT '登录客户端',
    `login_time` int(11) NOT NULL DEFAULT '0' COMMENT '登录时间',
    `logout_time` int(11) NOT NULL DEFAULT '0' COMMENT '登出时间',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户登录日志';

DROP TABLE IF EXISTS `$prefix$assets_domain`;
CREATE TABLE `$prefix$assets_domain` (
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`     int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `domain`        varchar(100)   NOT NULL DEFAULT '' COMMENT '域名',
    `desc`          varchar(255)   NOT NULL DEFAULT '' COMMENT '描述',
    `holder_name`   varchar(120)   NOT NULL DEFAULT '' COMMENT '持有者',
    `registrants`   varchar(120)   NOT NULL DEFAULT '' COMMENT '注册商',
    `price`         decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '价格',
    `status`        varchar(120)   NOT NULL DEFAULT '' COMMENT '域名状态',
    `register_date` datetime                DEFAULT NULL COMMENT '注册时间',
    `expire_date`   datetime                DEFAULT NULL COMMENT '到期时间',
    `hint`          varchar(255)   NOT NULL DEFAULT '' COMMENT '提示',
    `whois`         text COMMENT 'whois详情',
    `sort`          int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `user_id`       int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
    `create_time`   datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`   datetime                DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-域名表';


DROP TABLE IF EXISTS `$prefix$assets_server`;
CREATE TABLE `$prefix$assets_server` (
    `id`               int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`        int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title`            varchar(120)   NOT NULL DEFAULT '' COMMENT '名称',
    `desc`             varchar(255)   NOT NULL DEFAULT '' COMMENT '描述',
    `public_ip`        varchar(20)    NOT NULL DEFAULT '' COMMENT '公网ip',
    `service_provider` varchar(120)   NOT NULL DEFAULT '' COMMENT '服务商',
    `price`            decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '价格',
    `os`               varchar(120)   NOT NULL DEFAULT '' COMMENT '操作系统',
    `buy_date`         datetime                DEFAULT NULL COMMENT '购买时间',
    `expire_date`      datetime                DEFAULT NULL COMMENT '到期时间',
    `hint`             varchar(255)   NOT NULL DEFAULT '' COMMENT '提示',
    `user_id`          int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
    `create_time`      datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`      datetime                DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `public_ip` (`public_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-服务器表';

DROP TABLE IF EXISTS `$prefix$assets_trademark`;
CREATE TABLE `$prefix$assets_trademark` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title` varchar(120) NOT NULL DEFAULT '' COMMENT '名称',
    `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
    `category` varchar(255) NOT NULL DEFAULT '' COMMENT '分类（所属行业）',
    `apply_code` varchar(50) NOT NULL DEFAULT '' COMMENT '申请/注册号',
    `apply_user` varchar(120) NOT NULL DEFAULT '' COMMENT '申请人',
    `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
    `valid_time` datetime DEFAULT NULL COMMENT '有效时间',
    `agency` varchar(255) NOT NULL DEFAULT '' COMMENT '代理机构',
    `certificate_img` varchar(255) NOT NULL DEFAULT '' COMMENT '证书图片',
    `trademark_img` varchar(255) NOT NULL DEFAULT '' COMMENT '商标图片',
    `issuing_authority` varchar(255) NOT NULL DEFAULT '' COMMENT '发证机关',
    `register_address` varchar(255) NOT NULL DEFAULT '' COMMENT '注册地址',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `apply_code` (`apply_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-商标表';

DROP TABLE IF EXISTS `$prefix$assets_copyright`;
CREATE TABLE `$prefix$assets_copyright` (
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`     int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `category`      tinyint(2) NOT NULL DEFAULT '1' COMMENT '版权类别 1采购2自有',
    `type`          tinyint(2) NOT NULL DEFAULT '1' COMMENT '类型 1图片2音频3视频4字体',
    `apply_user`    varchar(120) NOT NULL DEFAULT '' COMMENT '创建人',
    `price`         decimal(15, 2)        DEFAULT '0.00' COMMENT '价格',
    `doc_no`        varchar(220)          DEFAULT '' COMMENT '授权文件号',
    `use_range`     varchar(255)          DEFAULT '' COMMENT '使用范围',
    `subject`       varchar(255)          DEFAULT '' COMMENT '授权的主体',
    `supplier_name` varchar(255)          DEFAULT '' COMMENT '供应商名称',
    `material_type` tinyint(2) DEFAULT '1' COMMENT '1录入网址 2上传文件',
    `begin_time`    date                  DEFAULT NULL COMMENT '开始时间',
    `end_time`      date                  DEFAULT NULL COMMENT '结束时间',
    `files`         text COMMENT '授权文件',
    `apply_id`      int(11) DEFAULT '0' COMMENT '创建人id',
    `create_time`   datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`   datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-版权表';

DROP TABLE IF EXISTS `$prefix$assets_copyright_detail`;
CREATE TABLE `$prefix$assets_copyright_detail` (
    `detail_id`      int(11) unsigned NOT NULL AUTO_INCREMENT,
    `seller_id`      int(11) DEFAULT '1' COMMENT '商户的id',
    `copyright_id`   int(11) unsigned DEFAULT '0' COMMENT '版权的id',
    `material_type`  tinyint(2) DEFAULT '1' COMMENT '1录入网址 2上传文件',
    `material_no`    varchar(220) DEFAULT '' COMMENT '素材编号',
    `material_name`  varchar(220) DEFAULT '' COMMENT '素材名称',
    `material_link`  varchar(255) DEFAULT '' COMMENT '素材内容',
    `material_cover` varchar(255) DEFAULT '' COMMENT '素材封面',
    `material_url`   varchar(255) DEFAULT '' COMMENT '使用网址',
    `category`       tinyint(2) DEFAULT '1' COMMENT '类别 1采购2自有',
    `type`           tinyint(2) DEFAULT '1' COMMENT '类型 1图片2视频 3音频 4字体',
    `create_time`    datetime     DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版权的素材信息';

DROP TABLE IF EXISTS `$prefix$assets_soft`;
CREATE TABLE `$prefix$assets_soft` (
    `id`           int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`    int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title`        varchar(120) NOT NULL DEFAULT '' COMMENT '证书名称',
    `reg_code`     varchar(50)  NOT NULL DEFAULT '' COMMENT '登记号',
    `code`         varchar(50)  NOT NULL DEFAULT '' COMMENT '证书编号',
    `apply_user`   varchar(120) NOT NULL DEFAULT '' COMMENT '著作权人',
    `right_scope`  varchar(255) NOT NULL DEFAULT '' COMMENT '权利范围',
    `grant_office` varchar(255) NOT NULL DEFAULT '' COMMENT '发证机关',
    `grant_time`   date                  DEFAULT NULL COMMENT '发证日期',
    `grant_file`   varchar(255) NOT NULL DEFAULT '' COMMENT '登记证书url',
    `user_id`      int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
    `create_time`  datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`  datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-软著表';

INSERT INTO `$prefix$sys_setting` (`id`, `seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (108, 1, 84, 'others', '站长之家 Token', 'chinaz_token', '', '', 1, 100, 0, 0);

ALTER TABLE `$prefix$keyword_query`
    ADD COLUMN `keyword_id` int(11) NULL DEFAULT 0 COMMENT '关键词id' AFTER `website_id`;

ALTER TABLE `$prefix$category`
    ADD COLUMN `need_check` tinyint(2) NOT NULL DEFAULT 2 COMMENT '需要审核？1需要2不需要' AFTER `is_search`;

DROP TABLE IF EXISTS `$prefix$category_check`;
CREATE TABLE `$prefix$category_check` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `category_id` int(11) DEFAULT '0' COMMENT '栏目ID',
    `check_users` varchar(255) DEFAULT '0' COMMENT '审核用户ids',
    `step` tinyint(4) DEFAULT '1' COMMENT '当前所属审核步骤数',
    `examine_mode` tinyint(2) DEFAULT '1' COMMENT '审核方式：1或签2会签',
    `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态：1正常2作废',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='栏目审核配置表';

DROP TABLE IF EXISTS `$prefix$sub_content_check`;
CREATE TABLE `$prefix$sub_content_check` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `category_check_id` int(11) NOT NULL DEFAULT '0' COMMENT '审核配置ID',
    `real_id` int(11) NOT NULL DEFAULT '0' COMMENT '内容ID',
    `version` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '内容版本号',
    `check_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '审核用户id',
    `step` tinyint(4) NOT NULL DEFAULT '1' COMMENT '当前所属审核步骤',
    `examine_mode` tinyint(2) NOT NULL DEFAULT '1' COMMENT '审核模式 1或签2会签',
    `oper_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作人id',
    `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态：1待审核2审核中3通过4拒绝5作废',
    `reason` varchar(500) NOT NULL DEFAULT '' COMMENT '拒绝原因 ',
    `files` varchar(255) NOT NULL DEFAULT '' COMMENT '附件',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='审核明细表';

DROP TABLE IF EXISTS `$prefix$sub_content_check_step`;
CREATE TABLE `$prefix$sub_content_check_step` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `real_id` int(11) DEFAULT '0' COMMENT '内容id',
    `version` int(11) unsigned DEFAULT NULL COMMENT '内容版本号',
    `check_id` int(11) DEFAULT '0' COMMENT '审核步骤id',
    `user_id` int(11) DEFAULT '0' COMMENT '操作用户id',
    `content` varchar(500) DEFAULT '' COMMENT '进度说明',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='审核进度表';

ALTER TABLE `$prefix$sub_content`
    ADD COLUMN `check_status` tinyint(2) NOT NULL DEFAULT '3' COMMENT '审核状态？1待审核2审核中3审核通过4审核拒绝5下线' AFTER `status`,
    ADD COLUMN `del_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '删除操作人id' AFTER `is_del`;

ALTER TABLE `$prefix$sub_content`
    ADD COLUMN `version` int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT '当前版本号' AFTER `seo_description`;

ALTER TABLE `$prefix$sub_content`
    ADD COLUMN `show_version` int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT '展示内容的版本号' AFTER `version`;

DROP TABLE IF EXISTS `$prefix$sub_content_history`;
CREATE TABLE `$prefix$sub_content_history` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '内容历史记录表ID',
    `sub_content_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '内容主表ID',
    `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `category_id` int(11) NOT NULL DEFAULT '0' COMMENT '主栏目id',
    `sub_category_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '副栏目ids，逗号拼接',
    `module_id` int(11) DEFAULT NULL,
    `lang` varchar(255) DEFAULT 'zh' COMMENT '语言',
    `main_id` int(11) DEFAULT NULL COMMENT '主表ID',
    `title` varchar(255) DEFAULT NULL COMMENT '标题',
    `sub_title` varchar(255) DEFAULT NULL COMMENT '副标题',
    `description` varchar(255) DEFAULT NULL,
    `content` text,
    `thumbnail` int(11) DEFAULT NULL COMMENT '缩略图',
    `cover` int(11) DEFAULT NULL COMMENT '详情页banner',
    `hits` int(11) DEFAULT '0' COMMENT '浏览数',
    `admin_id` int(11) DEFAULT '0' COMMENT '作者ID',
    `author` varchar(60) DEFAULT '' COMMENT '作者名称',
    `status` tinyint(4) DEFAULT '1' COMMENT '状态 1 正常 2 禁用',
    `check_status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '审核状态？1待审核2审核中3审核通过4审核拒绝5下线',
    `url` varchar(255) DEFAULT '' COMMENT '访问链接',
    `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
    `is_top` tinyint(1) DEFAULT '2' COMMENT '置顶 1 置顶 2 否',
    `top_time` int(11) DEFAULT NULL COMMENT '置顶时间',
    `is_recommend` tinyint(1) DEFAULT '2' COMMENT '推荐 1 推荐 2 否',
    `sort` int(11) DEFAULT '10000' COMMENT '排序',
    `detail_tpl` varchar(255) DEFAULT NULL COMMENT '模版',
    `seo_title` varchar(255) DEFAULT '' COMMENT 'seo标题',
    `seo_keyword` varchar(255) DEFAULT '' COMMENT 'seo关键词',
    `seo_description` varchar(255) DEFAULT '' COMMENT 'seo描述',
    `version` int(255) unsigned DEFAULT '1' COMMENT '内容版本号',
    `is_del` tinyint(4) DEFAULT '1' COMMENT '删除 1 正常 2 删除',
    `del_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '删除操作人id',
    `delete_time` int(11) DEFAULT 0,
    `create_time` int(11) DEFAULT 0,
    `update_time` int(11) DEFAULT 0,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='内容历史记录表';

ALTER TABLE `$prefix$sub_content`
    ADD COLUMN `category_id` int(11) NOT NULL DEFAULT 0 COMMENT '主栏目id' AFTER `seller_id`,
    ADD COLUMN `sub_category_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '副栏目ids，逗号拼接' AFTER `category_id`;

UPDATE `$prefix$sub_content` SET `category_id` = 9 WHERE `id` = 1;
UPDATE `$prefix$sub_content` SET `category_id` = 9 WHERE `id` = 2;
UPDATE `$prefix$sub_content` SET `category_id` = 9 WHERE `id` = 3;
UPDATE `$prefix$sub_content` SET `category_id` = 9 WHERE `id` = 4;
UPDATE `$prefix$sub_content` SET `category_id` = 9 WHERE `id` = 5;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 6;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 7;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 8;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 9;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 10;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 11;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 12;
UPDATE `$prefix$sub_content` SET `category_id` = 6 WHERE `id` = 13;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 15;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 16;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 17;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 18;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 19;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 20;
UPDATE `$prefix$sub_content` SET `category_id` = 11 WHERE `id` = 21;

INSERT INTO `$prefix$sys_setting` VALUES (109, 1, 78, 'others', '阿里短信AccessKeyId', 'ali_access_key_id', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (110, 1, 78, 'others', '阿里短信AccessKeySecret', 'ali_access_key_secret', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (111, 1, 78, 'others', '阿里短信模板CODE', 'ali_template_code', '', '', 1, 100, 0, 0);
INSERT INTO `$prefix$sys_setting` VALUES (112, 1, 78, 'others', '阿里短信签名名称', 'ali_sign_name', '', '', 1, 100, 0, 0);

ALTER TABLE `$prefix$role`
MODIFY COLUMN `auth` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限' AFTER `title`,
MODIFY COLUMN `kid_auth` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '子权限' AFTER `auth`;

ALTER TABLE `$prefix$admin`
ADD COLUMN `phone` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号' AFTER `account`,
ADD COLUMN `code_auth` tinyint(2) NOT NULL DEFAULT 2 COMMENT '是否需要短信验证' AFTER `password`;

DROP TABLE IF EXISTS `$prefix$phone_code`;
CREATE TABLE `$prefix$phone_code` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `phone` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
    `code` varchar(6) NOT NULL DEFAULT '' COMMENT '验证码',
    `used` tinyint(2) NOT NULL DEFAULT '2' COMMENT '是否已使用',
    `expired_time` datetime DEFAULT NULL COMMENT '过期时间',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='手机验证码表';

ALTER TABLE `$prefix$category`
ADD COLUMN `field_list` text NULL COMMENT '栏目自定义字段，json格式' AFTER `update_time`;

ALTER TABLE `$prefix$theme_file`
ADD COLUMN `file_hash` varchar(255) NOT NULL DEFAULT '' COMMENT '模版文件hash' AFTER `file`;

INSERT INTO `$prefix$sys_setting` VALUES (113, 1, 91, 'company', '版权标识', 'huocms_powerby', 'Powered by HuoCMS', '', 1, 100, 0, 0);

