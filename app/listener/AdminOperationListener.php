<?php
declare (strict_types = 1);

namespace app\listener;

use app\model\AdminOptLog;
use app\model\Model;

class AdminOperationListener
{
    /**
     * 事件监听处理
     *
     * @return mixed
     * @throws \app\exception\ModelException
     */
    public function handle($event)
    {
        (new AdminOptLog())->addAdminOptLog($event);
    }
}
