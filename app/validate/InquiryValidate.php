<?php


namespace app\validate;


use think\Validate;

class InquiryValidate extends Validate
{
    protected $rule = [
        'id|询盘ID' => 'require',
        'website_id|站点ID'=>'require|number',
        'phone|手机号' => 'mobile',
        'email|邮箱' => 'email',
        'qq|QQ号' => 'number',
        'ids|询盘ID' => 'require|array',
    ];


    protected $scene = [
        'read' => ['id'],
        'save' => ['website_id', 'phone', 'email', 'qq'],
        'update' => ['id', 'website_id', 'phone', 'email', 'qq'],
        'batch' => ['ids'],
    ];
}