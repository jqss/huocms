<?php

namespace app\service;


use app\exception\ModelException;
use app\model\Attachment;
use app\model\Category;
use app\model\CategoryCheck;
use app\model\Route;
use app\model\WebsiteLang;
use think\facade\Cache;
use think\facade\Db;

class CategoryService
{

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws ModelException
     */
    public function readCategory($param): \think\response\Json
    {
        $category = new Category();
        $res = $category->getCategory($param);
        if (!empty($res['data']['thumbnail'])) {
            $attachment = new Attachment();
            $whereThumb = ['id' => $res['data']['thumbnail']];
            $thumbnail = $attachment->getAttachment($whereThumb)['data'];
            $res['data']['thumbnail'] = $thumbnail;
        }
        return json($res);
    }

    /**
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function createCategory(&$param): \think\response\Json
    {
        $category = new Category();
        Db::startTrans();
        try {
            // 非单页面不能添加content数据
            if ($param['type'] != 4) {
                $param['content'] = '';
            }
            // 完善栏目数据
            if ($param['parent_id']) {
                $parentCate = $category->getCategory(['id' => $param['parent_id'], 'seller_id' => $param['seller_id']])['data'];
                $param['path'] = $parentCate['path'] . $param['parent_id'] . '-';
            } else {
                $param['path'] = 0 . '-';
            }
            $maxOrder = $category->getMaxOrderCategory(['parent_id' => $param['parent_id']], 'sort')['data'];
            if (!empty($maxOrder)) {
                $param['sort'] = (int)$maxOrder + 10;
            }
            if (!empty($param['parent_map'])) {
                $route = new Route();
                $route->where(['seller_id' => 1, 'website_id' => $param['website_id'], 'category_id' => $param['parent_map']])->delete();
                $param['has_map'] = 1;
            }
            if (isset($param['alias'])) {
                $param['alias'] = $this->dealWithAlias($param['alias']);
            }
            $checkList = $param['check_list'] ?? [];
            unset($param['check_list']);

            $cate = $category->addCategory($param);
            CacheService::deleteRelationCacheByObject(Category::class);
            // 美化url
            if (!empty($param['alias']) && in_array($param['type'], [1, 4])) {
                $routeParam = [
                    'alias' => $param['alias'],
                    'type' => $param['type'],
                    'lang' => $param['lang'],
                    'sort' => 1000,
                    'website_id' => $param['website_id'],
                    'seller_id' => $param['seller_id'],
                    'title' => $cate['data']['title'],
                    'detail_sort' => 4999,
                    'id' => $cate['data']['id']
                ];

                $this->setRoute($routeParam);
            }
            // 审核相关操作
            if ($param['need_check'] == 1) {
                $data = [];
                foreach ($checkList as $key => $value) {
                    $data[] = [
                        'category_id' => $cate['data']['id'],
                        'seller_id' => $param['seller_id'],
                        'check_users' => $value['check_users'],
                        'examine_mode' => $value['examine_mode'],
                        'step' => $key + 1,
                    ];
                }
                (new CategoryCheck())->saveAll($data);
            }
            // 操作日志
            optEventLog($cate['data']['id'], lang('栏目'), lang('添加'));
            Db::commit();
            Cache::clear();
        } catch (ModelException $me) {
            Db::rollback();
            return jsonReturn(-5, $me->getMessage());
        } catch (\Exception $e) {
            Db::rollback();
            return jsonReturn(-6, $e->getMessage());
        }
        return json($cate);
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws ModelException
     */
    public function updateCategory($param): \think\response\Json
    {
        $category = new Category();
        $cate = $category->getCategory(['id' => $param['id'], 'seller_id' => $param['seller_id']])['data'];
        Db::startTrans();
        try {
            // 非单页面不能添加content数据
            if ($param['type'] != 4) {
                $param['content'] = '';
            }
            if (!empty($param['parent_map'])) {
                $param['has_map'] = 1;
            }
            if (isset($param['alias'])) {
                $param['alias'] = $this->dealWithAlias($param['alias']);
            }
            if ($cate['title'] != $param['title']) {
                $cate->route()->update(['category_title' => $param['title']]);
            }
            if ($param['type'] != $cate['type']) {
                $cate->route()->delete();
            }
            if ($param['type'] == 1 || $param['type'] == 4) {
                $cate->route()->delete();
                $routeParam = [
                    'alias' => $param['alias'],
                    'type' => $param['type'],
                    'sort' => 1000,
                    'lang' => $param['lang'],
                    'website_id' => $param['website_id'],
                    'seller_id' => $cate['seller_id'],
                    'title' => $param['title'],
                    'detail_sort' => 4999,
                    'id' => $param['id'],
                ];
                $this->setRoute($routeParam);
            }
            if ($cate['parent_id'] != $param['parent_id']) {
                // 当前栏目路径
                if ($param['parent_id'] == 0) {
                    $parentCate['path'] = 0;
                } else {
                    $parentCate = $category->getCategory(['id' => $param['parent_id'], 'seller_id' => $param['seller_id']])['data'];
                }
                $param['path'] = $parentCate['path'] . $param['parent_id'] . '-';
                // 子栏目路径
                $subCate = $category->getAllCustomArrayData([['seller_id', '=', $param['seller_id']], ['path', 'like', $cate . '%']])['data'];
                foreach ($subCate as &$val) {
                    $val['path'] = str_replace($cate['path'], $param['path'], $val['path']);
                }
                $category->saveAll($subCate);
            }
            if ($param['module_id'] !== $cate['module_id'] && $param['parent_id'] == 0) {
                $category->updateCategory(['parent_id' => $cate['id']], ['module_id' => $param['module_id']]);
            }

            $checkList = $param['check_list'] ?? [];
            unset($param['check_list']);

            $category->updateCategory(['id' => $param['id'], 'seller_id' => $param['seller_id']], $param);
            $res = $category->getCategory(['id' => $param['id'], 'seller_id' => $param['seller_id']]);
            CacheService::deleteRelationCacheByObject(Category::class);
            // 更新审核配置，原有审核流作废，已经进入审核流程的就还按原来的审核流程
            $categoryCheckModel = new CategoryCheck();
            $categoryCheckModel->updateCustomData([
                ['seller_id', '=', $param['seller_id']],
                ['category_id', '=', $param['id']],
                ['status', '=', 1]
            ], ['status' => 2]);
            if (isset($param['need_check']) && $param['need_check'] == 1) {
                $data = [];
                foreach ($checkList as $key => $value) {
                    $data[] = [
                        'category_id' => $param['id'],
                        'seller_id' => $param['seller_id'],
                        'check_users' => $value['check_users'],
                        'examine_mode' => $value['examine_mode'],
                        'step' => $key + 1,
                    ];
                }
                (new CategoryCheck())->saveAll($data);
            }

            optEventLog($param['id'], lang('栏目'), lang('更新'));
            Db::commit();
            Cache::clear();
        } catch (ModelException $me) {
            Db::rollback();
            return jsonReturn(-7, $me->getMessage());
        } catch (\Exception $e) {
            Db::rollback();
            return jsonReturn(-8, $e->getMessage());
        }
        return json($res);
    }

    /**
     * @throws ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function setRoute($param)
    {
        $route = new Route();
        $route->setRoute($param['alias'], 'List/index', ['id' => $param['id']], $param['lang'], $param['type'], $param['sort'], $param['seller_id'], $param['id'], $param['website_id'], $param['title']);
        if ($param['type'] == 1) {
            $pattern = json_encode(['id' => '\d+', 'cid' => '\d+']);
            $route->setRoute($param['alias'] . '/:id', 'Detail/index', ['cid' => $param['id']], $param['lang'], $param['type'], $param['sort'], $param['seller_id'], $param['id'], $param['website_id'], $param['title'] . '详情页', $pattern);
        }
        $route->getRoutes($param['website_id'], $param['seller_id'], $param['lang']);
    }

    public function dealWithAlias($alias): string
    {
        $alias = trim($alias);
        if (strlen($alias) >= 1) {
            $tmpAlias = rtrim($alias, '/');
            if (strlen($tmpAlias)) {
                $alias = $tmpAlias;
            } else {
                $alias = '/';
            }
        } else {
            $alias = '';
        }
        return $alias;
    }

    /**
     * @throws \Exception
     */
    public function copyCate($siteId, $targetSite, $lang, $sellerId): array
    {
        // 获取所有的列表
        $Category = new Category();
        $category = $Category->getAllCustomArrayData(['seller_id' => $sellerId, 'website_id' => $siteId, 'lang' => $lang], 'id asc')['data'];
        $ids = array_column($category, 'id');
        $cateCount = count($category);
        Db::startTrans();
        try {
            foreach ($targetSite as $target) {
                // 查看看点是否有当前语言
                $WebsiteLang = new WebsiteLang();

                $langFlag = $WebsiteLang->getWebsiteLang(['seller_id' => $sellerId, 'website_id' => $target, 'lang' => $lang])['data'];

                if (empty($langFlag)) {
                    // 新增语言配置
                    $websiteService = new WebsiteLangService();
                    $websiteService->cateCopyAddLang($lang, $sellerId, $target);
                }
                // 删除原来的站点的栏目
                $Category->delCategory(['seller_id' => $sellerId, 'website_id' => $target, 'lang' => $lang]);
                // 删除原来站点的路由
                $Route = new Route();
                $Route->delRoute(['seller_id' => $sellerId, 'website_id' => $target, 'lang' => $lang]);
                $prefix = env('database.prefix');
                $cateTable = $prefix . 'category';
                $res = Db::query("SHOW TABLE STATUS WHERE NAME = '$cateTable'");
                $maxId = $res[0]['Auto_increment'];
                $targetIds = range($maxId, $maxId + $cateCount - 1);
                $map = array_combine($ids, $targetIds);
                $map[0] = 0;
                $cateData = [];
                foreach ($category as $val) {
                    unset($val['create_time']);
                    unset($val['update_time']);
                    $val['website_id'] = $target;
                    if ($val['parent_id'] != 0) {
                        $pathArr = explode('-', $val['path']);
                        $tmpPath = '';
                        array_pop($pathArr);
                        foreach ($pathArr as $path) {
                            $tmpPath .= $map[$path] . '-';
                        }
                        $val['path'] = $tmpPath;
                        $val['parent_id'] = $map[$val['parent_id']];
                    }
                    // 美化url
                    if (!empty($val['alias']) && in_array($val['type'], [1, 4])) {
                        $routeParam = [
                            'alias' => $val['alias'],
                            'type' => $val['type'],
                            'lang' => $val['lang'],
                            'sort' => 1000,
                            'website_id' => $val['website_id'],
                            'seller_id' => $sellerId,
                            'title' => $val['title'],
                            'detail_sort' => 4999,
                            'id' => $map[$val['id']]
                        ];
                        $this->setRoute($routeParam);
                    }
                    unset($val['id']);
                    $tmp = $val;
                    array_push($cateData, $tmp);
                }
                $Category->saveAll($cateData);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, 'success');
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws ModelException
     */
    public function getWebsite($cid, $sellerId)
    {
        $category = new Category();
        return $category->getCategory(['id' => $cid, 'seller_id' => $sellerId])['data'];
    }

}
