<?php


namespace app\service;

use app\model\Keyword;
use app\model\KeywordQuery;
use app\model\Website;
use app\service\keywordMonitor\FiveMonitor;

class MonitorService
{
    /**
     * @param $param
     * @return \think\response\Json
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function getMonitorData($param): \think\response\Json
    {
        $Website = new Website();
        $site = $Website -> getWebsite(['id'=>$param['website_id'],'seller_id'=>$param['seller_id']])['data'];
        $Keyword = new Keyword();
        $keyword = $Keyword -> getALLkeyword(['website_id' => $param['website_id'],'seller_id'=>$param['seller_id']])['data'];
        $keyword = array_column($keyword,'name');
        $total = count($keyword);
        if(!$total){
            return jsonReturn(0,lang('成功'),[]);
        }
        $wordArr = [];
        $tmpWord = '';

        if($total > 50){
            foreach($keyword as $key => $val){
                $tmpWord .= $val . '|';
                if($key >= 49 && $key%49 == 0){
                    $tmpWord = trim($tmpWord,'|');
                    array_push($wordArr,$tmpWord);
                    $tmpWord = '';
                }
            }
        }else{
            foreach($keyword as $val){
                $tmpWord .= $val . '|';
            }
            $tmpWord = trim($tmpWord,'|');
            array_push($wordArr,$tmpWord);
        }
        if(count($wordArr) > 1){
            foreach ($wordArr as $word){
                $FiveMonitor = new FiveMonitor($param['seller_id'],$site['title'],$word);
                $func = $param['engine'];
                return $FiveMonitor->$func();
            }
        }else{
            $FiveMonitor = new FiveMonitor($param['seller_id'],$site['title'],$wordArr[0]);
            $func = $param['engine'];
            $res = $FiveMonitor->$func();
            if($res['errcode'] == 0){
                return jsonReturn($res['errcode'],$res['errmsg'],$res['data']);
            }else{
                return jsonReturn($res['errcode'],$res['errmsg']);
            }
        }
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function keywordMonitor($param)
    {
        $Website = new Website();
        $website = $Website->getWebsite(['id'=>$param['website_id'],'seller_id'=>$param['seller_id']])['data'];
        $siteUrl = $website['domain'];
        $KeywordQuery = new KeywordQuery();
        $keywordModel = new Keyword();

        //获取所有关键词的统计最新的id
        $idsArr = $KeywordQuery->where([
            ['seller_id' ,'=', $param['seller_id']],
            ['website_id','=', $param['website_id']],
            ['search_engine' ,'=', $param['engine']],
        ])->order('keyword_id asc')->group('keyword_id')->column('max(id), create_time', 'max(id)');

        $ids = [];
        $time = date('Y-m-d');
        if (!empty($idsArr)) {
            $ids = array_keys($idsArr);
            $time = date('Y-m-d', $idsArr[$ids[0]]['create_time']);
        }


        // 排名第一页
        $where = [
            ['top_rank','<=',10],
            ['id' ,'in', $ids],
            ['seller_id' ,'=', $param['seller_id']],
            ['website_id','=', $param['website_id']],
            ['search_engine' ,'=', $param['engine']],
        ];
        $firstPage = $KeywordQuery->countKeyword($where, 'id')['data'];
//        $firstPage = 1;
        // 排名前十页
        $where = [
            ['top_rank','<=',100],
            ['id' ,'in', $ids],
            ['seller_id' ,'=', $param['seller_id']],
            ['website_id','=', $param['website_id']],
            ['search_engine' ,'=', $param['engine']],
        ];
        $tenPage = $KeywordQuery->countKeyword($where, 'id')['data'];
//        $tenPage = 2;
        // 达标关键词个数 关键词在前3页算达标
        $where = [
            ['top_rank','<=',30],
            ['id' ,'in', $ids],
            ['seller_id' ,'=', $param['seller_id']],
            ['website_id','=', $param['website_id']],
            ['search_engine' ,'=', $param['engine']],
        ];
        $done = $KeywordQuery->countKeyword($where, 'id')['data'];
//        $done = 3;
        // 关键词总数
        $all = $keywordModel->where([
            ['seller_id' ,'=', $param['seller_id']],
            ['website_id','=', $param['website_id']],
        ])->count();
//        $all = 4;

        $where = [
            ['seller_id' ,'=', $param['seller_id']],
            ['website_id','=', $param['website_id']],
            ['search_engine' ,'=', $param['engine']],
            ['id' ,'in', $ids],
        ];
        if(!empty($param['keyword'])){
            $where[] = ['keyword','like','%'.$param['keyword'].'%'];
        }
        if($rank = (int)$param['rank']){
            array_push($where,['top_rank','<=',$rank]);
        }
        $limit = 10;
        $limitParam = (int)input('limit');
        if($limitParam){
            $limit = $limitParam;
        }
        $keywordQueryList = $KeywordQuery->getKeywordQueryList($where,$limit);
        $data = [
            'count' => [
                'first'  => $firstPage,
                'ten'   => $tenPage,
                'done'  => $done,
                'all'   => $all,
                'time' => $time,
            ],
            'pageData' => [
                'count' => $keywordQueryList['data']->total(),
                'data'  => $keywordQueryList['data']->all()
            ]
        ];

        return jsonReturn(0,lang('成功'),$data);
    }
}