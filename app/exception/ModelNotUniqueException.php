<?php


namespace app\exception;

class ModelNotUniqueException extends BaseException
{
    protected $errCode = 50003;

    protected $errMsg = '内容已存在';


    public function __construct($msg = null, $code = null)
    {
        if (!$msg) {
            $msg = $this->errMsg;
        }
        if (!$code) {
            $code = $this->errCode;
        }
        parent::__construct($msg, $code);
    }
}