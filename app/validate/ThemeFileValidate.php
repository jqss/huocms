<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ThemeFileValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'path|文件路径' => 'require',
        'content|文件内容'  => 'require',
        'theme_id|模板ID' => 'require|number',
        'website_id|网站ID' => 'require|number',
        'url|URL' => 'require',
        'block|区块名称' => 'require'

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $scene = [
        'update' => ['path','content'],
        'design' => ['theme_id','website_id'],
        'compData' => ['theme_id','website_id','url']
    ];
}
