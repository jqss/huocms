<?php


namespace plugins\user\validate;


use think\Validate;

class UserGroupValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'title|等级名称' => 'require|length:2,80',
        'desc|描述' => 'max:255',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['title', 'desc'],
        'update' => ['id', 'title', 'desc'],
        'del' => ['id'],
    ];
}
