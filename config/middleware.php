<?php
// 中间件配置
return [
    // 别名或分组
    'alias'    => [
        // 登陆校验
        'login' => app\middleware\LoginMiddleware::class,
        'apiLogin' => app\middleware\ApiLoginMiddleware::class,
        // 权限校验
        'auth' => app\middleware\AuthMiddleware::class,
        // 
        'demo-fb' => app\middleware\ForbiddenMiddleware::class,
    ],
    // 优先级设置，此数组中的中间件会按照数组中的顺序优先执行
    'priority' => [],
];
