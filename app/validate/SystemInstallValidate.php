<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class SystemInstallValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'account|账号' => 'require|email',
        'password|密码' => 'require|confirm:password_confirm',
        'password_confirm|确认密码' => 'require'
    ];

    protected $message = [
        'account.email' => '账号必须使用邮箱，请输入正确的邮箱账号！'
    ];

}
