<?php

namespace app\controller\backend;

use app\model\Inquiry;
use app\model\RecycleBin;
use app\validate\InquiryValidate;
use think\exception\ValidateException;
use think\facade\Lang;
use think\response\Json;

class InquiryController extends BaseController {

    /**
     * 查询询单列表
     * @return Json
     */
    public function index(): Json
    {
        if (request()->isGet()) {
            $siteId = (int)input('website_id');
            if(empty($siteId)){
                 return jsonReturn(-1,Lang::get('网站ID不能为空'));
            }
            $where = [
                'seller_id' => $this->admin['seller_id'],
                'website_id' => $siteId,
                'is_del' => 1,
            ];
            $limit = $this->setLimit();
            $inquiry = new Inquiry();
            $res = $inquiry->getInquiryList($where,'*', $limit);
            return json(pageReturn($res));
        }
      return jsonReturn(-2, lang('请求方法错误'));
    }

    /**
     * 新增询单信息
     * @return Json
     * @method Post
     */
    public function backendSave(): Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            if (empty($param['phone']) && empty($param['wechat']) && empty($param['email']) && empty($param['qq']) && empty($param['telphone'])) {
                return jsonReturn(-4, lang('联系方式必须填写一个'));
            }
            try {
                validate(InquiryValidate::class)->scene('save')->check($param);
            } catch (ValidateException $e){
                return jsonReturn(-4, $e->getMessage());
            }
            $inquiry = new Inquiry();
            $res = $inquiry -> addInquiry($param);
            return json($res);
        }
        return jsonReturn(-2, lang('请求方法错误'));
    }

    /**
     * 询单信息查询
     * @return Json
     * @method Post
     */
    public function read(): Json
    {
        if (request()->isGet()) {
            $param = input('get.');
            try {
                validate(InquiryValidate::class)->scene('read')->check($param);
            } catch (ValidateException $e){
                return jsonReturn(-4, $e->getMessage());
            }
            $param['seller_id'] = $this->admin['seller_id'];

            $inquiry = new Inquiry();
            $res = $inquiry->getInquiry($param);

            return json($res);
        }
        return jsonReturn(-2, lang('请求方法错误'));
    }

    /**
     * 更新询单信息
     * @return Json
     * @method Post
     */
    public function update(): Json
    {
        if(request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            if (empty($param['mobile']) && empty($param['wechat']) && empty($param['email']) && empty($param['qq'])) {
                return jsonReturn(-4, lang('联系方式必须填写一个'));
            }

            try {
                validate(InquiryValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e){
                return jsonReturn(-4, $e->getMessage());
            }

            $inquiry = new Inquiry();
            $res = $inquiry->updateInquiry($param);

            return json($res);
        }
        return jsonReturn(-2, lang('请求方法错误'));
    }

    /**
     * 删除询单信息
     * @return Json
     * @throws \app\exception\ModelException
     */
    public function delete(): Json
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(InquiryValidate::class)->scene('read')->check($param);
            } catch (ValidateException $e){
                return jsonReturn(-4, $e->getMessage());
            }

            $Inquiry = new Inquiry();
            $inquiry = $Inquiry->getInquiry($param)['data'];
            if(empty($inquiry)){
                jsonReturn(-3, lang('询盘不存在'));
            }
            // 复制删除内容到回收站表
            $recycleBin = new RecycleBin();
            $binData = [
                'seller_id' => $param['seller_id'],
                'object_id' =>  $inquiry['id'],
                'sub_id' => 0,
                'module_id' => 0,
                'table_name' => lang('询盘'),
                'title' => !empty($inquiry['title']) ? $inquiry['title'] : lang('询盘内容') . $inquiry['id'],
                'admin_id' => $this->admin['uid'],
                'name' => $this->admin['name'],
            ];
            $recycleBin -> addRecycleBin($binData);
            $inquiry->is_del = 2;
            $inquiry->delete_time = time();
            $inquiry->save();
            return jsonReturn(0, lang('删除成功'));
        }
        return jsonReturn(-2, lang('请求方法错误'));
    }


    public function batch_delete(): Json
    {
        if (request()->isPost()) {
            $param = input('post.');
            try {
                validate(InquiryValidate::class)->scene('batch')->check($param);
            } catch (\Exception $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $where = [
                ['seller_id' ,'=', $this->admin['seller_id']],
                ['ids','in', $param['ids']],
            ];

            $linkWebsite = new Inquiry();
            $res = $linkWebsite->batchDeleteInquiry($where);

            return jsonReturn($res['code'], $res['msg']);
        }
        return jsonReturn(-1,  lang('请求方法错误'));
    }
}
