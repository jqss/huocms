<?php


namespace app\model;


class KeywordWebsite extends Model
{
    /**
     * 读取信息列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getKeywordWebsiteList ($where): array
    {
        try {
            $res = $this->where($where)->field('id, seller_id, keyword_id, website_id')->select();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 读取信息
     * @param $where
     * @return array
     */
    public function getKeywordWebsite($where): array
    {
        try {
            $res = $this->where($where)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 增加信息
     * @param $param
     * @return array
     */
    public function addKeywordWebsite($param): array
    {
        try {
            $has = $this->where(['seller_id'=>$param['seller_id'], 'keyword_id'=>$param['keyword_id'], 'website_id'=>$param['website_id']])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('信息重复'));
            }
            $res = self::create($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->addMsg, $res->id);
    }

    /**
     * 更新信息
     * @param $param
     * @return array
     */
    public function updateKeywordWebsite($param): array
    {
        try {
            $has = $this->where(['seller_id'=>$param['seller_id'], 'keyword_id'=>$param['keyword_id'], 'website_id'=>$param['website_id']])->where('id','<>', $param['id'])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('信息重复'));
            }
            self::where('id', $param['id'])->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->updateMsg);
    }

    /**
     * 删除信息
     * @param $where
     * @return array
     */
    public function deleteKeywordWebsite($where): array
    {
        try {
            self::destroy($where);
        }catch (\Exception $e){
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn($this->sucCode, $this->delMsg);
    }
}