<?php

namespace app\controller\frontend;

use think\facade\View;

class DesignBaseController extends BaseController
{
    protected $designBase = 'public/design/designPage';
    protected $designPath = '';

    public function initialize()
    {
        parent::initialize();
        $this->setPath();
    }

    public function setPath()
    {
        $this->designPath = CMS_ROOT . 'public/design/';
    }

    /**
     * 加载模板输出(system_file)
     * @access protected
     * @param string $template 模板文件名
     * @param array $vars     模板输出变量
     * @return mixed
     */
    protected function fetchDesign(string $template = '', array $vars = [])
    {
        $template = $this->parseTemplateDesign($template);
        $this->_initializeViewDesign($this->realSiteId,$this->lang);

        $html = View::fetch($template, $vars);
        $replace = <<<EOL
<script id="baseJs" src="/system_file/js/base.min.js"></script>
</head>
EOL;
        $html = str_replace('</head>', $replace, $html);
        return $html;
    }

    /**
     *
     * 定位模板
     * @param $template
     * @return string
     */
    private function parseTemplateDesign($template): string
    {
        $path = CMS_ROOT . "public/design/designPage" . DIRECTORY_SEPARATOR;
        $ext = getFileExt($template);
        $path .= DIRECTORY_SEPARATOR . ltrim($template,'/');
        if( $ext != config('view.view_suffix')){
            $path .= '.'.config('view.view_suffix');
        }
        return $path ;
    }

    /**
     * 常量替换
     * @param $siteId
     * @param $lang
     */
    protected function _initializeViewDesign($siteId,$lang)
    {
        $viewReplaceStr = [
            '__TMPL__'     => "/design/designPage",
            '__STATIC__'   => empty($this->settingData['static_file']) ? "/design/designPage/static" : $this->settingData['static_file'] ,
        ];

        View::engine()->config([
            'view_path' =>  CMS_ROOT . $this->designBase . DIRECTORY_SEPARATOR,
            'tpl_replace_string' => $viewReplaceStr,
            'display_cache' => false,
            'tpl_cache' => false,
        ]);
    }

    /**
     * 加载模板输出(system_file)
     * @access protected
     * @param string $template 模板文件名
     * @param array $vars     模板输出变量
     * @return mixed
     */
    protected function fetchDesignPage(string $template = '', array $vars = [])
    {
        $template = $this->parseTemplateDesignPage($template);
        $this->_initializeViewDesignPage($this->realSiteId,$this->lang);

        return View::fetch($template, $vars);
    }

    /**
     *
     * 定位模板
     * @param $template
     * @return string
     */
    private function parseTemplateDesignPage($template): string
    {
        $path = CMS_ROOT . "design/designPage/{$this->realSiteId}/{$this->lang}" . DIRECTORY_SEPARATOR;
        $ext = getFileExt($template);
        $path .= DIRECTORY_SEPARATOR . ltrim($template,'/');
        if( $ext != config('view.view_suffix')){
            $path .= '.'.config('view.view_suffix');
        }
        return $path ;
    }

    /**
     * 常量替换
     * @param $siteId
     * @param $lang
     */
    protected function _initializeViewDesignPage($siteId,$lang)
    {
        $viewReplaceStr = [
            '__TMPL__'     => "/design/designPage/{$siteId}/{$lang}",
            '__STATIC__'   => empty($this->settingData['static_file']) ? "/design/designPage/{$siteId}/{$lang}/static" : $this->settingData['static_file'] ,
        ];

        View::engine()->config([
            'view_path' =>  CMS_ROOT . $this->designBase . DIRECTORY_SEPARATOR . $siteId. DIRECTORY_SEPARATOR .$this->lang.DIRECTORY_SEPARATOR,
            'tpl_replace_string' => $viewReplaceStr,
            'display_cache' => false,
            'tpl_cache' => false,
        ]);
    }
}
