<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ModuleValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|模型ID' => 'require|number',
        'title|模型名称' => 'require|max:80|unique:module',
        'table|模型表名' => 'require|max:30|unique:module|regex:/^(?!_)(?!.*?_$)([a-z_])+$/',
        'status|状态' => 'in:1,2',
    ];

    protected $scene = [
        'save' => ['title','table'],
        'update' => ['id','title','status'],
    ];

    protected $message = [
//        'table.regex' => "模型表名请用英文字母数字和'_'组合,且下滑先不能开头和末尾",
        'table.regex' => "表名格式错误",
    ];
}
