<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Slide;
use app\service\CacheService;
use app\validate\SlideValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class SlideController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(Slide $slide): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'slide_cate_id' => (int)input('slide_cate_id'),
        ];
        $slideList = $slide->getAllCustomArrayData($where,'sort desc','*',['attachment'=>function($q){
            $q->field('id,name,url,type');
        }]);
        return json($slideList);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function save(Slide $slide): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(SlideValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }

            $res = $slide -> addSlide($param);
            CacheService::deleteRelationCacheByObject($slide);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(Slide $slide): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,Lang::get('幻灯片ID'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // 其他逻辑
        $res = $slide->getSlide($where,['attachment'=>function($q){
            $q->field('id,name,url,type');
        }]);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     *@throws \ReflectionException
     */
    public function update(Slide $slide): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(SlideValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $slide -> updateSlide($where,$param);
            CacheService::deleteRelationCacheByObject($slide);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function delete(Slide $slide): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,Lang::get('幻灯片ID'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $slide->delSlide($where);
            CacheService::deleteRelationCacheByObject($slide);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
