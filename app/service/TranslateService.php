<?php


namespace app\service;


use app\model\SysSetting;
use GuzzleHttp\Client;

class TranslateService
{
    /**
     * @throws \app\exception\ModelException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function baiduTranslate($q, $from, $to,$sellerId): array
    {
        $api = config('system.baidu_translate_api');
        $sysSetting = new SysSetting();
        $appid = $sysSetting -> getSysSettingValue(['seller_id'=>$sellerId,'title' => 'baidu_translate_appid'])['data'];
        $token = $sysSetting -> getSysSettingValue(['seller_id'=>$sellerId,'title' => 'baidu_translate_token'])['data'];;
        $salt = config('system.baidu_translate_salt');
        $sign = "{$appid}{$q}{$salt}{$token}";
        if(!empty($appid) && !empty($token)){
            $client = new Client();
            $param = [
                'q' => $q,
                'from' => $from,
                'to' => $to,
                'salt' => $salt,
                'appid' => $appid,
                'sign' => md5($sign),
            ];
            $response = $client -> request('POST',$api,[
               'form_params' => $param,
            ]);
            $res = json_decode($response->getBody()->getContents(),true);

            if(isset($res['error_code'])){
                return $res;
            }else{
                $tmp = '';
                foreach ($res['trans_result'] as $val){
                    $tmp .= $val['dst'] . PHP_EOL;
                }
                return dataReturn(0,lang('成功'),$tmp);
            }

        }else{
            return dataReturn(-1,lang('百度翻译配置缺少,请完善'));
        }

    }
}