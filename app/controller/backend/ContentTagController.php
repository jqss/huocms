<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\ContentTag;
use app\taglib\HcTaglib;
use app\validate\ContentTagValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class ContentTagController extends BaseController
{

    public function initialize()
    {
        define('ADMIN_SITE_ID',(int)input('site_id'));
        define('ADMIN_SELLER_ID',$this->admin['seller_id']);
        parent::initialize();
    }

    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(ContentTag $contentTag): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];
        $limit = 10;
        $limitParam = (int)input('limit');
        if($limitParam){
            $limit = $limitParam;
        }
        // TODO
        // 添加其他逻辑

        $contentTagList = $contentTag->getContentTagList($where,$limit);
        return json(pageReturn($contentTagList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(ContentTag $contentTag): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(ContentTagValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // TODO
            // 其他逻辑

            $res = $contentTag -> addContentTag($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(HcTaglib $hcTaglib): \think\response\Json
    {

        $param = input('post.');
        try {
            validate(ContentTagValidate::class)->scene('read')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $param['seller_id'] = $this->admin['seller_id'];
        $func = 'tag' . ucfirst($param['title']);
//        $hcTaglib = new HcTaglib(new Template());
        $hcTaglib->$func();
        dd($param);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(ContentTag $contentTag): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(ContentTagValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'is_del' => 1,
            ];
            $res = $contentTag -> updateContentTag($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(ContentTag $contentTag): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
               // TO DO
               // 替换错误提示
                return jsonReturn(-1,'ErrorMsg');
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $contentTag->delContentTag($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
