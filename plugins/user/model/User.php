<?php


namespace plugins\user\model;


use app\model\Attachment;
use app\model\PluginBaseModel;

class User extends PluginBaseModel
{
    public function avatar(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class,'avatar');
    }

    public function userGroup(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(UserGroup::class,'user_group');
    }

    public function userGrade(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(UserGrade::class,'user_grade');
    }
}
