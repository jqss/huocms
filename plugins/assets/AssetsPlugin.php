<?php

namespace plugins\assets;

use app\model\AdminMenu;
use app\Plugin;

class AssetsPlugin extends Plugin
{
    public $info = [
        'name'        => 'assets',//插件英文名
        'title'       => '知产管理模块',
        'description' => '管理商标、版权、软著',
        'status'      => 1,
        'author'      => 'huocms',
        'version'     => '1.0.0',
        'demo_url'    => 'http://www.demo_huocms.com',
        'author_url'  => 'http://www.huocms.com'
    ];

    public $hasAdmin = 1;//插件是否有后台管理界面

    // 插件安装
    public function install()
    {
        if (!$this->installSql()) {
            return false;
        }

        //更新菜单
        $this->editMenu(1, 'plugin_assets');
        return true;//安装成功返回true，失败false
    }

    // 插件卸载
    public function uninstall()
    {
        //更新菜单
        $this->editMenu(2, 'plugin_assets');
        return true;//卸载成功返回true，失败false
    }
}