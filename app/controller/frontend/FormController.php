<?php

namespace app\controller\frontend;

use app\model\Attachment;
use app\model\customModel\Down;
use app\model\DiyForm;
use app\model\SubContent;
use app\model\SysSetting;
use app\service\upload\Upload;
use think\facade\Db;

class FormController extends BaseController
{
    protected  $type = [
        'file'    => 4,
        'image'    => 2,
        'video'    => 3,
        'mp3'    => 5,
        'zip'   => 1,
    ];

    public function index()
    {
        $code = $this->request->param('code');

        $formModel = new DiyForm();
        $formData = $formModel->where('code', $code)->findOrEmpty();
        if (empty($formData)) {
            $this->to404();
        }

        $this->assign('formData', $formData);

        $html = $this->formHtml();

        $html = str_replace(['{formData_name}', '{formData_designContent}', '{formData_designOption}', '{formData_code}'],
            [$formData['name'],$formData['design_content'],$formData['design_option'],$formData['code']], $html);

        return $html;
    }

    public function save()
    {
        $param = $this->request->param();

        $code = $param['code'];
        unset($param['code']);
        $formModel = new DiyForm();
        $formData = $formModel->where('code', $code)->findOrEmpty();
        if (empty($formData)) {
            return jsonReturn(-1, lang('提交失败'));
        }

        $tableName = 'diyform_' . $formData['table'];

        foreach ($param as &$value) {
            if (is_array($value)) {
                $value = implode(',', $value);
            }
        }

        $param['ip'] = $this->request->ip();
        $param['user_agent'] = $this->request->header('user-agent');

        Db::name($tableName)->insert($param);

        return jsonReturn(0, lang('提交成功'));
    }

    // 上传文件
    public function uploadFormFile()
    {
        if(!request()->isPost()){
            return jsonReturn(-1, lang('请求错误'));
        }

        $file = request()->file('file');
        if(empty($file)){
            return jsonReturn(-2,lang('文件不能为空'));
        }
        $seller_id = $this->sellerId;
        // 查看文件类型
        $fileName = $file->getOriginalName();
        $fileExt = $file->getOriginalExtension();
        $file_type = fileFormat($fileName);
        $fileType = $this->type[$file_type];
        // 附件大小和类型验证
        // 获取上传配置
        $Settings = new SysSetting();
        $uploadSetting = $Settings->getAllCustomArrayData(['parent_id'=>1,'group'=>'upload','status'=>1],'id desc','id,group,title,value')['data'];
        $uploadSetting = getColumnForKeyArray($uploadSetting,'title');
        $limitSize = $uploadSetting[$file_type.'_size']['value'] * 1024;     // byte
        $fileSize = $file->getSize();       // 单位byte
        if($fileSize > $limitSize){
            return jsonReturn(-1,lang('文件过大，请修改上传限制或者替换小的文件'));
        }
        $extArr = explode(',',$uploadSetting[$file_type.'_ext']['value']);
        if(!in_array($fileExt,$extArr)){
            return jsonReturn(-2,lang('文件格式错误，请重新上传'));
        }
        // 文件信息提取
        $where = [
            'seller_id' => $seller_id,
            'group' => 'upload',
            'title' => 'storage'
        ];
        $place = new SysSetting();
        $type = $place->getSysSetting($where)['data']->toArray()['value'];

        $upload = new Upload();
        $info = $upload->create($file,$seller_id, $type,$file_type);

        if ($info) {
            $uploadInfo = $upload->getUploadFileInfo();
            if ($uploadInfo['code'] == 0) {
                $uploadInfo['data']['type'] = $fileType;
                $uploadInfo['data']['size'] = round($fileSize / 1024,2);
                $uploadInfo['data']['mime_type'] = $fileExt;
            }
            return json($uploadInfo);
        }else{
            return jsonReturn(-1, lang('上传失败请重新尝试'));
        }
    }

    /**
     * 下载文件
     * @return void
     */
    public function downFile()
    {
        $param = $this->request->param();

        if (empty($param)) {
            $this->to404();
        }

        $id = intval($param['id']);
        $downFlag = false; // 可下载标识
        $downModel = new Down();
        $downData = $downModel->where('sub_id', '=', $id)->findOrEmpty();
        if (empty($downData)) {
            $this->to404();
        }

        $subContentModel = new SubContent();
        $downData['title'] = $subContentModel->where('id', '=', $downData['sub_id'])->value('title');
        $formData = [];
        if ($downData['is_form'] == '是') {
            $formModel = new DiyForm();
            $formData = $formModel->where('id', '=', $downData['form_id'])->where('status', '=', 2)->findOrEmpty();
            if (empty($formData)) {
                $this->to404();
            }
            $tableName = 'diyform_' . $formData['table'];

            $where = [
                ['sub_id', '=', $id],
                ['ip', '=', $this->request->ip()],
                ['user_agent', '=', $this->request->header('user-agent')],
                ['visitor_id', '=', $this->request->param('visitor_id')]
            ];

            $id = Db::name($tableName)->where($where)->value('id');

            if (!empty($id)) {
                $downFlag = true;
            }
        } else {
            $downFlag = true;
        }

        if ($downFlag) {
            $fileModel = new Attachment();
            $downData['fileArr'] = $fileModel->where('id', 'in', json_decode($downData['file'], true))->select()->toArray();
        }

        $this->assign('downData', $downData);
        $this->assign('formData', $formData);
        $this->assign('downFlag', $downFlag);
        $this->assign('visitor_id', $this->request->param('visitor_id'));

        return $this->fetchSystem('down/index');
    }

    /**
     * 问卷html
     * @return string
     */
    protected function formHtml()
    {
        $html = <<<EOF
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{formData_name}</title>
</head>
<body>
<div class="container" style="display: block;max-width:1200px;margin: 0 auto; ">
    <div id="app-3" class="formBox" style="padding-top: 50px">
        <template>
            <form-create
                    v-model="fapi"
                    :rule="rule"
                    :option="option"
                    @submit="onSubmit"
            ></form-create>
        </template>
    </div>
</div>
</body>
<!-- import Vue.js -->
<!--    <script src="//vuejs.org/js/vue.min.js"></script>-->
<!-- 生产环境版本，优化了尺寸和速度 -->
<script src="/system_file/form/vue@2"></script>
<!-- import stylesheet -->
<link rel="stylesheet" href="/system_file/form/index.css">
<!-- import element -->
<script src="/system_file/form/index.js"></script>
<!-- import form-create/element -->
<script src="/system_file/form/form-create.min.js"></script>
<!-- import form-create/designer -->
<script src="/system_file/form/index.min.js"></script>

<script src="/system_file/form/jquery-3.4.1.min.js"></script>
<script>
    //FcDesigner 生成的`JSON`
    // const FcDesignerRule = '[{"type":"input","field":"cuk5qqdw3umc","title":"输入框","info":"","_fc_drag_tag":"input","hidden":false,"display":true}]';
    const FcDesignerRule = '{formData_designContent}';

    //FcDesigner 生成的`options`
    // const FcDesignerOptions = '{"form":{"labelPosition":"right","size":"mini","labelWidth":"125px","hideRequiredAsterisk":false,"showMessage":true,"inlineMessage":false}}';
    const FcDesignerOptions = '{formData_designOption}';

    const code = '{formData_code}';

    console.log(FcDesignerOptions, 'asdasdasdasdsada')

    var app3 = new Vue({
        el: '#app-3',
        data: {
            fapi: null,
            rule: formCreate.parseJson(FcDesignerRule),
            option: formCreate.parseJson(FcDesignerOptions)
        },
        methods: {
            onSubmit (formData) {
                formData.code = code
                //todo 提交表单
                console.log(formData, '提交表单提交表单提交表单')

                $.post('/addFormData.html', formData, function(res) {
                    console.log(res, '接口返回');
                    if (res.code == 0) {
                        alert('提交成功')
                    } else {
                        alert('提交失败')
                    }
                });
            }
        }
    })
</script>

</html>
EOF;

        return $html;
    }
}
