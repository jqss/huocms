<?php


namespace plugins\user\validate;


use think\Validate;

class UserGradeValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'title|等级名称' => 'require|length:2,80',
        'icon|等级图标' => 'max:255',
        'min_score|最低积分值' => 'require|number',
        'max_score|最大积分值' => 'require|number',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['title', 'icon', 'min_score', 'max_score'],
        'update' => ['id', 'title', 'icon', 'min_score', 'max_score'],
        'del' => ['id'],
    ];
}
