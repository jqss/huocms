<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Website extends Model
{
    public function link(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(Link::class, 'link_website');
    }

    public function keyword(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(Keyword::class, 'keyword_website');
    }

    public function theme(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(Theme::class,'theme_website');
    }

    public function inquiryEmail(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(InquiryEmail::class,'website_inquiry_email');
    }

    public function category(): \think\model\relation\HasMany
    {
        return $this->hasMany(Category::class);
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getWebsiteList(array $where = [], $with = [], int $limit = 10): array
    {
        try{
           $res =  $this->with($with)->where($where)->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getWebsite(array $where = [], $with = []): array
    {
        try{
           $res =  $this->with($with)->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
         }catch(\Exception $e){
             throw new ModelException($e->getMessage());
         }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @throws ModelException
     */
    public function getWebsiteByDomain($domain,$field='*'): array
    {
        try{
            $res =  $this->field($field)->where(['domain'=>$domain])->find();
            if(empty($res)){
                throw new ModelEmptyException();
            }
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @return array
     * @throws ModelException
     */
    public function getOnlyWebsite(array $where = []): array
    {
        try{
            $res =  $this->where($where)->find();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addWebsite($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res->id);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateWebsite(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delWebsite($where): array
    {
        try{
            $res =  $this->where($where)->delete();
         }catch(\Exception $e){
             throw new ModelException($e->getMessage());
         }
         return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    public function getAllCustomData($where=[],$sort='id desc',$fields="*")
    {
        try{
            $list =  $this->where($where)->order($sort)->field($fields)->select();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$list);
    }
}