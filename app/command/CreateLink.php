<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class CreateLink extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('admin-view:link')
            ->setDescription('the app\command\createLink command');
    }

    protected function execute(Input $input, Output $output)
    {
        foreach ($this->links() as $link => $target) {
            if (file_exists($link)) {
                $output->writeln('<error>' . $link . ' already exists!</error>');
            } else {
                $this->createLink($target, $link);

                $output->writeln('<info>' . $link . ' created successfully.</info>');
            }
        }

        $output->writeln('The links have been created.');
    }

    protected function links(): array
    {
        return ['public/view' => app()->getRootPath() .'view/'];
    }

    public function createLink($target,$link): bool
    {

        if (! windows_os()) {
            return symlink($target, $link);
        }

        $mode = is_directory($target) ? 'J' : 'H';

        exec("mklink /{$mode} ".escapeshellarg($link).' '.escapeshellarg($target));
    }

}
