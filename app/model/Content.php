<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Content extends Model
{
    protected $table;
    protected $autoWriteTimestamp = true;

    public function __construct(string $table,array $data = [])
    {
        parent::__construct($data);

        $this->table = $table;

    }
}
