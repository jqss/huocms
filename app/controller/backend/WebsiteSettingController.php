<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Attachment;
use app\model\Model;
use app\model\WebsiteSetting;
use app\validate\WebsiteSettingValidate;
use think\exception\ValidateException;
use think\facade\Cache;
use think\facade\Cookie;
use think\facade\Lang;


class WebsiteSettingController extends BaseController
{
    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelNotUniqueException
     */
    public function save(WebsiteSetting $websiteSetting): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(WebsiteSettingValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $param['seller_id'] = $this->admin['seller_id'];
            $param['setting'] = json_encode($param['setting']);
            $websiteSetting->saveUnique(['seller_id'=>$param['seller_id'],'website_id'=>$param['website_id'],'lang'=>$param['lang']],'相同配置已经存在,请编辑');
            $res = $websiteSetting -> addWebsiteSetting($param);
            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(WebsiteSetting $websiteSetting,Attachment $attachment): \think\response\Json
    {
        $param = input('param.');
        // 数据验证
        try{
            validate(WebsiteSettingValidate::class)->scene('read')->check($param);
        }catch(ValidateException $e){
            return jsonReturn(-1, $e->getError());
        }
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'website_id'    =>  $param['website_id'],
            'lang'    =>  $param['lang'],
        ];
        $cacheKey = $this->admin['seller_id'] .'_'.$param['website_id'] .'_'. $param['lang'] . '_website_cache_key';
        Cache::delete($cacheKey);
        $res = $websiteSetting->getWebsiteSetting($where);
        $res['data'] = $res['data']->toArray();
        $res['data']['setting'] = json_decode($res['data']['setting'],true);
        if(!empty($res['data']['setting']['logo'])){
            $attach = $attachment->getAttachment(['id'=>$res['data']['setting']['logo'],'seller_id'=>$this->admin['seller_id']])['data'];
            if(!empty($attach)){
                $res['data']['setting']['logo'] = $attach;
            }
        }
        if(!empty($res['data']['setting']['wechat_code'])){
            $attach = $attachment->getAttachment(['id'=>$res['data']['setting']['wechat_code'],'seller_id'=>$this->admin['seller_id']])['data'];
            if(!empty($attach)){
                $res['data']['setting']['wechat_code'] = $attach;
            }
        }
        if(!empty($res['data']['setting']['dark_logo'])){
            $attach = $attachment->getAttachment(['id'=>$res['data']['setting']['dark_logo'],'seller_id'=>$this->admin['seller_id']])['data'];
            if(!empty($attach)){
                $res['data']['setting']['dark_logo'] = $attach;
            }
        }
        return json($res);

    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(WebsiteSetting $websiteSetting): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(WebsiteSettingValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'seller_id' => $this->admin['seller_id'],
                'website_id'    =>  $param['website_id'],
                'lang'    =>  $param['lang'],
            ];
            $setting = json_encode($param['setting']);
            $res = $websiteSetting -> updateWebsiteSetting($where,['setting'=>$setting]);
            $siteCacheKey = $this->admin['seller_id'] .'_'.$param['website_id'] .'_'. $param['lang'] . '_website_cache_key';
            Cache::delete($siteCacheKey);

            if(config('lang')['use_cookie']){
                Cookie::set(config('lang')['cookie_var'],$param['lang']);
            }

            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

}
