<?php

use think\facade\Route;

// 前台公共路由
// 询盘提交
Route::post('inquiry/save', 'Inquiry/save');

// 标签
Route::get('/tag', 'Tag/index');

Route::get('/tag/:name', 'Tag/index')->name('Tag/index')
    ->pattern(array('name' => '[a-zA-Z]{1,1}+'));
Route::get('/tagList/:id', 'List/tag')->name('List/tag')
    ->pattern(array('id' => '\d+'));

// 搜索
Route::get('/search', 'Search/index');

// 表单
Route::get('/c/:code', 'Form/index')->name('Form/index')
    ->pattern(array('code' => '\w+'));
Route::post('/addFormData', 'Form/save');
Route::post('/uploadFormFile', 'Form/uploadFormFile');

Route::get('/down/:id', 'Form/downFile')->name('Form/downFile')
    ->pattern(array('id' => '\d+'));

Route::get('/design/index', 'Design/index');
Route::get('/design/getDesignHtml', 'Design/getDesignHtml');

Route::get('/robots.txt','Seo/getRobots');
Route::get('/sitemap.xml','Seo/getSitemapXml');
Route::get('/sitemap','Seo/getSitemapHtml');
Route::get('/404','Inquiry/404');