<?php


namespace app\model;


use think\db\exception\DbException;
use think\model\Pivot;

class LinkWebsite extends Model
{
    /**
     * 查询信息列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getLinkWebsiteList($where): array
    {
        try {
            $res = $this->where($where)->select();

        } catch (DbException $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 查询单条信息
     * @param $where
     * @return array
     */

    public function getLinkWebsite($where): array
    {
        try {
            $res = $this->where($where)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 新增信息
     * @param $param
     * @return array
     */

    public function addLinkWebsite($param): array
    {
        try{
            // 判断关联重复
            $has = $this->where(['seller_id'=>$param['seller_id'], 'link_id'=>$param['link_id'], 'website_id'=>$param['website_id']])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('数据重复'));
            }

            // 判断排名重复
            $has = $this->where(['seller_id'=>$param['seller_id'], 'website_id'=>$param['website_id'], 'sort'=>$param['sort']])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('排名重复'));
            }

            $res = $this::create($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->addMsg, $res->id);
    }

    /**
     * 更新信息
     * @param $param
     * @return array
     */

    public function updateLinkWebsite($param): array
    {
        try{
            $has = $this->where(['seller_id'=>$param['seller_id'], 'link_id'=>$param['link_id'], 'website_id'=>$param['website_id']])->where('id', '<>',$param['id'])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('数据重复'));
            }

            // 判断排名重复
            $has = $this->where(['seller_id'=>$param['seller_id'], 'website_id'=>$param['website_id'], 'sort'=>$param['sort']])->where('id', '<>',$param['id'])->find();
            if (!empty($has)) {
                return dataReturn(-3,  lang('排名重复'));
            }

            self::where('id', $param['id'])->where('seller_id', $param['seller_id'])->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->updateMsg);
    }



    /**
     * 删除信息
     * @param $where
     * @return array
     */

    public function deleteLinkWebsite($where): array
    {
        try {
            $this->where($where)->delete();
        } catch (\Exception $e) {
        return dataReturn(-1, $e->getMessage());
    }
        return dataReturn(0, $this->delMsg);
    }
}