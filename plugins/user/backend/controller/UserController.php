<?php


namespace plugins\user\backend\controller;


use app\controller\backend\BaseController;
use app\validate\AdminValidate;
use plugins\user\model\User;
use plugins\user\validate\UserValidate;
use think\exception\ValidateException;

class UserController extends BaseController
{
    public function index()
    {
        $param = $this->request->only([
            'username' => '',
            'nickname' => '',
            'mobile' => '',
            'email' => '',
            'status' => '',
            'user_group' => 0,
            'user_grade' => 0,
            'limit' => 10,
        ]);

        $where = [];
        if (!empty($param['username'])) {
            $where[] = ['username', 'like', "%{$param['username']}%"];
        }
        if (!empty($param['nickname'])) {
            $where[] = ['nickname', 'like', "%{$param['nickname']}%"];
        }
        if (!empty($param['mobile'])) {
            $where[] = ['mobile', 'like', "%{$param['mobile']}%"];
        }
        if (!empty($param['email'])) {
            $where[] = ['email', 'like', "%{$param['email']}%"];
        }
        if (!empty($param['status'])) {
            $where[] = ['status', '=', $param['status']];
        }
        if (!empty($param['user_group'])) {
            $where[] = ['user_group', '=', $param['user_group']];
        }
        if (!empty($param['user_grade'])) {
            $where[] = ['user_grade', '=', $param['user_grade']];
        }

        $userModel = new User();
        $list = $userModel->getList($where, ['userGroup', 'userGrade'], $param['limit'], 'id desc');

        return json(pageReturn($list));
    }

    public function add()
    {
        $param = $this->request->post([
            'username' => '',
            'password' => '',
            'nickname' => '',
            'sex' => '',
            'birthday' => '',
            'user_url' => '',
            'mobile' => '',
            'email' => '',
            'avatar' => 0,
            'signature' => '',
            'more' => [],
            'status' => 1,
            'user_group' => 0,
            'user_grade' => 0,
        ]);
        try {
            validate(UserValidate::class)->scene('save')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $param['password'] = $param['password'] ?? '123123';
        $param['password'] = makePassword($param['password']);
        $param['more'] = json_encode($param['more'], JSON_UNESCAPED_UNICODE);
        $param['seller_id'] = $this->admin['seller_id'];

        $userModel = new User();
        $res = $userModel->add($param);

        return json($res);
    }

    public function edit()
    {
        $param = $this->request->post([
            'id' => 0,
            'username' => '',
            'password' => '',
            'nickname' => '',
            'sex' => '',
            'birthday' => '',
            'user_url' => '',
            'mobile' => '',
            'email' => '',
            'avatar' => 0,
            'signature' => '',
            'more' => [],
            'status' => 1,
            'user_group' => 0,
            'user_grade' => 0,
        ]);
        try {
            validate(UserValidate::class)->scene('update')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        if (!empty($param['password'])) {
            $param['password'] = makePassword($param['password']);
        } else {
            unset($param['password']);
        }

        $param['more'] = json_encode($param['more'], JSON_UNESCAPED_UNICODE);

        $userModel = new User();
        $res = $userModel->edit(['id' => $param['id']], $param);

        return json($res);
    }

    public function editStatus()
    {
        $param = $this->request->post([
            'id' => 0,
            'status' => 0,
        ]);

        try {
            validate(UserValidate::class)->scene('editStatus')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $userModel = new User();
        $res = $userModel->edit(['id' => $param['id']], $param);

        return json($res);
    }

}
