var swiper = new Swiper(".swiper-container", {
  direction: "vertical",
  slidesPerView: 1,
  spaceBetween: 0,
  mousewheel: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
});

var indexBlock6Img1 = document.getElementsByClassName("indexBlock6Img1");
for (var i = 0; i < indexBlock6Img1.length; i++) {
  if (i >= 12) indexBlock6Img1[i].style.borderBottom = "unset";
}
