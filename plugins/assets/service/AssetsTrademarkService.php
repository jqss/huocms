<?php

namespace plugins\assets\service;

use plugins\assets\model\AssetsTrademark;

class AssetsTrademarkService
{
    public $trademark;

    public function __construct()
    {
        $this->trademark = new AssetsTrademark();
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function addTrademark($param): array
    {
        if ($this->trademark->isExist([
            ['apply_code', '=', $param['apply_code']]
        ])) {
            return dataReturn(-2, lang('注册号已经存在'));
        }
        if (empty($param['apply_time'])) unset($param['apply_time']);

        return $this->trademark->add($param);
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function editTrademark($param)
    {
        if ($this->trademark->isExist([
            ['id', '<>', $param['id']],
            ['apply_code', '=', $param['apply_code']]
        ])) {
            return jsonReturn(-2, lang('注册号已经存在'));
        }

        if (empty($param['apply_time'])) unset($param['apply_time']);

        return $this->trademark->edit(['id' => $param['id']], $param);

    }

    public function deleteTrademark($id,$sellerId): array
    {
        $res = $this->trademark->where(['id' => $id,'seller_id'=>$sellerId])->delete();
        if ($res === false) {
            return dataReturn(-1, lang('删除失败'));
        }
        return dataReturn(0, lang('删除成功'));
    }

    public function readTrademark(int $id, $sellerId): array
    {
        return $this->trademark->getCustomData(['id'=>$id,'seller_id'=>$sellerId]);
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function getTrademarkList(array $param): array
    {
        $where = [];
        if(!empty($param['title'])){
            $where[] = ['title','like','%'.$param['title'].'%'];
        }
        return pageReturn($this->trademark->getList($where,[],$param['limit']));
    }

}
