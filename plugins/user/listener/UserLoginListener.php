<?php


namespace plugins\user\listener;


use app\exception\ModelException;
use plugins\user\model\UserLogin;

class UserLoginListener
{
    /**
     * 事件监听处理
     *
     * @param $event
     * @return mixed
     * @throws ModelException
     */
    public function handle($event)
    {
        (new UserLogin())->add($event);
    }
}
