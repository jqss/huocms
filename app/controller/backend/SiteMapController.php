<?php
declare (strict_types = 1);

namespace app\controller\backend;


use app\model\Website;
use app\service\Sitemap;
use think\facade\Lang;


class SiteMapController extends BaseController
{
    /**
     * @throws \app\exception\ModelException
     */
    public function gen() {
        $website_id = input('website_id');
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'website_id' => $website_id,
        ];

        $sitemap = new Sitemap($where['seller_id'], $where['website_id']);
        $sitemap->scan();
        $silian = $sitemap->siliamGen();

        // 生成xml文件
        $path = $sitemap->sitemapGen();


        $websiteWhere = [
            'id' => $website_id
        ];
        $param = [
            'sitemap_url' => $path,
            'silian_url' => $silian,
        ];

        $website = new Website();
        $res = $website->updateWebsite($websiteWhere, $param);

        return jsonReturn($res['code'], Lang('生成成功'), $res['data']);
    }

}
