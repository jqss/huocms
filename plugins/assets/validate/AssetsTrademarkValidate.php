<?php

namespace plugins\assets\validate;

class AssetsTrademarkValidate extends \think\Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'title|名称' => 'require|length:2,80',
        'desc|描述' => 'max:255',
        'category|类别号' => 'require|max:255',
        'apply_code|申请/注册号' => 'require|max:255',
        'apply_user|申请人' => 'require|max:255',
        'apply_time|申请时间' => 'require|date',
        'certificate_img|证书图片' => 'require|max:255',
        'trademark_img|商标图片' => 'require|max:255',
        'register_address|注册地址' => 'require|max:255',
        'valid_time|到期时间' => 'require|date'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['title', 'apply_code','category','apply_user','apply_time','certificate_img','trademark_img','register_address'],
        'update' => ['id', 'title', 'apply_code','apply_code','category','apply_user','apply_time','certificate_img','trademark_img','register_address'],
        'del' => ['id'],
    ];
}
