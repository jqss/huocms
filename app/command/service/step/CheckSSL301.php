<?php

namespace app\command\service\step;

use GuzzleHttp\Client;

class CheckSSL301
{
    /**
     * 检测 301 和 ssl
     * @param $taskData
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function run($taskData)
    {
        $analysis = [];
        $urlMap = explode('.', $taskData['websiteUrl']);
        $client = new Client();

        // 二级域名的情况
        if (count($urlMap) == 3 && $urlMap[0] != 'www') {
            $analysis['c301'] = [
                'status' => 2,
                'msg' => '该网站使用的非一级域名，建议使用一级域名'
            ];

            $analysis['ssl'] = self::requestWithHttps($client, $taskData);

            return $analysis;
        }

        // 非二级域名的情况
        if (count($urlMap) == 3 && $urlMap[0] == 'www') {
            $url = 'http://' . $urlMap['1'] . '.' . $urlMap['2'];
            $domain = $urlMap['1'] . '.' . $urlMap['2'];
        } else {
            $url = 'http://' . $taskData['websiteUrl'];
            $domain = $taskData['websiteUrl'];
        }

        $userAgentMap = config('userAgent');
        $userAgentKey = array_rand($userAgentMap);
        $res = $client->request('GET', $url, [
            'allow_redirects' => false,
            'headers' => [
                'user-agent' => $userAgentMap[$userAgentKey]
            ],
        ]);

        // 做了 301 跳转的
        if ($res->getStatusCode() == 301) {
            // 进一步确认
            $resHeaders = $res->getHeaders();
            $location = $resHeaders['Location'][0];

            if (strpos($location, 'https://') !== false) {
                $analysis['ssl'] = [
                    'status' => 1,
                    'msg' => '该网站采用了安全的https协议'
                ];
            } else {
                $analysis['ssl'] = [
                    'status' => 2,
                    'msg' => '该网站没有采用了安全的https协议'
                ];
            }

            $ckDomain = strstr($location, 'www');
            if ($ckDomain !== false) {

                $analysis['c301'] = [
                    'status' => 1,
                    'msg' => '该网站采用正确的301跳转方式'
                ];

                return $analysis;
            }

            // 进一步确认
            if ($analysis['ssl']['status'] != 1) {
                $analysis['c301'] = [
                    'status' => 2,
                    'msg' => '该网站没有采用正确的301跳转方式'
                ];

                return $analysis;
            }

            // 去请求带https的www域名
            $res2 = $client->request('GET', 'https://www.' . $domain, [
                'allow_redirects' => false
            ]);

            if ($res2->getStatusCode() == 200) {
                $analysis['c301'] = [
                    'status' => 1,
                    'msg' => '该网站采用正确的301跳转方式'
                ];
            } else {
                $analysis['c301'] = [
                    'status' => 2,
                    'msg' => '该网站没有采用正确的301跳转方式'
                ];
            }

            return $analysis;
        }

        // 未做 301 跳转的
        $analysis['c301'] = [
            'status' => 2,
            'msg' => '该网站没有采用正确的301跳转方式'
        ];

        $analysis['ssl'] = self::requestWithHttps($client, $taskData);

        return $analysis;
    }

    private static function requestWithHttps($client, $taskData)
    {
        $ssl = [];
        $userAgentMap = config('userAgent');
        $userAgentKey = array_rand($userAgentMap);

        try {
            $res2 = $client->request('GET', 'https://' . $taskData['websiteUrl'], [
                'allow_redirects' => false,
                'headers' => [
                    'user-agent' => $userAgentMap[$userAgentKey]
                ],
            ]);

            if ($res2->getStatusCode() == 200) {
                $ssl = [
                    'status' => 1,
                    'msg' => '该网站采用了安全的https协议'
                ];
            } else {
                $ssl = [
                    'status' => 2,
                    'msg' => '该网站没有采用了安全的https协议'
                ];
            }
        } catch (\Exception $e) {

            $ssl = [
                'status' => 2,
                'msg' => '该网站没有采用了安全的https协议'
            ];
        }

        return $ssl;
    }
}