<?php

return [

    // 密码加密
    'pwd_salt' => '-0\3erk~',
    // 中文
    'lang' => [
        '简体中文' => 'zh',
        '繁体中文' => 'tc',
        '英语' => 'en',
        '日语' => 'ja',
        '德语' => 'de',
        '法语' => 'fa',
        '印度语' => 'id',
        '泰语' => 'th',
        '俄语' => 'ru',
    ],
    'langIcon' => [
        'zh' => "/system_file/flags/4x3/cn.svg",
        'tc' => "/system_file/flags/4x3/cn.svg",
        'en' => "/system_file/flags/4x3/gb.svg",
        'ja' => "/system_file/flags/4x3/jp.svg",
        'de' => "/system_file/flags/4x3/de.svg",
        'fa' => "/system_file/flags/4x3/gf.svg",
        'id' => "/system_file/flags/4x3/in.svg",
        'th' => "/system_file/flags/4x3/th.svg",
        'ru' => "/system_file/flags/4x3/ru.svg",
    ],
    // 关键词模版
    'keyword_template_path' => env('APP_HOST') . '/system_file/keyword_template.xlsx',
    // 授权链接查询
    'auth_query_url' => 'https://www.huocms.com/auth',
    // 每篇文章内链最大关键词数
    'keyword_num' => 1,
    // 每个关键词的最大替换次数
    'keyword_url' => 3,
    // 站点默认配置
    'website_setting' => [
        "site_name" => "",
        "seo_title" => "",
        "seo_keywords" => "",
        "seo_description" => "",
        "icp" => "",
        "gwa" => "",
        "admin_email" => "",
        "analytics_code" => "",
        "author" => "智火建站",
        "icp_link" => "https://beian.miit.gov.cn/#/Integrated/index",
        "gwa_link" => "http://www.beian.gov.cn/portal/index.do",
        "logo" => "",
        "customer_code" => "",
        "qq_code" => "",
        "customer_link" => "",
        "wechat_code" => "",
        "common_template" => 1,
        "static_file" => "",
        "dark_logo" => "",
        "alias_rule" => 2,
        "meta_link" => "",
        "favicon_link" => "",
    ],
    // suwork 接收的参数
    'suwork_accept' => [
        'company_name' => '',
        'contacts_name' => '',
        'telphone' => '',
        'phone' => '',
        'wechat' => '',
        'email' => '',
        'company_address' => '',
        'web_site' => '',
        'referer_type' => '',
        'referer' => '',
        'referer_web' => '',
        'ip' => '',
        'inquiry_time' => '',
        'expect_amount' => '',
        'explain' => '',
        'abutting_name' => ''
    ],
    // 百度翻译api
    'baidu_translate_api' => 'https://fanyi-api.baidu.com/api/trans/vip/translate',
    'baidu_translate_salt' => 'sfa8f`s13',
    // sitemap 提交地址
    // 百度
    'sitemap_baidu_api' => 'http://data.zz.baidu.com/urls',

    //图片格式后缀字段
        'image_ext'=>['webp', 'jpg', 'png', 'ico', 'bmp', 'gif', 'tif', 'pcx', 'tga', 'bmp', 'pxc', 'tiff', 'jpeg',
            'exif', 'fpx', 'svg', 'psd', 'cdr', 'pcd', 'dxf', 'ufo', 'eps', 'ai', 'hdri'],

    //视频格式后缀
        'video_ext'=>['mp4', 'avi', '3gp', 'rmvb', 'wmv', 'mkv', 'mpg', 'vob', 'mov', 'flv', 'swf', 'ape', 'wma', 'aac',
            'mmf', 'amr', 'm4a', 'm4r', 'ogg', 'wav', 'wavpack', 'm4v', 'webm'],

    //音频格式后缀
        'audio_ext'=>['mp3', 'mpeg', 'wma', 'mid', 'cd', 'wave', 'aiff', 'mpeg-4', 'midi', 'flac', 'acc', 'ape', 'amr'],
    //压缩包格式后缀
        'zip_ext'=>['rar', 'zip', 'tar', 'cab', 'uue', 'jar', 'iso', 'z', '7-zip', 'ace', 'lzh', 'arj', 'gzip', 'bz2', 'tz'],

        'file_ext'=>['exe', 'doc', 'ppt', 'xls','xlsx','wps', 'txt', 'lrc', 'wfs', 'torrent', 'html', 'htm', 'java', 'js',
            'css', 'less', 'php', 'pdf', 'pps', 'host', 'box', 'docx', 'word', 'perfect', 'dot', 'dsf', 'efe', 'ini',
            'json', 'lnk', 'log', 'msi', 'ost', 'pcs', 'tmp', 'xlsb',"ttf", "eot",'woff','woff2'],
        'font_ext'=>["ttf", "eot",'woff','woff2'],
    // 密码加密配置
    'rsa_public_key' => env('RSA_PUBLIC_KEY','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC5Bxjyw7r9IW08OMudzn0M9CBu3dvKtNOtbMuN7iEQPEosUMbfk3vpi8vX7pdGKHnzUZ47yh44FTcDyoBbOG+NseuTrirkdGCvXyTZyfAZ9TQ/FFBVG1cemoeA5/ry1PMMw6V6EGK8hBL6h/WWZTnksWiWvmLzneQyYphcL1UpKQIDAQAB'),
    'rsa_private_key' => env('RSA_PRIVATE_KEY','MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALkHGPLDuv0hbTw4y53OfQz0IG7d28q0061sy43uIRA8SixQxt+Te+mLy9ful0YoefNRnjvKHjgVNwPKgFs4b42x65OuKuR0YK9fJNnJ8Bn1ND8UUFUbVx6ah4Dn+vLU8wzDpXoQYryEEvqH9ZZlOeSxaJa+YvOd5DJimFwvVSkpAgMBAAECgYEAiLo68GI9EoMaAo+Bv8pv1Buuv96IZcG8ToJ/5RDcM/Apa5gBbgPBZHLenmF8Peb5PxnIESypqg3bSpzgvwf0PgJUNgkAbnt8RYe5IuOvOoPK31nWnh19psXjrPAvtrbLtFPkZeobR5lf7/NbDqptjYmuJKEUmW3UmACMu2ELGsUCQQDczyOz070Mf8kzFHEfiHji4WkYmlkqUvif3TRjaV5YHtnV+jqW8gtx8iHkIxiEgSskVC3cBLYeDU1KC9XjrzcXAkEA1oQZ2bu+4maZnnenS9H35/K/99zebbzHi60XQt//6101W1n4Vjkm/UsBocrWW+230OblFGkK1HbdCKpm5bbJvwJAVUS4aM0viBAgxlTyilbNN/AMAbA5FpsL39AwVuNtx2HChVsoHPpDaTEz3UGgSisHWfQX8YrwORKx8hI3+sDC6QJAcZVezrAfYWdsHcFDXt0dg75AMTDMwZQ8oFOVRn6u1mPzFZys5pK6d1GY2p85I1Hai41GbKk158vMO4NmmmlPpwJBAJ/uEzopf7Q4Mpjwwup9zyxCBh1n4u/GWNWNW7WVAZtN0Vol7NSsNOwuPysC6J4GBGUlFAFg0kAM+zbIWNZ/GZc='),
    // 懒加载图片大小
    'lazy_load_size' => env('LAZY_LOAD_SIZE','100kb')

];
