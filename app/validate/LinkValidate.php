<?php


namespace app\validate;


use \think\Validate;
class LinkValidate extends Validate
{
    protected $rule = [
        'id|友情链接ID' => 'require|number',
        'website_id|网站ID'=>'require|number',
        'name|链接名称' => 'require',
        'url|链接地址' => 'require|url',
        'type|友情链接类型' => 'require|in:1,2',
        'url_type|访问链接类型' => 'require|in:1,2,3,4|checkValue',
        'site_type|网站类型'  => 'require|in:1,2,3,4,5',
        'baidu_weight|百度权重' => 'number',
        'export_link|导入链接'    => 'number',
        'check_log|检查记录'  => 'in:1,2',
        'add_person|添加人' => 'require',
        'mobile|手机号' => 'mobile',
        'add_time|添加时间' => 'date',
        'end_time|截止时间' => 'date',
        'target|打开方式' => 'in:1,2',
        'copy_site_id|复制站点ID' => 'require|number',
    ];

    protected $scene = [
        'index' => ['website_id','type'],
        'read' => ['id','website_id','type'],
        'save' => ['website_id', 'name', 'url','type','url_type','site_type','baidu_weight','position','add_person','mobile','target','end_time','add_time','check_log','export_link'],
        'update' => ['id', 'name', 'url','type','url_type','site_type','baidu_weight','position','add_person','mobile','target','end_time','add_time','check_log','export_link'],
        'copy' => ['website_id','copy_site_id'],
    ];

    public function checkValue($value)
    {
        if(request()->param('type') == 1){
            if($value != 1 && $value != 2){
                return '访问链接类型错误';
            }
        }else{
            if($value != 1 && $value != 3 && $value != 4){
                return '访问链接类型错误';
            }
        }
        return true;
    }

}