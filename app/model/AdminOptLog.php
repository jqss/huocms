<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class AdminOptLog extends Model
{
    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getAdminOptLogList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->order('id desc')->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addAdminOptLog($param): array
    {
        try{
            $ip = request()->ip();
            $param['ip'] = $ip;
            $address = getLocationByIp($ip,2);
            if(empty($address['province'])){
                $area = lang("内网ip");
            }else{
                $area = $address['province']."-".$address['city'];
            }
            $param['addr'] = $area;
            $param['agent'] = request()->header()['user-agent'];
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res->id);
    }

    public function getAgentAttr($value): string
    {
        return getDeviceInfo($value)['deviceOs'];
    }
}