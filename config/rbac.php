<?php

return [
    'skip_auth' => [
        '/admin/self' => 1,
        '/dashboard/index' => 1,
        '/websitelang/index' => 1,
        '/websitesetting/read' => 1,
        '/websitelang/system' => 1,
        '/websiteserver/read' => 1,
        '/themefile/singleupdate' => 1,
        '/attachment/uploadandsave' => 1,
        '/theme/uninstall' => 1,
        '/theme/installtheme' => 1,
        '/content/imglocal' => 1,
        '/content/descextraction' => 1,
        '/content/addinnerchat' => 1,
        '/content/wordsearch' => 1,
        '/category/getmodulecate' => 1,
        '/website/index' => 1,
        '/keyword/template' => 1,
        '/keywordquery/index' => 1,
        '/domain/whois' => 1,
    ]
];
