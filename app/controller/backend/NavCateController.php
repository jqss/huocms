<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Nav;
use app\model\NavCate;
use app\service\CacheService;
use app\validate\NavCateValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class NavCateController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(NavCate $navCate): \think\response\Json
    {
        $siteId = (int)input('website_id');
        if(!$siteId){
             return jsonReturn(-1,Lang::get('网站ID不能为空'));
        }
        $lang = input('lang');
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'website_id' => $siteId,
            'lang' => $lang
        ];

        $navCateList = $navCate->getAllCustomArrayData($where,'id asc');
        return json($navCateList);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelNotUniqueException
     * @throws \ReflectionException
     */
    public function save(NavCate $navCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(NavCateValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // 唯一验证
            $navCate -> saveUnique(
                [
                    'seller_id'=>$this->admin['seller_id'],
                    'website_id' => $param['website_id'],
                    'title'=>$param['title'],
                    'lang' => $param['lang'],
                ],'导航分类已存在');

            $res = $navCate -> addNavCate($param);
            CacheService::deleteRelationCacheByObject($navCate);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(NavCate $navCate): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,lang('导航分类ID不能为空'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        $res = $navCate->getNavCate($where);
        return json($res);

    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \app\exception\ModelNotUniqueException
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function update(NavCate $navCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(NavCateValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            // 唯一验证
            $navCate -> updateUnique(
                [
                    'seller_id'=>$this->admin['seller_id'],
                    'website_id' => $param['website_id'],
                    'title'=>$param['title'],
                    'lang' => $param['lang'],
                ],$param['id'],lang('导航分类已存在'));
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $navCate -> updateNavCate($where,$param);
            CacheService::deleteRelationCacheByObject($navCate);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function delete(NavCate $navCate,Nav $Nav): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(NavCateValidate::class)->scene('delete')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $whereNav = [
                'nav_cate_id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'website_id' => $param['website_id'],
            ];
            $nav = $Nav->getNavList($whereNav)['data']->toArray();
            if(!empty($nav)){
                return jsonReturn(-2,lang('分类下有菜单不能直接删除'));
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'website_id' => $param['website_id'],
            ];
            $res = $navCate->delNavCate($where);
            CacheService::deleteRelationCacheByObject($navCate);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
