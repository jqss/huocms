<?php


namespace app\service;


use app\service\upload\Upload;

class ComponentDataService
{

    /**
     *
     */
    public function getStaticImage($path)
    {
        $path = 'view/website/3/default/assets/images';
        // 获取所有图片
        dd(234);
    }


    /**
     * 静态图片资源上传编辑
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function staticImageUpload($param): array
    {
        $upload = new Upload();
        $flag = $upload->create($param['file'], $param['seller_id'],$param['type'],$param['asset']);
        if($flag){
            return $upload->getUploadFileInfo();
        }else{
            return ['code'=>-2,'msg'=>lang('上传失败')];
        }
    }

    public function backendTagEdit($param)
    {

    }

    public function backendDefaultEdit($param)
    {

    }


}