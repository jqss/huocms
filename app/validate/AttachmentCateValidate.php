<?php


namespace app\validate;


class AttachmentCateValidate extends \think\Validate
{
    protected $rule = [
        'id|栏目ID' => 'require|number',
        'title|栏目名称' => 'require',
        'parent_id|父栏目ID' => 'require|number'
    ];

    protected $scene = [
        'save' => ['title','parent_id'],
        'update' => ['id', 'title','parent_id']
    ];
}