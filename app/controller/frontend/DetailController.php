<?php
declare (strict_types = 1);

namespace app\controller\frontend;

use app\exception\ModelEmptyException;
use app\model\Category;
use app\model\SysSetting;
use app\service\ContentService;
use think\facade\Cache;

class DetailController extends BaseController
{

    /**
     * 内容详情
     * @param Category $Category
     * @return mixed
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function index(Category $Category)
    {
        $id = (int)input('id');
        $cid = (int)input('cid');
        $cateCacheKey = 'hc_detail_cate_' .$this->sellerId.'_'.$this->siteId.'_'.$this->lang.'_'. $cid ;
        $category = cache($cateCacheKey);
        if(empty($category)){
            try {
                $category = $Category->getCategory(['id' => $cid, 'seller_id' => $this->sellerId, 'website_id' => $this->siteId,'lang' => $this->lang],['module.moduleField'])['data']->toArray();
                Cache::set($cateCacheKey,$category);
            }catch (ModelEmptyException $me){
                $category = $Category->getCategory(['id' => $cid, 'seller_id' => $this->sellerId, 'website_id' => $this->realSiteId,'lang' => $this->lang],['module.moduleField'])['data']->toArray();
                Cache::set($cateCacheKey,$category);
            }
        }
        if (!empty($category['content'])) {
            $category['content'] = htmlspecialchars_decode($category['content']);
        }
        $contentService = new ContentService();
        $param = [
            'cate' => $category,
            'id' => $id,
            'seller_id' => $this->sellerId,
            'website_id' => $this->siteId,
            'lang' => $this->lang
        ];
        $content = $contentService->getContentDetail($param);
        $content->hits =  $content->hits + 1;
        $content->save();

        if (!empty($content['content'])) {
            $content['content'] = htmlspecialchars_decode($content['content']);
        }

        $where = [
            'group' => 'company',
            'title' => 'huocms_powerby',
        ];
        $huocmsPowerby = SysSetting::where($where)->value('value');
        if ($huocmsPowerby) {
            $category['seo_title'] .= " - $huocmsPowerby";
            $category['seo_keywords'] .= "-$huocmsPowerby";
            $category['seo_description'] .= ",$huocmsPowerby";
            $content['seo_title'] .= "-$huocmsPowerby";
            $content['seo_keyword'] .= ",$huocmsPowerby";
            $content['seo_description'] .= ",$huocmsPowerby";
        }

        $this->assign('hc_content',$content->toArray());
        $this->assign('current_cate',$category);
        $this->assign('root_domain',$this->rootDomain);
        $template = !empty($content['detail_tpl']) ? $content['detail_tpl'] : $category['detail_tpl'];
        return $this->fetch($template);
    }

}
