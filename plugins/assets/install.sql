DROP TABLE IF EXISTS `$prefix$assets_domain`;
CREATE TABLE `$prefix$assets_domain`
(
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`     int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `domain`        varchar(100)   NOT NULL DEFAULT '' COMMENT '域名',
    `desc`          varchar(255)   NOT NULL DEFAULT '' COMMENT '描述',
    `holder_name`   varchar(120)   NOT NULL DEFAULT '' COMMENT '持有者',
    `registrants`   varchar(120)   NOT NULL DEFAULT '' COMMENT '注册商',
    `price`         decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '价格',
    `status`        varchar(120)   NOT NULL DEFAULT '' COMMENT '域名状态',
    `register_date` datetime                DEFAULT NULL COMMENT '注册时间',
    `expire_date`   datetime                DEFAULT NULL COMMENT '到期时间',
    `hint`          varchar(255)   NOT NULL DEFAULT '' COMMENT '提示',
    `whois`         text COMMENT 'whois详情',
    `sort`          int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `user_id`       int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
    `create_time`   datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`   datetime                DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-域名表';

DROP TABLE IF EXISTS `$prefix$assets_server`;
CREATE TABLE `$prefix$assets_server`
(
    `id`               int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`        int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title`            varchar(120)   NOT NULL DEFAULT '' COMMENT '名称',
    `desc`             varchar(255)   NOT NULL DEFAULT '' COMMENT '描述',
    `public_ip`        varchar(20)    NOT NULL DEFAULT '' COMMENT '公网ip',
    `service_provider` varchar(120)   NOT NULL DEFAULT '' COMMENT '服务商',
    `price`            decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '价格',
    `os`               varchar(120)   NOT NULL DEFAULT '' COMMENT '操作系统',
    `buy_date`         datetime                DEFAULT NULL COMMENT '购买时间',
    `expire_date`      datetime                DEFAULT NULL COMMENT '到期时间',
    `hint`             varchar(255)   NOT NULL DEFAULT '' COMMENT '提示',
    `user_id`          int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
    `create_time`      datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`      datetime                DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `public_ip` (`public_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-服务器表';

DROP TABLE IF EXISTS `$prefix$assets_trademark`;
CREATE TABLE `$prefix$assets_trademark` (
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
   `title` varchar(120) NOT NULL DEFAULT '' COMMENT '名称',
   `desc` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
   `category` varchar(255) NOT NULL DEFAULT '' COMMENT '分类（所属行业）',
   `apply_code` varchar(50) NOT NULL DEFAULT '' COMMENT '申请/注册号',
   `apply_user` varchar(120) NOT NULL DEFAULT '' COMMENT '申请人',
   `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
   `valid_time` datetime DEFAULT NULL COMMENT '有效时间',
   `agency` varchar(255) NOT NULL DEFAULT '' COMMENT '代理机构',
   `certificate_img` varchar(255) NOT NULL DEFAULT '' COMMENT '证书图片',
   `trademark_img` varchar(255) NOT NULL DEFAULT '' COMMENT '商标图片',
   `issuing_authority` varchar(255) NOT NULL DEFAULT '' COMMENT '发证机关',
   `register_address` varchar(255) NOT NULL DEFAULT '' COMMENT '注册地址',
   `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
   `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
   PRIMARY KEY (`id`) USING BTREE,
   UNIQUE KEY `apply_code` (`apply_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-商标表';

DROP TABLE IF EXISTS `$prefix$assets_copyright`;
CREATE TABLE `$prefix$assets_copyright`
(
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`     int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `category`      tinyint(2) NOT NULL DEFAULT '1' COMMENT '版权类别 1采购2自有',
    `type`          tinyint(2) NOT NULL DEFAULT '1' COMMENT '类型 1图片2音频3视频4字体',
    `apply_user`    varchar(120) NOT NULL DEFAULT '' COMMENT '创建人',
    `price`         decimal(15, 2)        DEFAULT '0.00' COMMENT '价格',
    `doc_no`        varchar(220)          DEFAULT '' COMMENT '授权文件号',
    `use_range`     varchar(255)          DEFAULT '' COMMENT '使用范围',
    `subject`       varchar(255)          DEFAULT '' COMMENT '授权的主体',
    `supplier_name` varchar(255)          DEFAULT '' COMMENT '供应商名称',
    `material_type` tinyint(2) DEFAULT '1' COMMENT '1录入网址 2上传文件',
    `begin_time`    date                  DEFAULT NULL COMMENT '开始时间',
    `end_time`      date                  DEFAULT NULL COMMENT '结束时间',
    `files`         text COMMENT '授权文件',
    `apply_id`      int(11) DEFAULT '0' COMMENT '创建人id',
    `create_time`   datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`   datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-版权表';

DROP TABLE IF EXISTS `$prefix$assets_copyright_detail`;
CREATE TABLE `$prefix$assets_copyright_detail`
(
    `detail_id`      int(11) unsigned NOT NULL AUTO_INCREMENT,
    `seller_id`      int(11) DEFAULT '1' COMMENT '商户的id',
    `copyright_id`   int(11) unsigned DEFAULT '0' COMMENT '版权的id',
    `material_type`  tinyint(2) DEFAULT '1' COMMENT '1录入网址 2上传文件',
    `material_no`    varchar(220) DEFAULT '' COMMENT '素材编号',
    `material_name`  varchar(220) DEFAULT '' COMMENT '素材名称',
    `material_link`  varchar(255) DEFAULT '' COMMENT '素材内容',
    `material_cover` varchar(255) DEFAULT '' COMMENT '素材封面',
    `material_url`   varchar(255) DEFAULT '' COMMENT '使用网址',
    `category`       tinyint(2) DEFAULT '1' COMMENT '类别 1采购2自有',
    `type`           tinyint(2) DEFAULT '1' COMMENT '类型 1图片2视频 3音频 4字体',
    `create_time`    datetime     DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版权的素材信息';

DROP TABLE IF EXISTS `$prefix$assets_soft`;
CREATE TABLE `$prefix$assets_soft`
(
    `id`           int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`    int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title`        varchar(120) NOT NULL DEFAULT '' COMMENT '证书名称',
    `reg_code`     varchar(50)  NOT NULL DEFAULT '' COMMENT '登记号',
    `code`         varchar(50)  NOT NULL DEFAULT '' COMMENT '证书编号',
    `apply_user`   varchar(120) NOT NULL DEFAULT '' COMMENT '著作权人',
    `right_scope`  varchar(255) NOT NULL DEFAULT '' COMMENT '权利范围',
    `grant_office` varchar(255) NOT NULL DEFAULT '' COMMENT '发证机关',
    `grant_time`   date                  DEFAULT NULL COMMENT '发证日期',
    `grant_file`   varchar(255) NOT NULL DEFAULT '' COMMENT '登记证书url',
    `user_id`      int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
    `create_time`  datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`  datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='虚拟资产-软著表';
