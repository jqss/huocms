<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;

/**
 * @mixin \think\Model
 */
class ThemeWebsite extends Model
{


    /**
     * @throws ModelException
     */
    public function getColumn($where, $column): array
    {
        try{
            $res =  $this->where($where)->column($column);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @throws ModelException
     */
    public function editData($where, $param): array
    {
        try{
            $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }
}
