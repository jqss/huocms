<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;

/**
 * @mixin \think\Model
 */
class CategorySubContent extends Model
{
    /**
     * @throws ModelException
     */
    public function getAllCategorySubContent($where): array
    {
        try{
            $res =  $this->where($where)->select()->toArray();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }
}
