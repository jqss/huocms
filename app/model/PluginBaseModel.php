<?php


namespace app\model;


use app\exception\ModelException;
use Exception;

class PluginBaseModel extends Model
{
    protected $hidden = [
        'password','seller_id'
    ];

    /**
     * 获取列表
     * @param $where
     * @param string $with
     * @param string $limit
     * @param string $order
     * @param string $field
     * @return array
     * @throws ModelException
     */
    public function getList($where = [], $with = '', $limit = '10', $order = "id desc", $field = "*")
    {
        try {
            $list = $this->where($where)->with($with)->field($field)->order($order)->paginate($limit);
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), -1);
        }
        return dataReturn(0, lang('查询成功'), $list);
    }


    /**
     * 不带分页的列表
     * @param array $where
     * @param string $with
     * @param string $order
     * @param string $field
     * @return array
     * @throws ModelException
     */
    public function getListNoLimit($where = [], $with = '', $order = "id desc", $field = "*")
    {
        try {
            $list = $this->where($where)->with($with)->field($field)->order($order)->select();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), -1);
        }
        return dataReturn(0, lang('查询成功'), $list);
    }

    /**
     * 获取详情
     * @param array $where
     * @param string $with
     * @param string $field
     * @return array
     * @throws ModelException
     */
    public function getInfo($where = [], $with = '', $field = "*")
    {
        try {
            $info = $this->where($where)->with($with)->field($field)->find();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), -1);
        }
        return dataReturn(0, lang('查询成功'), $info);
    }

    /**
     * 添加信息
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function add($param)
    {
        try {
            $id = self::insertGetId($param);
        } catch (Exception $e) {
            throw new ModelException($e->getMessage());
        }
        return dataReturn(0, lang('添加成功'), $id);
    }

    /**
     * 修改信息
     * @param $where
     * @param $data
     * @return array
     * @throws ModelException
     */
    public function edit($where, $data)
    {
        try {
            $this->where($where)->update($data);
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), -1);
        }
        return dataReturn(0, lang('操作成功'));
    }

    /**
     * 是否存在
     * @param $where
     * @param string $IdField
     * @return bool
     */
    public function isExist($where, $IdField = 'id'): bool
    {
        $id = $this->where($where)->value($IdField);
        if ($id) {
            return true;
        } else {
            return false;
        }
    }
}
