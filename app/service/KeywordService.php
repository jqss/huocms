<?php

namespace app\service;

use app\model\Keyword;
use app\model\KeywordQuery;
use app\model\SysSetting;
use think\facade\Log;

class KeywordService
{
    public static $baiduPcUrl = 'https://apidatav2.chinaz.com/single/baidupc/keywordranking';
    public static $baiduMobileUrl = 'https://apidatav2.chinaz.com/single/baidumobile/keywordranking';
    public static $pc360Url = 'https://apidatav2.chinaz.com/single/sopc/keywordranking';
    public static $sogouMobileUrl = 'https://apidatav2.chinaz.com/single/sogoumobile/keywordranking';

    public static function getChinaZToken($sellerId)
    {
        $sysSettingModel = new SysSetting();

        return $sysSettingModel->where([
            ['title', '=', 'chinaz_token'],
            ['seller_id', '=', $sellerId]
        ])->value('value');
    }

    public static function baiduPcRank($token, $keywordData)
    {
        // 百度PC
        $res = curlPost(self::$baiduPcUrl, [
            'key' => $token,
            'domain' => $keywordData['url'],
            'keyword' => $keywordData['name'],
            'top100' => true
        ])['data'];

        Log::info($keywordData['name'] . ' 单个更新 百度PC res>>>>>>>:' . $res);
        $res = json_decode($res, true);

        if (!empty($res['Result']['Ranks'])) {
            $rank = explode('-', $res['Result']['Ranks']['0']['RankStr']);
            $updateData['baidu_pc'] = ($rank[0] - 1) * 10 + $rank['1'];

            self::writeKeywordsQuery($keywordData, $res, $rank, 'baidu_pc');

            $keywordModel = new Keyword();
            $keywordModel->updateKeyword(['id', $keywordData['id']], $updateData);
        }
    }

    public static function baiduMobileRank($token, $keywordData)
    {
        // 百度移动
        $res = curlPost(self::$baiduMobileUrl, [
            'key' => $token,
            'domain' => $keywordData['url'],
            'keyword' => $keywordData['name'],
            'top100' => true
        ])['data'];

        Log::info($keywordData['name'] . ' 单个更新 百度移动 res>>>>>>>:' . $res);
        $res = json_decode($res, true);

        if (!empty($res['Result']['Ranks'])) {
            $rank = explode('-', $res['Result']['Ranks']['0']['RankStr']);
            $updateData['baidu_mob'] = ($rank[0] - 1) * 10 + $rank['1'];

            self::writeKeywordsQuery($keywordData, $res, $rank, 'baidu_mob');

            $keywordModel = new Keyword();
            $keywordModel->updateKeyword(['id', $keywordData['id']], $updateData);
        }
    }

    public static function pc360Rank($token, $keywordData)
    {
        // 360pc
        $res = curlPost(self::$pc360Url, [
            'key' => $token,
            'domain' => $keywordData['url'],
            'keyword' => $keywordData['name'],
            'top100' => true
        ])['data'];

        Log::info($keywordData['name'] . ' 单个更新 360pc res>>>>>>>:' . $res);
        $res = json_decode($res, true);

        if (!empty($res['Result']['Ranks'])) {
            $rank = explode('-', $res['Result']['Ranks']['0']['RankStr']);
            $updateData['three_pc'] = ($rank[0] - 1) * 10 + $rank['1'];

            self::writeKeywordsQuery($keywordData, $res, $rank, 'three_pc');

            $keywordModel = new Keyword();
            $keywordModel->updateKeyword(['id', $keywordData['id']], $updateData);
        }
    }

    public static function sougouMobileRank($token, $keywordData)
    {
        // 搜狗移动
        $res = curlPost(self::$sogouMobileUrl, [
            'key' => $token,
            'domain' => $keywordData['url'],
            'keyword' => $keywordData['name'],
            'top100' => true
        ])['data'];

        Log::info($keywordData['name'] . ' 单个更新 搜狗移动 res>>>>>>>:' . $res);
        $res = json_decode($res, true);

        if (!empty($res['Result']['Ranks'])) {
            $rank = explode('-', $res['Result']['Ranks']['0']['RankStr']);
            $updateData['sougou_mob'] = ($rank[0] - 1) * 10 + $rank['1'];

            self::writeKeywordsQuery($keywordData, $res, $rank, 'sougou_mob');

            $keywordModel = new Keyword();
            $keywordModel->updateKeyword(['id', $keywordData['id']], $updateData);
        }
    }

    public static function writeKeywordsQuery($vo, $res, $rank, $engine)
    {
        $keywordQueryModel = new KeywordQuery();
        $keywordQueryModel->insert([
            'keyword_id' => $vo['id'],
            'seller_id' => $vo['seller_id'],
            'website_id' => $vo['website_id'],
            'keyword' => $vo['name'],
            'search_engine' => $engine,
            'collect_count' => isset($res['Result']['SiteCount']) ? $res['Result']['SiteCount'] : 0,
            'top_rank' => ($rank[0] - 1) * 10 + $rank['1'],
            'create_time' => time(),
            'page_title' => $res['Result']['Ranks']['0']['Title'] ?? '',
            'page_url' => $res['Result']['Ranks']['0']['Url'] ?? '',
        ]);
    }

}