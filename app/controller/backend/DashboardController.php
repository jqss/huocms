<?php
declare (strict_types = 1);

namespace app\controller\backend;



use think\facade\Db;

class DashboardController extends BaseController
{
    /**
     * 后台首页看板
     *
     */
    public function index(): \think\Response
    {
        if(!hcInstalled()){
            createInstallFile();
        }
        $install_date = filectime(app()->getRootPath().'public/system_file/install.lock');
        // 运行天数
        $total_day = (int)ceil((time() - $install_date) / (24*3600)) ?: 1;
        // 总站点数
        $site_count =  Db::name('website')->where('seller_id',$this->admin['seller_id'])->count('id');
        // 总访客数
        $visit_count = count(Db::name('visit_log')->distinct(true)->field('ip,visited_time')->select()->toArray());
        // 总文章数 (模型为系统文章模型)
        $article_count = Db::name('sub_content')->where('is_del',1)->count('id');
        // 总询盘数
        $inquiry_count = Db::name('inquiry')->where('is_del',1)->count('id');
        // 总关键词数
        $keyword_count = Db::name('keyword')->where('is_del',1)->count('id');
        // 总导入链接数
        $in_link_count = Db::name('link')->where(['type'=>1,'is_del'=>1])->count('id');
        // 总导出链接数
        $out_link_count = Db::name('link')->where(['type'=>2,'is_del'=>1])->count('id');
        // 授权信息     // https://www.huocms.com/auth
        $domain = request()->param('domain');
        if(empty($domain)){
            $auth_info = null;
        }else{
            $authRes = curlPost(config('system.auth_query_url'),['domain'=> request()->param('domain')])['data'];
            $auth_info = json_decode($authRes,true);
            if(empty($auth_info['data'])){
                $auth_info = null;
            }else{
                $auth_info =  $auth_info['data'];
            }
            $websiteTitle = Db::name('website')->where('domain',$domain)->where('seller_id',$this->admin['seller_id'])->value('title');
        }
        return jsonReturn(0,'success',[
            'total_day'    => $total_day,
            'site_count'   => $site_count,
            'visit_count'  => $visit_count,
            'article_count'  => $article_count,
            'inquiry_count' => $inquiry_count,
            'keyword_count'    => $keyword_count,
            'in_link_count'    => $in_link_count,
            'out_link_count'   => $out_link_count,
            'auth_info' => $auth_info,
            'version' => file_get_contents(app()->getRootPath().'version'),
            'website_title' => $websiteTitle ?? '',
        ]);

    }

}
