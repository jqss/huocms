<?php

return [
    '登录后台系统'=>'login management system',
    '用户名密码不正确' => "incorrect username / password",
    '该账号已经被禁用' => "This account has been disabled",
    '登录成功' => "login successful",
    '成功'=>'success',
    '管理员ID不能为空' => "Admin ID cannot be empty",
    '系统管理员不能修改' => "System administrators cannot modify",
    '请求方法错误' => "wrong request method",
    '开启短信验证登录需填写手机号！' => "To enable SMS verification and log in, you need to fill in your mobile phone number!",
    '分类ID不能为空' => "Category ID cannot be empty",
    '分类下有附件不能直接删除' => "There are attachments under the category that cannot be deleted directly",
    '分类下面有子分类，不能直接删除' => "There are subcategories under the category, which cannot be deleted directly",
    '文件不存在！' => "file does not exist!",
    '附件ID不能为空' => "Attachment ID cannot be empty",
    '主键不能为空' => "primary key cannot be empty",
    '当前页数不能为空' => "The current page number cannot be empty",
    '当前页数必须为数字' => "The current page number must be a number",
    '文件名称' => "file name",
    '文件地址' => "file address",
    '文件类型' => "file type",
    '存储方式' => "storage method",
    '文件大小' => "File size",
    '批量删除' => "batch deletion",
    '网站ID不能为空' => "Website ID cannot be empty",
    '别名已被系统占用，请修改别名' => "The alias has been taken by the system, please modify the alias",
    '栏目已经存在' => "column already exists",
    '栏目别名已经存在' => "Column alias already exists",
    '请设置审核人员！' => "Please set reviewers!",
    '栏目ID' => "Column ID",
    '父级栏目ID' => "parent column id",
    '模型' => "Model",
    '栏目名称' => "program name",
    '栏目类型' => "column type",
    '列表模板' => "list template",
    '语言' => "language",
    '详情模板' => "Details template",
    '栏目排序' => "column sorting",
    '别名' => "alias",
    '网站ID' => "website ID",
    '模型ID' => "model ID",
    '原始网站ID' => "original site id",
    '目标网站ID' => "Target site ID",
    '父站点栏目' => "parent site column",
    "别名请以'/'开头" => "Aliases please start with '/'",
    "别名请用英文字母数字和'-,_,/'组合" => "Please use the combination of English letters and numbers and '-,_,/' for the alias",
    '别名请以英文字母或者数字结尾' => "Please end the alias with an English letter or number",
    '请输入正确的URL格式' => "Please enter the correct URL format",
    '详情页面模版不能为空' => "Details page template cannot be empty",
    '列表页面模版不能为空' => "List page template cannot be empty",
    '栏目下面有子栏目，不能直接删除' => "There are sub-columns under the column, which cannot be deleted directly",
    '删除栏目,栏目ID为' => "Delete column, the column ID is",
    '栏目管理' => "column management",
    '目标站点数据异常，请检查' => "The target site data is abnormal, please check",
    '内容ID' => "Content ID",
    '副表内容' => "Sub table content",
    '类型' => "type",
    '状态' => "state",
    '推荐' => "recommend",
    '置顶操作' => "Top operation",
    '站点ID' => "site ID",
    '文件' => "document",
    '没有配置密钥，请去系统设置-第三方设置中填写词爪密钥' => "There is no configuration key, please go to system settings - third-party settings to fill in the Cipaw key",
    '内容不存在' => "content does not exist",
    '模型方法错误' => "model method error",
    '参数错误' => "Parameter error",
    '审核失败！版本' => "Audit failure! Version",
    '待审核信息不存在' => "The information to be reviewed does not exist",
    '审核失败！' => "Audit failure!",
    '审核通过' => "examination passed",
    '审核驳回' => "Review rejected",
    '栏目内容' => "column content",
    '恢复成功' => "Recovery succeeded",
    '栏目ID不能为空' => "Column ID cannot be empty",
    '表名不能为空' => "Table name cannot be empty",
    '请传入文件' => "Please pass in the file",
    '上传文件为空' => "Upload file is empty",
    '方法请求错误' => "Method request error",
    '优化成功' => "Optimized successfully",
    '备份文件不存在' => "backup file does not exist",
    '恢复失败' => "Recovery failed",
    '数据库为空' => "database is empty",
    '表名不存在' => "table name does not exist",
    '备份失败' => "Backup failed",
    '备份成功' => "backup successful",
    '修复成功' => "successful repair",
    '下载失败' => "download failed",
    '文件名不能为空' => "File name cannot be empty",
    '发布成功' => "Published successfully",
    '执行失败' => "Execution failed",
    '可能的原因是缺少写权限或文件路径不正确' => "Possible causes are lack of write permission or incorrect file path",
    '关键词重复' => "keyword repetition",
    '内链ID不能为空' => "Internal link ID cannot be empty",
    '分类重复' => "Duplicate categories",
    '系统错误请重试' => "System error please try again",
    '内容已存在' => "content already exists",
    '友情链接名称已存在' => "Friendly link name already exists",
    '友情链接地址已存在' => "The friendship link address already exists",
    '友情链接' => "Links",
    '目标网站ID和复制网站ID不能相等' => "Destination site id and copy site id cannot be equal",
    '友情链接ID' => "Friendship link ID",
    '链接名称' => "link name",
    '链接地址' => "link address",
    '友情链接类型' => "Friendship link type",
    '访问链接类型' => "access link type",
    '网站类型' => "website type",
    '百度权重' => "Baidu weight",
    '导入链接' => "import link",
    '检查记录' => "Inspection records",
    '添加人' => "add a person",
    '手机号' => "Phone number",
    '添加时间' => "add time",
    '截止时间' => "deadline",
    '打开方式' => "open method",
    '复制站点ID' => "copy site id",
    '访问链接类型错误' => "Access link type error",
    '内链ID' => "Internal link ID",
    '关键词' => "Key words",
    '权重' => "Weights",
    '内链类型' => "Internal link type",
    '内链状态' => "Internal link status",
    '模型名称' => "model name",
    '模型表名' => "model table name",
    '表名格式错误' => "Table name format error",
    '模型不存在' => "model does not exist",
    '系统模型不能删除' => "System models cannot be deleted",
    '模型有关联的栏目，不能直接删除' => "The model has associated columns, which cannot be deleted directly",
    '模型删除成功' => "Model deleted successfully",
    '模型创建成功' => "Model created successfully",
    '模型ID不能为空' => "Model ID cannot be empty",
    '字段ID' => "Field ID",
    '标题' => "title",
    '数据表字段名称' => "Data table field name",
    '字段类型' => "Field Type",
    '字段排序' => "field sort",
    '字段状态' => "field status",
    '字段空值设置' => "Field empty value setting",
    '数据源' => "data source",
    '关联关系' => "connection relation",
    '数据表字段请以小写字母和下划线格式添加,下划线不能在开头和结尾' => "Please add data table fields in lowercase letters and underscore format, underscores cannot be at the beginning and end",
    '数据源不能为空' => "Data source cannot be empty",
    '关联关系不能为空' => "Relationship cannot be empty",
    '关联数据类型错误' => "Associated data type error",
    '关联数据表不能为空' => "The associated data table cannot be empty",
    '字段名不能添加数字' => "Field names cannot add numbers",
    '字段ID不能为空' => "Field ID cannot be empty",
    '关联字段不能编辑' => "Associated fields cannot be edited",
    '字段类型不存在' => "field type does not exist",
    '导航分类已存在' => "Navigation category already exists",
    '导航分类ID不能为空' => "Navigation Category ID cannot be empty",
    '导航分类ID' => "Navigation Category ID",
    '导航分类名称' => "Navigation category name",
    '站点语言' => "site language",
    '分类下有菜单不能直接删除' => "There is a menu under the category that cannot be deleted directly",
    '菜单ID' => "menu ID",
    '菜单标题' => "menu title",
    '父级菜单ID' => "parent menu id",
    '菜单分类ID' => "Menu category ID",
    '菜单状态' => "menu state",
    '菜单类别' => "menu category",
    '菜单地址' => "menu address",
    '菜单排序' => "menu sort",
    '菜单地址不能为空' => "Menu URL cannot be empty",
    '链接设置错误' => "link setting error",
    '导航已存在' => "navigation already exists",
    '有子菜单不能删除' => "There are submenus that cannot be deleted",
    '插件卸载失败' => "Plugin uninstall failed",
    '插件预安装失败' => "Plugin pre-installation failed",
    '插件信息缺失' => "Plugin information is missing",
    '插件已安装' => "plugin installed",
    '插件不存在' => "plugin does not exist",
    '数据ID不能为空' => "Data ID cannot be empty",
    '恢复数据参数错误' => "Recovery data parameter error",
    '恢复数据参数不能为空' => "The restore data parameter cannot be empty",
    '恢复数据不存在' => "recovery data does not exist",
    '请选择相同类型的内容恢复' => "Please select the same type of content to restore",
    '角色已经存在' => "role already exists",
    '角色ID不能为空' => "Role ID cannot be empty",
    '系统默认角色,不能编辑' => "System default role, cannot be edited",
    '角色名称' => "Role Name",
    '角色不存在' => "role does not exist",
    '系统默认角色，不能删除' => "System default role, cannot be deleted",
    'Id不能为空' => "Id cannot be empty",
    '生成成功' => "Generated successfully",
    '分类ID' => "Category ID",
    '分类名称' => "Category Name",
    '幻灯片ID' => "Slide ID",
    '幻灯片名称' => "slide title",
    '幻灯片地址' => "slideshow address",
    '幻灯片图片' => "slideshow pictures",
    '收件箱账号' => "inbox account",
    '发件箱账号' => "Outbox account",
    '发件箱密码' => "outbox password",
    '发件箱账号类型' => "Outbox account type",
    '图片大小' => "Image size",
    '视频大小' => "video size",
    '音频大小' => "audio size",
    '压缩包大小' => "Compression size",
    '图片类型' => "image type",
    '视频类型' => "video type",
    '音频类型' => "audio type",
    '压缩包类型' => "Compression type",
    '接收密钥ID' => "Receive Key ID",
    '接收密钥密码' => "receive key password",
    '根地址' => "root address",
    '容器名' => "container name",
    '地区' => "area",
    '协议头' => "protocol header",
    '分组' => "group",
    '分类' => "Classification",
    '分类信息错误' => "Category information error",
    '标签ID' => "Tag ID",
    '标签名称' => "label name",
    '是否推荐' => "Is it recommended",
    '标签点击数' => "Tag Hits",
    '标签名称已经存在' => "label name already exists",
    '标签ID不能为空' => "Tag ID cannot be empty",
    '模板ID' => "Template ID",
    '模版名称' => "template name",
    '模版语言' => "template language",
    '访问方法错误' => "access method error",
    '相同模版已经安装,请修改模版名称' => "The same template has already been installed, please modify the template name",
    '卸载成功' => "uninstalled successfully",
    '模版更新成功' => "Template updated successfully",
    '模板安装文件不存在' => "template does not exist",
    '模板安装失败' => "Template installation failed",
    '模版安装成功' => "Template installed successfully",
    '未启用模版' => "Template not enabled",
    '模版安装启用错误' => "Template installation error",
    '模版名称不能为空' => "Template name cannot be empty",
    '文件路径有误' => "wrong file path",
    '文件路径不能为空' => "The file path cannot be empty",
    '文件路径' => "file path",
    '文件内容' => "document content",
    '区块名称' => "block name",
    '文件不存在' => "file does not exist",
    '更新成功' => "update completed",
    '更新失败' => "update failed",
    '该站点没有启用的模板' => "There are no templates enabled for this site",
    '文件过大，请修改上传限制或者替换小的文件' => "The file is too large, please modify the upload limit or replace a smaller file",
    '文件格式错误，请重新上传' => "The file format is wrong, please upload again",
    '上传失败请重新尝试' => "Upload failed, please try again",
    '网站名称已存在' => "Site name already exists",
    '网站名称' => "website name",
    '父站点ID' => "parent site id",
    '网站域名' => "website domain name",
    '语言选择错误' => "Wrong language selection",
    '服务器类型' => "server type",
    '网站服务器' => "web server",
    '服务器ip地址' => "server ip address",
    'ssh账号' => "ssh account",
    'ssh密码' => "ssh password",
    'shh端口' => "shh port",
    '网站根目录' => "website root directory",
    '配置' => "configuration",
    '请求错误' => "wrong request",
    '查询成功' => "search successful",
    '操作成功' => "Successful operation",
    '操作失败' => "operation failed",
    '用户ID' => "User ID",
    '用户名' => "username",
    '密码' => "password",
    '用户组' => "user group",
    '邮箱' => "Mail",
    '性别' => "gender",
    '用户状态' => "user status",
    '生日' => "Birthday",
    '昵称' => "Nick name",
    '地址' => "address",
    '密码修改成功' => "Password reset complete",
    '密码修改失败' => "Password modification failed",
    '验证码' => "verification code",
    '确认密码' => "Confirm Password",
    '手机号验证码错误' => "Mobile phone number verification code error",
    '验证码不存在' => "verification code does not exist",
    '验证码已过期' => "Verification code has expired",
    '验证成功' => "Verification successful",
    '邮箱验证码错误' => "Email verification code error",
    '用户信息不存在' => "User information does not exist",
    '新旧号码不能一致' => "The old and new numbers cannot match",
    '号码已存在，请修改号码' => "The number already exists, please modify the number",
    '修改成功' => "Successfully modified",
    '修改失败' => "fail to edit",
    '退出成功' => "exit successfully",
    '文件不能为空' => "file cannot be empty",
    '获取成功' => "get success",
    '文件失效' => "file failure",
    '上传失败' => "upload failed",
    '用户已注册请去登录' => "The user has already registered, please log in",
    '账号已被禁用！请联系管理员!' => "Account has been disabled! Please contact the administrator!",
    '账号还未注册！请先去注册！' => "The account has not been registered yet! Please register first!",
    '认证失败！' => "Authentication failed!",
    '企业名称' => "Company Name",
    '企业网址' => "Enterprise website",
    '企业介绍' => "Company Introduction",
    '广告语' => "Advertising slogan",
    '营业执照' => "business license",
    '扩展属性' => "extended attributes",
    '拒绝原因' => "Denial Reason",
    '手机号已被使用' => "phone number already used",
    '邮箱已被使用' => "Email is already in use",
    '请输入拒绝理由' => "Please enter reason for rejection",
    '请输入号码' => "please enter the number",
    '类型错误' => "type error",
    '您的请求太频繁了，请稍等60秒后再发' => "Your request is too frequent, please wait for 60 seconds before sending",
    '发送成功' => "Sent successfully",
    '请输入邮箱' => "please input your email",
    '您的登录/企业登录/注册/企业注册验证码为：123456，5分钟内有效' => "Your login/enterprise login/registration/enterprise registration verification code is: 123456, valid within 5 minutes",
    '删除失败' => "failed to delete",
    '删除成功' => "successfully deleted",
    '用户名已被使用' => "username already taken",
    '昵称已被使用' => "Nickname already in use",
    '用户标签' => "user label",
    '注册类型' => "Registration Type",
    '第1行：用户名不能为空' => "Line 1: username cannot be empty",
    '第1行：用户名已被使用' => "Line 1: username already taken",
    '第1行：手机号已被使用' => "Line 1: The phone number is already in use",
    '第1行：邮箱已被使用' => "Line 1: mailbox already in use",
    '第1行：昵称已被使用' => "Line 1: Nickname already in use",
    '第1行：注册类型不存在' => "Line 1: Registration type does not exist",
    '男' => "male",
    '不存在' => "does not exist",
    '第1行：用户标签不存在' => "Line 1: User tag does not exist",
    '导入失败' => "import failed",
    '文件上传成功！导入成功10条，失败10条！失败详情：' => "File uploaded successfully! 10 were successfully imported and 10 were failed! Failure details:",
    '名称不能为空' => "Name is required",
    '名称已被使用' => "name already in use",
    '此类型已被使用，不允许删除' => "This type is already in use, deletion is not allowed",
    '存在异常的课程' => "Classes with exceptions",
    '已有报名的人,不能删除' => "Those who have already signed up cannot be deleted",
    '存在相同的培训标题' => "The same training title exists",
    '分类不存在' => "category does not exist",
    '开启成功' => "Open successfully",
    '禁用成功' => "disabled successfully",
    '请设置审核人员' => "Please set reviewers",
    '分类名称已存在' => "Category name already exists",
    '素材名称已存在' => "Material name already exists",
    '待修改' => "to be modified",
    '待审核' => "to be reviewed",
    '只有审核通过的素材才能操作' => "Only approved material can be operated",
    '培训ID' => "Training ID",
    '图片' => "picture",
    '报名总数' => "total number of registrations",
    '报名截止时间' => "Registration Deadline",
    '简述' => "brief description",
    '是否加水印' => "Whether to add watermark",
    '视频' => "video",
    '课程' => "course",
    '视频不能为空' => "Video cannot be empty",
    '没有可以导出的数据' => "no data to export",
    '培训标题' => "training title",
    '报名人名称' => "applicant name",
    '公司名称' => "Company Name",
    '号码' => "Number",
    '教育' => "educate",
    '城市' => "City",
    '详细地址' => "Address",
    '报名时间' => "registration time",
    '请先去登录' => 'please to sign in',
    '请先报名' => 'please apply first',
    '登录' => 'sign in',
    '企业登录' => 'enterprise login',
    '注册' => 'logon',
    '企业注册' => 'enterprise registration',
    '修改用户信息' => 'Modify user information',
    '重置密码' => 'reset password',
    '管理员管理' => 'administrator management',
    '新增管理员' => 'Add new administrator',
    '更新管理员信息' => 'Update administrator information',
    '删除管理员' => 'remove admin',
    '登录日志' => 'login log',
    '登录日志删除' => 'Login log deletion',
    '内网ip' => 'local ip',
    '操作日志' => 'operation log',
    '操作日志删除' => 'Operation log deletion',
    '广告' => 'advertise',
    '新增' => 'Add',
    '广告ID不能为空' => 'Advertising ID cannot be empty',
    '更新' => 'renew',
    '删除' => 'delete',
    '全部分类' => 'All Categories',
    '必填项不能为空！' => 'Required fields cannot be empty!',
    '失败' => 'failed',
    '附件' => 'appendix',
    '文件上传失败,请重新尝试' => 'File upload failed, please try again',
    '添加' => 'add',
    '自定义字段名称重复：' => 'Duplicate custom field name:',
    '栏目管理：' => 'Column management:',
    '请求方式错误' => 'wrong request method',
    '自动审核失败！版本：' => 'Automatic review failed! Version:',
    '添加内容完成！' => 'Adding content is complete!',
    '发布！' => 'release!',
    '审核失败！版本：' => 'Audit failure! Version:',
    '字段已经存在' => 'field already exists',
    '该表单不存在' => 'The form does not exist',
    '内链类型不能为空' => 'Internal link type cannot be empty',
    '内链类型错误' => 'Internal link type error',
    '联系方式必须填写一个' => 'Contact must fill',
    '询盘不存在' => 'Inquiry does not exist',
    '询盘内容' => 'Inquiry content',
    '站点不能为空' => 'site cannot be empty',
    '关键词已经存在' => 'keyword already exists',
    '数据读取失败或者文件加为空，请检查' => 'Data reading failed or the file is empty, please check',
    '数据格式错误，请参考样例' => 'The data format is wrong, please refer to the sample',
    '关键词不存在！' => 'Keyword does not exist!',
    '搜索引擎不存在' => 'search engine does not exist',
    '执行成功！' => 'execution succeed!',
    '关键词ID' => 'Keyword ID',
    '信息重复' => 'Duplicate information',
    '上传成功' => 'uploaded successfully',
    '访客数(UV)' => 'number of visitors(UV)',
    'IP数' => 'Number of IPs',
    '跳出率' => 'Bounce Rate',
    '平均访问时长' => 'average visit time',
    '转化次数' => 'conversions',
    '浏览量(PV)' => 'Views(PV)',
    '站点ID不能为空' => 'Views(PV)',
    '未知' => 'unknown',
    '验证码错误' => 'Verification code error！',
    '栏目' => 'Column',
    '请输入理由' => 'Please enter the reason!',
    '分类名称已被使用' => 'Category name has been used!',
    '分类下有帖子不能删除' => 'There are posts under the category and cannot be deleted!',
    '帖子不存在' => 'The post does not exist!',
    '标签名称已被使用' => 'tag name has been used!',
    '标签关联有文章不能删除' => 'There are posts under the tag and cannot be deleted!',
    '违禁词已存在' => 'There are posts under the tag and cannot be deleted!',
    '请求错误！' => 'wrong request!',
    '类型错误!' => 'type error',
    '手机号已被使用!' => 'The mobile number has be used!',
    '您的请求太频繁了，请稍等' => 'Your request is too frequent, please wait',
    '秒后再发' => 'send in seconds',
    '邮箱还未注册' => 'The mailbox is not registered!',
    '您的' => 'Your',
    '验证码为：' => 'verification code is:',
    '，5分钟内有效！' => ', effective within 5 minutes!',
    '拒绝理由不能为空' => 'refuse reason cannot be empty',
    '您的手机号已经被注册为企业不能再注册为个人账户' => 'Your phone number has been registered as an enterprise and cannot be registered as an individual!',
    '您的邮箱已经被注册为企业不能再注册为个人账户' => 'Your email has been registered as an enterprise and cannot be registered as an individual!',
    '账号已被禁用！请联系管理员！' => 'Account has been disabled! Please contact the administrator!',
    '验证码不能为空' => 'verification code must be filled',
    '您的邮箱已经被注册为企业，不能注册为个人' => 'Your email has been registered as an individual and cannot be registered as an enterprise!',
    '注册成功，请等待审核' => 'Registration succeeded, please wait for approval!',
    '请先去注册' => 'Please register first',
    '密码错误' => 'wrong password',
    '无权修改' => 'No right to modify',
    '企业名称已被使用' => 'company name be used！',
    'id不能为空' => 'id cannot be empty',
    '此类型已被使用，不允许删除！' => 'This type is already in use, deletion is not allowed!',
    '手机号不能为空' => 'phone number cannot be empty！',
    '手机号码未注册' => 'Mobile number is not registered',
    '栏目不存在' => 'column does not exist',
    '您已经报过名了' => 'you have signed up',
    '培训信息不存在' => 'Training information does not exist',
    '报名人数已满' => 'Registration is full',
    '报名已经截止' => 'Registration has closed',
    '内部不存在' => 'does not exist inside',

    '配置分组类型错误'=>'configuration grouping type error',
    '配置分组不能为空'=>'configuration group cannot be empty',
    '百度统计配置未完成'=>'baidu statistics configuration incomplete',
    '该站点正在检测中'=>'this site is currently being detected',
    '启动成功'=>'successfully started',
    '该账号已经存在'=>'the account already exists',
    '添加账号成功'=>'successfully added account',
    '编辑账号成功'=>'successfully edited account',
    '请填写海报名称'=>'please fill in the poster name',
    '海报名称已存在'=>'poster name already exists',
    '找不到要复制的海报'=>'unable to find the poster to copy',
    '-副本'=>'-copy',
    '复制成功'=>'replicating success',
    '安装成功'=>'installed successfully',
    '安装失败'=>'Installation failed',
    '不能为空'=>'cannot be empty',
    '职位ID不能为空'=>'position ID cannot be empty',
    '工作类别在使用中'=>'Job category in use',
    '保存成功'=>'successfully saved',
    '询盘'=>'inquiry',
    '请输入表单名称'=>'please enter a form name',
    '该表单已经存在'=>'the form already exists',
    '添加成功'=>'added successfully',
    '编辑成功'=>'successfully edited',
    '部署成功'=>'successfully deployed',
    '该表单尚未发布，无法卸载'=>'this form has not been published and cannot be uninstalled',
    '请先完成表单设计'=>'please complete the form design first',
    '该表单已经发布，请先卸载'=>'this form has been published, please uninstall first',
    '该表单已经发布，无需再次发布'=>'this form has been published and does not need to be published again',
    '缺少必要列：'=>'missing necessary columns：',
    '请先添加栏目'=>'please add a column first',
    '标题不能为空'=>'title cannot be empty',
    '图片格式错误'=>'image format error',
    '对应的分类不存在'=>'the corresponding classification does not exist',
    '导入成功'=>'import was successful',
    '网络附件链接地址请以http开头'=>'please start the network attachment link address with http',
    '菜单不存在'=>'menu does not exist',
    '数据类型配置错误'=>'data type configuration error',
    '商品名称'=>'trade name',
    '修改完成，提交审核'=>'modification completed, submit for review',
    '有栏目数据不属于该模型,请重新选择'=>'some column data does not belong to this model, please reselect',
    '询盘提醒邮件'=>'inquiry reminder email',
    '格式不正确'=>'incorrect format',
    '关联字段不存在,请稍后重试'=>'the associated field does not exist. Please try again later',
    '被关联字段不存在,请稍后重试'=>'the associated field does not exist. Please try again later',
    '字段名称已存在'=>'field name already exists',
    '验证失败'=>'validation failed',
    '生成失败'=>'generation failed',
    '错误信息:'=>'error message:',
    '验证码发送成功'=>'verification code sent successfully',
    '请完整填写阿里短信配置'=>'please complete the Alibaba SMS configuration',
    '百度PC配置错误,请检查'=>'Baidu PC configuration error, please check',
    '数据采集失败,请重新尝试'=>'Data collection failed, please try again',
    '域名格式错误'=>'Domain name format error',
    '管理'=>'manage',
    'ID为'=>'ID is',
    '网站信息不存在'=>'website information does not exist',
    '未开始'=>'not started',
    '已过期'=>'expired',
    '生效中'=>'in effect',
    '素材内容错误'=>'material content error',
    '无法获取IP'=>'unable to obtain IP',
    '无法解析出IP'=>'unable to resolve IP',
    '公网ip已经存在'=>'public IP already exists',
    '本系统主机'=>'host of this system',
    '证书编号已经存在'=>'certificate number already exists',
    '注册号已经存在'=>'registration number already exists',
    '验证码未到期,请稍后再发'=>'the verification code has not expired, please resend it later',
    '本地'=>'local',
    '阿里云'=>'ali cloud',
    '七牛云'=>'qiniuyun',
    '腾讯云'=>'tencent cloud',
    '网络附件'=>'network attachments',
    '未找到数据'=>'no data found',
    '城市名称重复'=>'duplicate city name',
    '百度PC'=>'baidu PC',
    '百度移动'=>'baidu mobile',
    '360PC'=>'360PC',
    '搜狗移动'=>'sogou mobile',
    '数据重复'=>'data duplication',
    '排名重复'=>'ranking duplicates',
    '插件目录不可读'=>'the plugin directory is not readable',
    '实例化插件失败'=>'failed to instantiate plugin',
    'url格式错误'=>'URL format error',
    '详情页'=>'detail pages',
    '信息已经存在'=>'information already exists',

    '域名已经存在'=>'the domain name already exists',
    '还有'=>'have',
    '天过期'=>'days expired',
    '域名已过期'=>'the domain name has expired',
    '天'=>'day',
    '没找到域名数据'=>'no domain name data found',
    '执行失败！whois查询失败'=>'execution failed! Whois query failed',
    '主机已过期'=>'host has expired',
    '当前站点没有激活的模板，请去安装模板'=>'the current site does not have an activated template. Please install the template',
    '当前站点已安装模板，但没有栏目使用单页模板，如当前站是子站，需要编辑父站点的数据，请回后台切换至父站点'=>'The current site has installed a template, but no columns use a single page template. If the current site is a child site and needs to edit the data of the parent site, please switch back to the background and switch to the parent site',
    '请填写询盘信息'=>'Please fill in the inquiry information',
    '网站收到询盘'=>'Website receives inquiry',
    '演示站点数据不能修改，请下载到本地操作'=>'demo site data cannot be modified, please download to local operation',

    '上传文件有误'=>'error uploading file',
    '上传文件类型有误'=>'the upload file type is incorrect',
    '上传文件内容为空'=>'the uploaded file content is empty',
    '不支持的编辑文件'=>'Unsupported edit this file',
];
