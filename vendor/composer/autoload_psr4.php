<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'think\\view\\driver\\' => array($vendorDir . '/topthink/think-view/src'),
    'think\\trace\\' => array($vendorDir . '/topthink/think-trace/src'),
    'think\\migration\\' => array($vendorDir . '/topthink/think-migration/src'),
    'think\\' => array($vendorDir . '/topthink/framework/src/think', $vendorDir . '/topthink/think-helper/src', $vendorDir . '/topthink/think-orm/src', $vendorDir . '/topthink/think-template/src', $vendorDir . '/topthink/think-filesystem/src'),
    'plugins\\' => array($baseDir . '/plugins'),
    'mailer\\' => array($vendorDir . '/yzh52521/think-mail/src/mailer'),
    'app\\' => array($baseDir . '/app'),
    'ZipStream\\' => array($vendorDir . '/maennchen/zipstream-php/src'),
    'Workerman\\MySQL\\' => array($vendorDir . '/workerman/mysql/src'),
    'Workerman\\' => array($vendorDir . '/workerman/workerman'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Qiniu\\' => array($vendorDir . '/qiniu/php-sdk/src/Qiniu'),
    'Qcloud\\Cos\\' => array($vendorDir . '/qcloud/cos-sdk-v5/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'PhpOffice\\PhpSpreadsheet\\' => array($vendorDir . '/phpoffice/phpspreadsheet/src/PhpSpreadsheet'),
    'Phinx\\' => array($vendorDir . '/topthink/think-migration/phinx/src/Phinx'),
    'PHPSocketIO\\' => array($vendorDir . '/workerman/phpsocket.io/src'),
    'PHPMailer\\PHPMailer\\' => array($vendorDir . '/phpmailer/phpmailer/src'),
    'Overtrue\\Pinyin\\' => array($vendorDir . '/overtrue/pinyin/src'),
    'OneSm\\' => array($vendorDir . '/lizhichao/one-sm/src'),
    'OSS\\' => array($vendorDir . '/aliyuncs/oss-sdk-php/src/OSS'),
    'MyCLabs\\Enum\\' => array($vendorDir . '/myclabs/php-enum/src'),
    'Matrix\\' => array($vendorDir . '/markbaker/matrix/classes/src'),
    'League\\MimeTypeDetection\\' => array($vendorDir . '/league/mime-type-detection/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'Kkokk\\Poster\\' => array($vendorDir . '/hedali/poster/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\Command\\Guzzle\\' => array($vendorDir . '/guzzlehttp/guzzle-services/src'),
    'GuzzleHttp\\Command\\' => array($vendorDir . '/guzzlehttp/command/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'FormBuilder\\' => array($vendorDir . '/xaboy/form-builder/src'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/src'),
    'EasySwoole\\Utility\\' => array($vendorDir . '/easyswoole/utility/src'),
    'EasySwoole\\Jwt\\' => array($vendorDir . '/easyswoole/jwt/src'),
    'Doctrine\\Deprecations\\' => array($vendorDir . '/doctrine/deprecations/lib/Doctrine/Deprecations'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/src'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Darabonba\\OpenApi\\' => array($vendorDir . '/alibabacloud/darabonba-openapi/src'),
    'Darabonba\\GatewaySpi\\' => array($vendorDir . '/alibabacloud/gateway-spi/src'),
    'Complex\\' => array($vendorDir . '/markbaker/complex/classes/src'),
    'Channel\\' => array($vendorDir . '/workerman/channel/src'),
    'AlibabaCloud\\Tea\\XML\\' => array($vendorDir . '/alibabacloud/tea-xml/src'),
    'AlibabaCloud\\Tea\\Utils\\' => array($vendorDir . '/alibabacloud/tea-utils/src'),
    'AlibabaCloud\\Tea\\' => array($vendorDir . '/alibabacloud/tea/src'),
    'AlibabaCloud\\SDK\\Dysmsapi\\V20170525\\' => array($vendorDir . '/alibabacloud/dysmsapi-20170525/src'),
    'AlibabaCloud\\OpenApiUtil\\' => array($vendorDir . '/alibabacloud/openapi-util/src'),
    'AlibabaCloud\\Endpoint\\' => array($vendorDir . '/alibabacloud/endpoint-util/src'),
    'AlibabaCloud\\Credentials\\' => array($vendorDir . '/alibabacloud/credentials/src'),
    'Adbar\\' => array($vendorDir . '/adbario/php-dot-notation/src'),
);
