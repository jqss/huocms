<?php


namespace app\validate;

use think\Validate;

class InquiryEmailValidate extends Validate
{
    protected $rule = [
        'id|邮箱ID' => 'require|number',
        'website_id|网站'=>'require|array',
        'email|邮箱账号' => 'require|email|unique:inquiry_email',
    ];

    protected $scene = [
        'read' => ['id'],
        'save' => ['website_id', 'email'],
        'update' => ['id', 'website_id', 'email']
    ];
}
