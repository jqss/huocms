<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Job extends Model
{

    public function jobCity()
    {
        return $this->belongsTo(JobCity::class);
    }

    public function jobCate()
    {
        return $this->belongsTo(JobCate::class);
    }

    public function getJobCityIdAttr($value)
    {
        if(empty($value) || $value == 0){
            $value = null;
        }
        return $value;
    }

    public function getJobCateIdAttr($value)
    {
        if(empty($value) || $value == 0){
            $value = null;
        }
        return $value;
    }

    /**
     * @throws ModelException
     */
    public function getJobTagList($where, $param): array
    {
        try{
            $res =  $this->where($where)->with(['jobCate', 'jobCity'])->paginate([
                'page' => $param['page'],
                'list_rows' => $param['limit'],
            ]);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getJobList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->with(['jobCate', 'jobCity'])->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getJob(array $where = [],$with=[]): array
    {
        try{
           $res =  $this->with($with)->where($where)->find();
           if(empty($res)){
                return dataReturn(-1, lang('未找到数据'));
           }
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * 职位搜索
     * @param string $value
     * @return array
     */
    public function searchJob(array $where = [], string $value = '', int $limit = 10): array
    {
        try {
            $res = $this->where('title', 'like', '%'.$value.'%')->with(['jobCate', 'jobCity'])->where($where)->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addJob($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res->id);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateJob(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelJob($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delJob($where): array
    {
        try{
            $res =  $this->where($where)->delete();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }
}
