<?php

namespace plugins\user;

use app\model\AdminMenu;
use app\Plugin;

class UserPlugin extends Plugin
{
    public $info = [
        'name'        => 'user',//插件英文名
        'title'       => '会员模块',
        'description' => '用户会员模块管理，支持小程序/公众号登录',
        'status'      => 1,
        'author'      => 'huocms',
        'version'     => '1.0.0',
        'demo_url'    => 'http://www.demo_huocms.com',
        'author_url'  => 'http://www.huocms.com'
    ];

    public $hasAdmin = 1;//插件是否有后台管理界面

    // 插件安装
    public function install()
    {
        if (!$this->installSql()) {
            return false;
        }

        //更新菜单
        $this->editMenu(1, 'plugin_user');
        return true;//安装成功返回true，失败false
    }

    // 插件卸载
    public function uninstall()
    {
        //更新菜单
        $this->editMenu(2, 'plugin_user');
        return true;//卸载成功返回true，失败false
    }

}
