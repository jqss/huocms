<?php

return [
    'dataTypes' => [
        '单行文本','多行文本','富文本编辑器','整数','数字','日期','时间',
        '日期时间','日期时间区间','列表','列表多选','图片','多图','视频','多视频','文件','多文件',
        '开关','单选','多选','金额', '关联字段'
    ],

    'dbTypeMaps' => [
        '单行文本' => 'string',
        '多行文本' => 'string',
        '富文本编辑器' => 'text',
        '整数' => 'integer',
        '数字' => 'float',
        '金额' => 'decimal',
        '日期' => 'string',
        '时间' => 'string',
        '日期时间' => 'string',
        '日期时间区间' => 'string',
        '列表' => 'string',
        '列表多选' => 'string',
        '图片' => 'integer',
        '多图' => 'string',
        '视频' => 'integer',
        '多视频' => 'string',
        '文件' => 'integer',
        '多文件' => 'string',
        '开关' => 'string',
        '单选' => 'string',
        '多选' => 'string',
        '关联字段' => 'reference',
    ],

    'formTypeMaps' => [
        '单行文本' => 'input',
        '多行文本' => 'textarea',
        '富文本编辑器' => 'richText',
        '整数' => 'input',
        '数字' => 'input',
        '金额' => 'input',
        '日期' => 'date',
        '时间' => 'time',
        '日期时间' => 'datetime',
        '日期时间区间' => 'dateTimeRange',
        '列表' => 'select',
        '列表多选' => 'multiSelect',
        '图片' => 'image',
        '多图' => 'multiImage',
        '视频' => 'video',
        '多视频' => 'multiVideo',
        '文件' => 'file',
        '多文件' => 'multiFile',
        '开关' => 'switch',
        '单选' => 'radio',
        '多选' => 'checkbox',
        '关联字段' => 'reference',
        '城市地区' => 'city',
    ],

    'validateRules' => [
        '必填' => 'require',
        '整数' => 'number',
        '字母' => 'alpha',
        '日期' => 'date',
        '时间' => 'time',
        '日期时间' => 'datetime',
        '邮箱' => 'email',
        '数组' => 'array',
        '值必须在list中' => 'list'

    ],

    'defaultFields' => [
        'thumbnail' => ['comment'=>'缩略图','default'=>'','length'=>120,'type'=>'string'],
        'is_top' => ['comment'=>'置顶','default'=>0,'type'=>'integer'],
        'recommended' => ['comment'=>'推荐','default'=>0,'type'=>'integer'],
        'publish_time' => ['comment'=>'发布时间','default'=>0,'','type'=>'integer'],
        'delete_time' => ['comment'=>'删除','default'=>0,'type'=>'integer'],
        'create_time' => ['comment'=>'创建时间','default'=>0,'type'=>'integer'],
        'update_time' => ['comment'=>'更新时间','default'=>0,'type'=>'integer'],
    ],
];