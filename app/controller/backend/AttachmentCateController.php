<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\AttachmentCate;
use app\service\CacheService;
use app\validate\AttachmentCateValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class AttachmentCateController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(AttachmentCate $attachmentCate): \think\response\Json
    {
        $where = [
           ['seller_id' ,'=', $this->admin['seller_id']],
        ];
        $title = input('title');
        if(!empty($title)){
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        $attachmentCateList = $attachmentCate->getAllCustomArrayData($where)['data'];
        $attachmentCateList = makeTree($attachmentCateList);
        array_unshift($attachmentCateList,[
            'id' => 0,
            'parent_id' => 0,
            'path' => 0,
            'title'=> Lang::get('全部分类'),
            'children' => [],
        ]);
        return jsonReturn(0,Lang::get('成功'),$attachmentCateList);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelNotUniqueException
     * @throws \ReflectionException
     */
    public function save(AttachmentCate $attachmentCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(AttachmentCateValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $param['seller_id'] = $this->admin['seller_id'];
            $attachmentCate->saveUnique(['title'=>$param['title'],'seller_id'=>$param['seller_id'],'parent_id'=>$param['parent_id']],'栏目已经存在');
            // 完善栏目数据
            if ($param['parent_id']) {
                $parentCate = $attachmentCate->getCustomData(['id' => $param['parent_id'], 'seller_id' => $param['seller_id']])['data'];
                $param['path'] = $parentCate['path'] . '-' . $param['parent_id'];
            } else {
                $param['path'] = 0;
            }
            $res = $attachmentCate -> addAttachmentCate($param);
            CacheService::deleteRelationCacheByObject($attachmentCate);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(AttachmentCate $attachmentCate): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,Lang::get('分类ID不能为空'));
        }
        $where = [
            'id' => $id,
        ];
        // 其他逻辑
        $res = $attachmentCate->getAttachmentCate($where);
        return json($res);

    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function update(AttachmentCate $attachmentCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(AttachmentCateValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $attachmentCate -> updateAttachmentCate($where,$param);
            CacheService::deleteRelationCacheByObject($attachmentCate);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function delete(AttachmentCate $attachmentCate): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,lang('分类ID不能为空'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $subCate = $attachmentCate -> getAllCustomArrayData(['seller_id'=>$this->admin['seller_id'],'parent_id'=>$id])['data'];
            if(!empty($subCate)){
                return jsonReturn(-1,Lang::get('分类下面有子分类，不能直接删除'));
            }
            $cate = $attachmentCate -> getCustomData($where,'attachment')['data']->toArray();
            if(!empty($cate['attachment'])){
                return jsonReturn(-2,Lang::get('分类下有附件不能直接删除'));
            }
            $res = $attachmentCate->delAttachmentCate($where);
            CacheService::deleteRelationCacheByObject($attachmentCate);
            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
