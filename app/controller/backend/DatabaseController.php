<?php

namespace app\controller\backend;

use app\model\Database;
use app\service\MysqlBackupService;
use think\App;
use think\facade\Db;
use think\facade\Env;

class DatabaseController extends BaseController
{

    protected $service;

    /**
     * @throws \Exception
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $config = array(
            'level' => 5,//数据库备份卷大小
            'compress' => 0,//数据库备份文件是否启用压缩 0不压缩 1 压缩
        );
        $this->service = new MysqlBackupService($config);
    }

    /**
     * 数据表列表
     * @return \think\response\Json
     */
    public function tableList(): \think\response\Json
    {
        return json($this->service->dataList());
    }

    /**
     * 备份文件列表
     * @return mixed
     * @throws \app\exception\ModelException
     */
    public function index()
    {
        $path = app()->getRootPath() .'backup/';
        $glob = hcScanDir($path.'*');
        if(empty($glob)){
            return jsonReturn(0,'success');
        }
        arsort($glob);
        $data = [];
        foreach ($glob as $key => $val){
            $tmp = [
                'id'    => $key+1,
                'title' => $val,
                'size'  => sprintf("%.2f",filesize($path.$val) / 1000) .'kb' ,
            ];
            array_push($data,$tmp);
        }
        return jsonReturn(0,lang('成功'),$data);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function detail($name)
    {
        return jsonReturn($this->service->detail($name));
    }

    /**
     * 数据库备份
     * @throws \app\exception\ModelException
     * @throws \think\db\exception\BindParamException
     */
    public function backup(): \think\response\Json
    {
        return $this->service->backupDatabase($this->admin['seller_id']);
    }

    /**
     * 文件恢复
     * @return \think\response\Json
     */
    public function restore(): \think\response\Json
    {
        if(!request()->isPost()){
            return jsonReturn(-1,lang('方法请求错误'));
        }
        $filename = input('post.filename');
        $res = $this->service->import($filename,0);
        return json($res);
    }


    /**
     * 数据表优化
     * @param $name
     * @throws \Exception
     */
    public function optimize($name): \think\response\Json
    {
        $this->service->optimize($name);
        return jsonReturn(0,lang('优化成功'));
    }

    /**
     * 数据表修复
     * @param $name
     * @throws \Exception
     */
    public function repair($name): \think\response\Json
    {
        $this->service->repair($name);
        return jsonReturn(0,lang('修复成功'));
    }

    /**
     * @return \think\response\File
     */
    public function downloadFile(): \think\response\File
    {
        try {
            $time = intval($this->request->param('filename'));
            $file =$this->service->getFile('time', $time);
            $fileName = $file[0];
            return  download($fileName,$time);
        }catch (UploadFailException $e){
            return app('json')->fail(lang('下载失败'));
        }
    }

    /**
     * @throws \Exception
     */
    public function delete(): \think\response\Json
    {
        $filename = $this->request->param('filename');
        if(empty($filename)){
            return jsonReturn(-2,lang('文件名不能为空'));
        }
        return $this->service->delFile($filename);
    }

}
