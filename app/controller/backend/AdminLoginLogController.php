<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\AdminLoginLog;
use think\facade\Lang;
use think\facade\Request;

class AdminLoginLogController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     * @throws \app\exception\ModelException
     */
    public function index(AdminLoginLog $adminLoginLog): \think\Response
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
        ];
        $limit = $this->setLimit();
        $name = input('admin_name');
        if($name){
            $where['admin_name'] = $name;
        }
        $id = input('admin_id');
        if($id){
            $where['id'] = $id;
        }
        $adminLogList = $adminLoginLog->getAdminLoginLogList($where,$limit);
        return json(pageReturn($adminLogList));
    }

    public function delete(): \think\response\Json
    {
        return parent::customDelete(Lang::get('登录日志'),Lang::get('登录日志删除'));
    }
}
