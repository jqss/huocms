<?php
// 事件定义文件
return [
    'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'AdminLoginLog' => [\app\listener\AdminLoginListener::class],
        'AdminOptLog'   => [\app\listener\AdminOperationListener::class],
    ],

    'subscribe' => [
    ],
];
