<?php

namespace app\service;

use app\model\PhoneCode;
use think\facade\Cache;

class PhoneCodeService
{

    /**
     * 校验短信验证码
     * @return array
     */
    public static function authCode($data): array
    {
        $exist = Cache::get($data['phone'] . '_login');
        if (!$exist) {
            return dataReturn(-1, lang('验证失败'));
        }
        $codeModel = new PhoneCode();
        $codeData = $codeModel->where([
            ['phone', '=', $data['phone']],
            ['code', '=', $data['code']],
            ['used', '=', 2],
        ])->findOrEmpty()->toArray();
        if (empty($codeData['id'])) {
            return dataReturn(-2, lang('验证码不存在'));
        }
        $expiredTime = $codeData['expired_time'];

        $codeModel->where('id', $codeData['id'])->update(['used' => 1]);

        if ($expiredTime <= date('Y-m-d H:i:s')) {
            return dataReturn(-3, lang('验证码已过期'));
        } else {
            return dataReturn(0, lang('验证成功'));
        }
    }
}