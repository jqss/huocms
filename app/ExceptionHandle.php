<?php
namespace app;

use app\exception\AuthException;
use app\exception\BadSysSettingException;
use app\exception\BaseException;
use app\exception\ModelException;
use app\exception\ModelNotUniqueException;
use app\exception\TokenException;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\facade\Log;
use think\Response;
use Throwable;

/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        // 使用内置的方式记录异常日志
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // 详细错误日志写入文件
        Log::error($e->getMessage());
        $str = $this->formatterTraceStr($e);
        Log::error($str);
        if(request()->isAjax() || request()->isJson() || env('APP_DEBUG') == 1){
            // 添加自定义异常处理机制
            return jsonReturn($e->getCode() ?: -1, $e->getMessage());
        }

        // 其他错误交给系统处理
        return parent::render($request, $e);
    }

    public function formatterTraceStr(Throwable $e): string
    {
        $str = "=========手动记录日志=============\n" ;
        $str .= '请求的API是【' . request()->url() . "】\n";
        $str .= '请求携带的参数是【' . json_encode(request() -> param()) . "】\n";
        $str .= $e->getTraceAsString();
        $str .= "\n【".date('Y-m-d H:i:s') . "】=========日志记录结束=============";
        return $str;
    }
}
