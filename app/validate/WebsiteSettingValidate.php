<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class WebsiteSettingValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'setting|配置' => 'array',
        'website_id|站点ID'   => 'require|number',
        'lang|站点语言' => 'require'
    ];

    protected $scene = [
        'read' => ['website_id','lang'],
        'update' => ['setting','website_id','lang']
    ];

}
