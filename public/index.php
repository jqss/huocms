<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

use sys\HcApp;

// 调试模式开关
define('APP_DEBUG', true);

// 定义CMS根目录,可更改此目录
define('CMS_ROOT', dirname(__DIR__) . '/');

// 定义应用目录
define('APP_PATH', CMS_ROOT . 'app/');

// 定义插件目录
define('PLUGINS_PATH', CMS_ROOT . 'plugins/');

// 定义网站入口目录
define('WEB_ROOT', __DIR__ . '/');

require __DIR__ . '/../vendor/autoload.php';
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
    header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE,OPTIONS,PATCH');
    exit;
}
// 执行HTTP应用并响应
$http = (new HcApp())->http;

$response = $http->run();

$response->send();

$http->end($response);
