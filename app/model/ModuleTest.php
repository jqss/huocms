<?php
declare (strict_types = 1);

namespace app\model;

class ModuleTest extends Model
{
       public function subContent(): \think\model\relation\BelongsTo
       {
            return $this->belongsTo(SubContent::class,'sub_id','id');
       }

}