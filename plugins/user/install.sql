DROP TABLE IF EXISTS `$prefix$user_group`;
CREATE TABLE `$prefix$user_group`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`   int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title`       varchar(120) NOT NULL DEFAULT '' COMMENT '分类名称',
    `desc`        varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
    `create_time` datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户分类表';

DROP TABLE IF EXISTS `$prefix$user_grade`;
CREATE TABLE `$prefix$user_grade`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id`   int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `title`       varchar(120) NOT NULL DEFAULT '' COMMENT '等级名称',
    `icon`        int(11) NOT NULL DEFAULT 0 COMMENT '等级图标',
    `min_score`   int(11) NOT NULL DEFAULT '0' COMMENT '最低积分值',
    `max_score`   int(11) NOT NULL DEFAULT '0' COMMENT '最大积分值',
    `sort`        int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `create_time` datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time` datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户等级表';


DROP TABLE IF EXISTS `$prefix$user`;
CREATE TABLE `$prefix$user`
(
    `id`                  int(11) unsigned NOT NULL AUTO_INCREMENT,
    `seller_id`           int(11) unsigned DEFAULT '0' COMMENT '商户ID',
    `user_group`          int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属用户分类',
    `user_grade`          int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员等级',
    `sex`                 tinyint(2) NOT NULL DEFAULT '0' COMMENT '性别;0:保密,1:男,2:女',
    `birthday`            varchar(20)                                                 NOT NULL DEFAULT '' COMMENT '生日',
    `score`               int(11) NOT NULL DEFAULT '0' COMMENT '用户积分',
    `coin`                int(10) unsigned NOT NULL DEFAULT '0' COMMENT '金币',
    `balance`             decimal(10, 2)                                               NOT NULL DEFAULT '0.00' COMMENT '余额',
    `status`              tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '用户状态;1:正常,2:未验证,3:禁用',
    `username`            varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
    `password`            varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '登录密码;加密',
    `nickname`            varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
    `email`               varchar(100)                                                 NOT NULL DEFAULT '' COMMENT '用户登录邮箱',
    `user_url`            varchar(100)                                                 NOT NULL DEFAULT '' COMMENT '用户个人网址',
    `avatar`              varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '用户头像',
    `signature`           varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '个性签名',
    `last_login_ip`       varchar(15)                                                  NOT NULL DEFAULT '' COMMENT '最后登录ip',
    `last_login_time`     datetime                                                              DEFAULT NULL COMMENT '最后登录时间',
    `user_activation_key` varchar(60)                                                  NOT NULL DEFAULT '' COMMENT '激活码',
    `mobile`              varchar(20)                                                  NOT NULL DEFAULT '' COMMENT '中国手机不带国家代码，国际手机号格式为：国家代码-手机号',
    `more`                text COMMENT '扩展属性',
    `address`             varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '省市区',
    `job`                 varchar(255)                                                 NOT NULL DEFAULT '' COMMENT '职业',
    `create_time`         datetime                                                              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`         datetime                                                              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`),
    KEY                   `username` (`username`),
    KEY                   `nickname` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';


DROP TABLE IF EXISTS `$prefix$user_login`;
CREATE TABLE `$prefix$user_login`
(
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '登录日志ID',
    `seller_id`     int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
    `website_id`    int(11) DEFAULT NULL COMMENT '站点id',
    `user_id`       int(11) NOT NULL DEFAULT '0' COMMENT '站点用户ID',
    `username`      varchar(120) NOT NULL DEFAULT '' COMMENT '用户名称',
    `ip`            varchar(120) NOT NULL DEFAULT '' COMMENT '登录IP',
    `login_address` varchar(255) NOT NULL DEFAULT '' COMMENT '登录地址',
    `desc`          varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
    `agent`         varchar(255) NOT NULL DEFAULT '' COMMENT '登录客户端',
    `login_time`    int(11) NOT NULL DEFAULT '0' COMMENT '登录时间',
    `logout_time`   int(11) NOT NULL DEFAULT '0' COMMENT '登出时间',
    `create_time`   datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
    `update_time`   datetime              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户登录日志';

DROP TABLE IF EXISTS `$prefix$user_collection`;
CREATE TABLE `$prefix$user_collection`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(11) unsigned DEFAULT '0' COMMENT '用户id',
    `article_id`  int(11) unsigned DEFAULT '0' COMMENT '文章的id',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户的收藏列表';

