<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\exception\ModelEmptyException;
use app\exception\ModelException;
use app\model\Job;
use app\model\JobCate;
use app\model\JobCity;
use app\model\Website;
use app\validate\JobValidate;
use think\exception\ValidateException;
use think\facade\Lang;
use think\response\Json;

class JobController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return Json
     * @throws ModelException
     */
    public function index(Job $job): Json
    {
        $seller_id = $this->admin['seller_id'];
        $job_city_id = input('job_city_id');
        $job_cate_id = input('job_cate_id');
        $title = input('title');

        $where = [
            'seller_id' => $seller_id,
            'is_del' => 1,
        ];
        $limit = 10;
        $limitParam = (int)input('limit');
        if($limitParam){
            $limit = $limitParam;
        }
        // TODO
        // 添加其他逻辑
        if (!empty($job_city_id)) {
            $where['job_city_id'] = $job_city_id;
        }

        if (!empty($job_cate_id)) {
            $where['job_cate_id'] = $job_cate_id;
        }

        if (!empty($title)) {
            // 模糊查询
            $jobList = $job->searchJob($where,$title, $limit);
        } else {
            $jobList = $job->getJobList($where,$limit);
        }

        return json(pageReturn($jobList));
    }

    /**
     * 保存新建的资源
     *
     * @return Json
     * @throws ModelException
     */
    public function save(Job $job): Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            // 数据验证
            try{
                validate(JobValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }

            $res = $job -> addJob($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return Json
     * @throws ModelException
     * @throws ModelEmptyException
     */
    public function read(Job $job): Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,lang('不能为空'));
        }
        $siteId = (int)input('website_id');
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id'],
            'website_id' => $siteId,
        ];
        $jobRes = $job->getJob($where,['jobCity','jobCity'])['data']->toArray();


        return jsonReturn(0, lang('查询成功'), $jobRes);

    }

    /**
     * 保存更新的资源

     * @return Json
     * @throws ModelException
     */
    public function update(Job $job): Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];
            try {
                validate(JobValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'is_del' => 1,
                'website_id' => $param['website_id']
            ];
            $res = $job -> updateJob($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws ModelException
     */
    public function delete(Job $job): Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,lang('职位ID不能为空'));
            }
            $siteId = (int)input('website_id');
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id'],
                'website_id' => $siteId,
            ];
            $res = $job->delJob($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
