<?php

namespace app\command\service\step;

use think\facade\App;

class CheckTplCode
{
    public static function run($taskData)
    {
        $analysis = [
            'hn' => [
                'status' => 1,
                'msg' => []
            ],
            'tags' => [
                'status' => 1,
                'msg' => []
            ],
            'robots' => [
                'status' => 1,
                'msg' => 'robots.txt文件正确'
            ],
            'sitemap' => [
                'status' => 1,
                'msg' => 'sitemap文件放置正确'
            ],
            'urlLevel' => [
                'status' => 1,
                'msg' => []
            ],
            'c404' => [
                'status' => 1,
                'msg' => '404文件放置正确'
            ]
        ];

        // 目录层级
        foreach ($taskData['cate'] as $vo) {
            if ($vo['alias'] != '/' && !empty($vo['alias'])) {
                $level = count(array_filter(explode('/', $vo['alias'])));
                if ($level >= 3) {
                    $analysis['urlLevel']['status'] = 2;
                    $analysis['urlLevel']['msg'][] = $vo['title'] . '页面，url层级太深，达到了' . $level . '层';
                }
            }
        }

        // 404文件
        $rootPath = App::getRootPath() . 'public';
        if (!is_file($rootPath . '/system_file/hc_error/404.html')) {
            $analysis['c404']['status'] = 2;
            $analysis['c404']['msg'] = '未正确放置404文件';
        }

        $path = $rootPath . '/themes/website/' . $taskData['websiteId'] . '/' . $taskData['lang'] . '/' . $taskData['theme'];
        if (!is_dir($path)) {
            return $analysis;
        }

        $files = scandir($path);

        foreach ($files as $value){
            if($value != '.' && $value != '..' && is_file($path . '/' . $value)){
                $html = file_get_contents($path . '/' . $value);

                // 存在ajax
                if (strstr($html, '$.ajax') !== false || strstr($html, '$.getJSON') !== false
                    || strstr($html, '$.post') !== false) {
                    $analysis['tags']['status'] = 2;
                    $analysis['tags']['msg']['ajax'][] = $value . '页面，有ajax异步请求，建议更换。';
                }

                $html = \phpQuery::newDocumentHTML($html);

                // 页面为vue.js编写
                $html['script']->html('');
                $html['noscript']->html('');
                $vueJs = str_replace(PHP_EOL, '', trim(strip_tags($html)));
                if (empty($vueJs)) {
                    $analysis['tags']['status'] = 2;
                    $analysis['tags']['msg']['vuejs'][] = $value . '页面，是Vue.js编写无法收录';
                }

                // 检测h1数量
                $h1 = $html["h1"];
                if (count($h1) >= 2) {
                    $analysis['hn']['status'] = 2;
                    $analysis['hn']['msg']['h1'][] = $value . '有' . count($h1) . '个<h1>标签，数量过多。';
                }

                // 检测iframe框架
                $iframe = $html["iframe"];
                if (count($iframe) > 0) {
                    $analysis['tags']['status'] = 2;
                    $analysis['tags']['msg']['iframe'][] = $value . '页面，存在iframe标签，建议更换。';
                }

                // 检测flash技术
                $flash = $html["object"];
                if (count($flash) > 0) {
                    $analysis['tags']['status'] = 2;
                    $analysis['tags']['msg']['flash'][] = $value . '页面，存在flash标签，建议更换。';
                }

                // alt标签
                $imgs = $html["img"];
                foreach($imgs as $imgObj) {
                    if (empty(pq($imgObj)->attr('alt'))) {
                        $analysis['tags']['status'] = 2;
                        $analysis['tags']['msg']['alt'][] = $value . '页面，img标签alt属性缺少或为空';
                        break;
                    }
                }

                // robots 文件
                if (!is_file($rootPath . '/robots.txt')) {
                   $analysis['robots'] = [
                        'status' => 2,
                        'msg' => '未在正确放置robots.txt文件'
                   ];
                }

                // sitemap
                if (!is_file($rootPath . '/xml/' . $taskData['websiteId'] . '_sitemap.xml')) {
                    $analysis['sitemap'] = [
                        'status' => 2,
                        'msg' => '未正确放置sitemap.xml文件'
                    ];
                }
            }
        }

        return $analysis;
    }
}