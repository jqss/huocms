<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class SlideValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|幻灯片ID' => 'require|number',
        'title|幻灯片名称' => 'require',
        'url|幻灯片地址' => 'url',
        'attachment|幻灯片图片' => 'require|number',
        'slide_cate_id|分类ID' => 'require|number',
    ];

    protected $scene = [
        'save' => ['title','url','attachment'],
        'update' => ['id','title','url','attachment']
    ];

}
