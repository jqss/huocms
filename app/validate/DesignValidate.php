<?php

namespace app\validate;

use think\Validate;

class DesignValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'website_id|站点ID'   => 'require|number',
        'lang|站点语言' => 'require'
    ];

    protected $scene = [
        'index' => ['website_id','lang'],
    ];
}
