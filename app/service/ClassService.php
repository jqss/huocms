<?php

namespace app\service;

class ClassService
{
    /**
     * 获取模型名称
     * @throws \ReflectionException
     */
    public static function getClassName($object): string
    {
        $reflection = new \ReflectionClass($object);
        return $reflection->getShortName();
    }
}
