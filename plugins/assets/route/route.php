<?php
use think\facade\Route;

Route::group(function () {

    // 版权接口
    Route::get('/copyright/list', 'plugin.assets.copyright/index');
    Route::post('/copyright/add', 'plugin.assets.copyright/add');
    Route::get('/copyright/getInfo', 'plugin.assets.copyright/getInfo');
    Route::post('/copyright/edit', 'plugin.assets.copyright/edit');
    Route::post('/copyright/del', 'plugin.assets.copyright/del');

    // 域名管理接口
    Route::get('/domain/list', 'plugin.assets.domain/index');
    Route::post('/domain/add', 'plugin.assets.domain/add');
    Route::post('/domain/edit', 'plugin.assets.domain/edit');
    Route::post('/domain/del', 'plugin.assets.domain/del');
    Route::post('/domain/whois', 'plugin.assets.domain/whois');

    // 主机管理接口
    Route::get('/server/list', 'plugin.assets.server/index');
    Route::post('/server/add', 'plugin.assets.server/add');
    Route::post('/server/edit', 'plugin.assets.server/edit');
    Route::post('/server/del', 'plugin.assets.server/del');

    // 商标管理接口
    Route::get('/trademark/list', 'plugin.assets.trademark/index');
    Route::post('/trademark/add', 'plugin.assets.trademark/add');
    Route::post('/trademark/edit', 'plugin.assets.trademark/edit');
    Route::post('/trademark/del', 'plugin.assets.trademark/del');
    Route::get('/trademark/read', 'plugin.assets.trademark/read');

    // 软著管理接口
    Route::get('/soft/list', 'plugin.assets.soft/index');
    Route::get('/soft/getInfo', 'plugin.assets.soft/getInfo');
    Route::post('/soft/add', 'plugin.assets.soft/add');
    Route::post('/soft/edit', 'plugin.assets.soft/edit');
    Route::post('/soft/del', 'plugin.assets.soft/del');
})->middleware('login');

