<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ThemeValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'theme_id|模板ID' => 'require|number',
        'theme|模版名称'    => 'require',
        'website_id|网站ID'  => 'require|number',
        'lang|模版语言'     => 'require',
        'type|类型' => 'require|in:0,1,2,3'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $scene = [
        'templateFile' => ['website_id','lang','type'],
        'tmpFile' => ['theme_id','website_id','lang'],
        'active'  => ['theme_id','website_id','lang'],
        'update'  => ['theme_id','website_id'],
        'installTheme' => ['theme','website_id','lang'],
        'uninstall'  => ['theme_id','website_id'],
        'allFiles' => ['website_id','path','lang'],
        'allFilePath' => ['website_id','lang'],
    ];
}
