<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\exception\ModelException;
use app\model\InnerChart;
use app\validate\InnerChartValidate;
use think\exception\ValidateException;
use think\facade\Lang;

class InnerChartController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(InnerChart $innerChart): \think\response\Json
    {

        $limit = $this->setLimit();
        $type = input('type');
        $siteId = (int)input('website_id');
        if(empty($type)){
            $type = 1;
        }
        $where = [
            ['seller_id' ,'=', $this->admin['seller_id']],
            ['type' ,'=', $type],
        ];

        if ($type != 2) {
            $where[] = ['website_id','=',$siteId];
        }

        $keyword = input('keyword');
        if($keyword){
            $where[] = ['keyword','like','%'.$keyword.'%'];
        }
        try{
            $innerList = $innerChart->where($where)->order('id','desc')->paginate($limit)->each(function(&$item){
                $count = (int)$item->content()->sum('total');
                $item -> count = $count;
                $item -> save();
            });
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }

        return json(['code'=>0,'total'=>$innerList->total(),'msg'=>'success','data'=>$innerList->all()]);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(InnerChart $innerChart): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];
            if(empty($param['website_id'])){
                $param['website_id'] = 0;
            }
            // 数据验证
            try{
                validate(InnerChartValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }

            $res = $innerChart -> addInnerChart($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(InnerChart $innerChart): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,lang('内链ID不能为空'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        $res = $innerChart->getInnerChart($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(InnerChart $innerChart): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(InnerChartValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            if(empty($param['website_id'])){
                $param['website_id'] = 0;
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $innerChart -> updateInnerChart($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(InnerChart $innerChart): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,lang('内链ID不能为空'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $innerChart->delInnerChart($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
