<?php
declare (strict_types=1);

namespace plugins\user\validate;

use think\Validate;

class UserValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|用户ID' => 'require|number',
        'username|用户名' => 'require|length:2,80',
        'password|密码' => 'require|length:4,20',
        'user_group|用户组' => 'require',
        'email|邮箱' => 'email',
        'mobile|手机号' => 'mobile',
        'sex|性别' => 'in:0,1,2',
        'status|用户状态' => 'in:1,2,3',
        'birthday|生日' => 'dateFormat:Y-m-d',
        'nickname|昵称' => 'require|length:2,80',
        'address|地址'=>"max:100",
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['username', 'user_group'],
        'update' => ['id', 'username', 'user_group'],
        'editStatus' => ['id', 'status'],
        'editInfo'=>['nickname','job','birthday','address'],
    ];
}
