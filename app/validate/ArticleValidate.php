<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ArticleValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|文章ID' => 'require|number',
        'category_id|栏目ID' => 'require|number',
        'title|文章标题' => 'require',
    ];

    protected $scene = [
        'save' => ['title','category_id'],
        'update' => ['id','title','category_id'],
    ];
}
