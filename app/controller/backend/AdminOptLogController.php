<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\AdminOptLog;
use think\facade\Lang;


class AdminOptLogController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(AdminOptLog $adminOptLog): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
        ];
        $limit = $this->setLimit();
        $name = input('admin_name');
        if($name){
            $where['admin_name'] = $name;
        }
        $id = input('admin_id');
        if($id){
            $where['id'] = $id;
        }
        $adminLogList = $adminOptLog->getAdminOptLogList($where,$limit);
        return json(pageReturn($adminLogList));
    }

    public function delete(): \think\response\Json
    {
        return parent::customDelete(Lang::get('操作日志'),Lang::get('操作日志删除'));
    }

}
