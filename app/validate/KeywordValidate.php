<?php


namespace app\validate;


use \think\Validate;
class KeywordValidate extends Validate
{
    protected $rule = [
        'id|关键词ID' => 'require',
        'website_id|站点ID' => 'require|number',
        'keyword_id|关键词ID' => 'require|array',
        'name|关键词名称' => 'require',
        'url|关键词链接' => 'require|url',
//        'position' => 'require'
        'file|导入文件' => 'require|file|fileExt:xls,xlsx,csv|fileSize:1048576',
        'engine|搜索引擎' => 'require|in:baidupc,baidumobile,haosoupc,sogoumobile',
        'search_engine|搜索引擎' => 'require|in:baidu_pc,baidu_mob,three_pc,sougou_mob',
        'days|天数' => 'require|in:7,30,60'
    ];

    protected $scene = [
        'read' => ['id', 'website_id'],
        'save' => ['website_id','name', 'url','position'],
        'update' => [ 'id','website_id','name','url','position'],
        'delete' => ['keyword_id','website_id'],
        'echarts' => ['id', 'days', 'search_engine'],
        'updateRank' => ['id', 'search_engine'],
        'import' => ['file','website_id'],
        'monitor' => ['website_id,engine'],
    ];

    protected $message = [
        'file.fileExt' => '请上传xls,xlsx,csv格式的文件',
    ];
}