<?php

namespace app\service\upload\storage;

use Exception;
use OSS\OssClient;
use OSS\Core\OssException;
use think\facade\Lang;

class Aliyuncs
{
    public function upload($fileName, $filePath, $param)
    {
        $accessKeyId = $param['accessKeyId'];
        $accessKeySecret = $param['accessKeySecret'];
// Endpoint以杭州为例，其它Region请按实际情况填写。
        $endpoint = $param['storageRoot'];

        // 存储空间名称
        $bucket= $param['bucket'];

        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossResponse = $ossClient->uploadFile($bucket, $fileName, $filePath);
        } catch (\Exception $e) {
            throw new Exception(50055,Lang::get('上传失败'));
        }

        $res = [
            'path' => $ossResponse['info']['url']
        ];
        return $res;
    }
}