<?php


namespace plugins\user\model;


use app\model\Attachment;
use app\model\PluginBaseModel;

class UserGrade extends PluginBaseModel
{
    public function icon(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class,'icon');
    }
}
