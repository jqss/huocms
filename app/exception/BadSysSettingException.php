<?php


namespace app\exception;

class BadSysSettingException extends BaseException
{
    protected $errCode = 500025;

    protected $errMsg = '配置未完成';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }
        parent::__construct($msg,$code);
    }
}