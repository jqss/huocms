<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class AdminValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|管理员ID' => 'require|number',
        'name|名称' => 'require|length:2,80',
        'account|登录账号' => 'require|email|unique:admin',
        'password|密码' => 'length:4,20',
        'phone|手机号' => 'mobile',
        'role|角色' => 'require|array'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['name','account','password','role'],
        'update' => ['id','name','account','role'],
        'selfUpdate' => ['name','password', 'phone'],
    ];
}
