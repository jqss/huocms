<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Job;
use app\model\JobCity;
use app\validate\JobCityValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class JobCityController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(JobCity $jobCity): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];
        $limit = 10;
        $limitParam = (int)input('limit');
        if($limitParam){
            $limit = $limitParam;
        }
        // TODO
        // 添加其他逻辑

        $jobCityList = $jobCity->getJobCityList($where,$limit);
        return json(pageReturn($jobCityList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(JobCity $jobCity): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];
            // 数据验证
            try{
                validate(JobCityValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // TODO
            // 其他逻辑

            $res = $jobCity -> addJobCity($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(JobCity $jobCity): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            // TODO
            // 修改错误消息
            return jsonReturn(-1,'ErrorMsg');
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // TODO
        // 其他逻辑
        $res = $jobCity->getJobCity($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(JobCity $jobCity): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(JobCityValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'is_del' => 1,
            ];
            $res = $jobCity -> updateJobCity($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(JobCity $jobCity, Job $job): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            $seller_id = $this->admin['seller_id'];
            if(!$id){
               // TO DO
               // 替换错误提示
                return jsonReturn(-1,'ErrorMsg');
            }

            // TODO 分类使用判断
            $job_where = [
                'job_city_id' => $id,
                'seller_id' => $seller_id,
            ];

            $has = $job->getJob($job_where);
            if ($has['code'] == 0) {
                return jsonReturn(-3, lang('工作类别在使用中'));
            } else {
                $where = [
                    'id' => $id,
                    'seller_id' => $this->admin['seller_id']
                ];
                $res = $jobCity->softDelJobCity($where);
                return json($res);
            }
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
