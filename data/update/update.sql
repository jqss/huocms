-- 后台菜单
DROP TABLE IF EXISTS `$prefix$admin_menu`;
CREATE TABLE `$prefix$admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '后台菜单ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '菜单类型;1:有界面可访问菜单,2:无界面可访问菜单,3:只作为菜单',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态;1:正常,2:禁用',
  `sort` smallint(6) NOT NULL DEFAULT '10000' COMMENT '排序',
  `controller` varchar(120) NOT NULL DEFAULT '' COMMENT '控制器名',
  `action` varchar(120) NOT NULL DEFAULT '' COMMENT '操作名称',
  `param` varchar(120) NOT NULL DEFAULT '' COMMENT '额外参数',
  `path` varchar(255) DEFAULT '' COMMENT '访问地址',
  `title` varchar(120) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(120) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `is_menu` tinyint(3) unsigned DEFAULT '1',
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '删除 1 正常 2 删除',
  `delete_time` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `controller` (`controller`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 COMMENT='后台菜单表';

-- ----------------------------
-- Records of $prefix$admin_menu
-- ----------------------------
BEGIN;
INSERT INTO `$prefix$admin_menu` VALUES (1, 1, 0, 1, 1, 1, '', '', '', '', '系统管理', '', '', 1, 1, 0, 1633486619, 1633486619);
INSERT INTO `$prefix$admin_menu` VALUES (2, 1, 1, 2, 1, 11, 'SysSetting', 'dashbord', '', '/SysSetting/dashbord', '控制面板', 'icon-kongzhimianban', '', 1, 1, 0, 1633486674, 1633486674);
INSERT INTO `$prefix$admin_menu` VALUES (3, 1, 1, 2, 1, 12, 'SysSetting', 'index', '', '/SysSetting/index', '系统设置', 'icon-xitongshezhi', '', 1, 1, 0, 1633486794, 1633486794);
INSERT INTO `$prefix$admin_menu` VALUES (4, 1, 3, 1, 1, 31, 'SysSetting', 'update', '', '/SysSetting/update', '系统邮件设置保存', '', '', 2, 1, 0, 1633486902, 1633486902);
INSERT INTO `$prefix$admin_menu` VALUES (5, 1, 3, 1, 1, 32, 'SysSetting', 'update', '', '/SysSetting/update', '系统上传设置保存', '', '', 2, 1, 0, 1633486980, 1633486980);
INSERT INTO `$prefix$admin_menu` VALUES (6, 1, 3, 1, 1, 33, 'SysSetting', 'update', '', '/SysSetting/update', '系统统计分析设置保存', '', '', 2, 1, 0, 1633487008, 1633487008);
INSERT INTO `$prefix$admin_menu` VALUES (7, 1, 1, 1, 1, 13, '', '', '', '', '管理员权限', 'icon-guanliyuanquanxian', '', 1, 1, 0, 1633487076, 1633487076);
INSERT INTO `$prefix$admin_menu` VALUES (8, 1, 7, 2, 1, 71, 'Admin', 'index', '', '/Admin/index', '管理员管理', '', '', 1, 1, 0, 1633487110, 1633487110);
INSERT INTO `$prefix$admin_menu` VALUES (9, 1, 8, 1, 1, 81, 'Admin', 'save', '', '/Admin/save', '管理员添加', '', '', 2, 1, 0, 1633487146, 1633487146);
INSERT INTO `$prefix$admin_menu` VALUES (10, 1, 8, 1, 1, 82, 'Admin', 'update', '', '/Admin/update', '管理员编辑', '', '', 2, 1, 0, 1633487161, 1633487161);
INSERT INTO `$prefix$admin_menu` VALUES (11, 1, 8, 1, 1, 83, 'Admin', 'delete', '', '/Admin/delete', '管理员删除', '', '', 2, 1, 0, 1633487171, 1633487171);
INSERT INTO `$prefix$admin_menu` VALUES (12, 1, 7, 2, 1, 72, 'Role', 'index', '', '/Role/index', '角色管理', '', '', 1, 1, 0, 1633487327, 1633487327);
INSERT INTO `$prefix$admin_menu` VALUES (13, 1, 12, 1, 1, 121, 'Role', 'save', '', '/Role/save', '角色添加', '', '', 2, 1, 0, 1633487354, 1633487354);
INSERT INTO `$prefix$admin_menu` VALUES (14, 1, 12, 1, 1, 122, 'Role', 'update', '', '/Role/update', '角色编辑', '', '', 2, 1, 0, 1633487373, 1633487373);
INSERT INTO `$prefix$admin_menu` VALUES (15, 1, 12, 1, 1, 123, 'Role', 'delete', '', '/Role/delete', '角色删除', '', '', 2, 1, 0, 1633487391, 1633487391);
INSERT INTO `$prefix$admin_menu` VALUES (16, 1, 8, 1, 1, 84, 'Admin', 'read', '', '/Admin/read', '管理员详情', '', '', 2, 1, 0, 1633487423, 1633487423);
INSERT INTO `$prefix$admin_menu` VALUES (17, 1, 1, 2, 1, 14, 'Attachment', 'index', '', '/Attachment/index', '附件管理', 'icon-fujianguanli', '', 1, 1, 0, 1633487558, 1633487558);
INSERT INTO `$prefix$admin_menu` VALUES (18, 1, 1, 1, 1, 171, 'Attachment', 'save', '', '/Attachment/save', '附件添加', '', '', 2, 1, 0, 1633487586, 1633487586);
INSERT INTO `$prefix$admin_menu` VALUES (19, 1, 1, 1, 1, 172, 'Attachment', 'delete', '', '/Attachment/delete', '附件删除', '', '', 2, 1, 0, 1633487603, 1633487603);
INSERT INTO `$prefix$admin_menu` VALUES (20, 1, 1, 1, 1, 15, '', '', '', '//', '日志管理', 'icon-rizhiguanli', '', 1, 1, 0, 1633487634, 1633487634);
INSERT INTO `$prefix$admin_menu` VALUES (21, 1, 20, 2, 1, 201, 'AdminLoginLog', 'index', '', '/AdminLoginLog/index', '登录日志', '', '', 1, 1, 0, 1633487666, 1633487666);
INSERT INTO `$prefix$admin_menu` VALUES (22, 1, 20, 2, 1, 202, 'AdminOptLog', 'index', '', '/AdminOptLog/index', '操作日志', '', '', 1, 1, 0, 1633487695, 1633487695);
INSERT INTO `$prefix$admin_menu` VALUES (23, 1, 1, 2, 1, 16, 'RecycleBin', 'index', '', '/RecycleBin/index', '回收站管理', 'icon-huishouzhanguanli', '', 1, 1, 0, 1633487783, 1633487783);
INSERT INTO `$prefix$admin_menu` VALUES (24, 1, 23, 1, 1, 231, 'RecycleBin', 'clean', '', '/RecycleBin/clean', '回收站清空', '', '', 2, 1, 0, 1633487824, 1633487824);
INSERT INTO `$prefix$admin_menu` VALUES (25, 1, 23, 1, 1, 232, 'RecycleBin', 'delete', '', '/RecycleBin/delete', '回收站删除', '', '', 2, 1, 0, 1633487841, 1633487841);
INSERT INTO `$prefix$admin_menu` VALUES (26, 1, 23, 1, 1, 233, 'RecycleBin', 'restore', '', '/RecycleBin/restore', '单条数据恢复', '', '', 2, 1, 0, 1633487920, 1633487920);
INSERT INTO `$prefix$admin_menu` VALUES (27, 1, 23, 1, 1, 234, 'RecycleBin', 'allRestore', '', '/RecycleBin/allRestore', '多条数据恢复', '', '', 2, 1, 0, 1633487940, 1633487940);
INSERT INTO `$prefix$admin_menu` VALUES (28, 1, 1, 2, 1, 17, 'Database', 'index', '', '/Database/index', '数据库备份', 'icon-shujukubeifen', '', 1, 1, 0, 1633487973, 1633487973);
INSERT INTO `$prefix$admin_menu` VALUES (29, 1, 28, 1, 1, 281, 'Database', 'backup', '', '/Database/backup', '备份数据库', '', '', 2, 1, 0, 1633488085, 1633488085);
INSERT INTO `$prefix$admin_menu` VALUES (30, 1, 28, 1, 1, 282, 'Database', 'restore', '', '/Database/restore', '数据库还原', '', '', 2, 1, 0, 1633488174, 1633488174);
INSERT INTO `$prefix$admin_menu` VALUES (31, 1, 28, 1, 1, 283, 'Database', 'delete', '', '/Database/delete', '备份文件删除', '', '', 2, 1, 0, 1633488191, 1633488191);
INSERT INTO `$prefix$admin_menu` VALUES (32, 1, 0, 1, 1, 2, '', '', '', '//', '网站管理', '', '', 1, 1, 0, 1633488240, 1633488240);
INSERT INTO `$prefix$admin_menu` VALUES (33, 1, 32, 2, 1, 321, 'Website', 'index', '', '/Website/index', '网站列表', 'icon-zhandianliebiao', '', 1, 1, 0, 1633488270, 1633488270);
INSERT INTO `$prefix$admin_menu` VALUES (34, 1, 33, 1, 1, 331, 'Website', 'save', '', '/Website/save', '网站添加', '', '', 2, 1, 0, 1633488296, 1633488296);
INSERT INTO `$prefix$admin_menu` VALUES (35, 1, 33, 1, 1, 332, 'Website', 'update', '', '/Website/update', '网站编辑', '', '', 2, 1, 0, 1633488317, 1633488317);
INSERT INTO `$prefix$admin_menu` VALUES (36, 1, 33, 1, 1, 333, 'Website', 'delete', '', '/Website/delete', '网站删除', '', '', 2, 1, 0, 1633488416, 1633488416);
INSERT INTO `$prefix$admin_menu` VALUES (37, 1, 32, 2, 1, 322, 'WebsiteSetting', 'index', '', '/WebsiteSetting/index', '网站基本设置', 'icon-wangzhanjibenshezhi', '', 1, 1, 0, 1633488480, 1633488480);
INSERT INTO `$prefix$admin_menu` VALUES (38, 1, 37, 1, 1, 371, 'WebsiteSetting', 'update', '', '/WebsiteSetting/update', '基本设置保存', '', '', 2, 1, 0, 1633488542, 1633488542);
INSERT INTO `$prefix$admin_menu` VALUES (39, 1, 37, 1, 1, 372, 'WebsiteLang', 'update', '', '/WebsiteLang/update', '语言设置保存', '', '', 2, 1, 0, 1633488558, 1633488558);
INSERT INTO `$prefix$admin_menu` VALUES (40, 1, 37, 1, 1, 373, 'WebsiteLang', 'update', '', '/WebsiteLang/update', '服务器设置保存', '', '', 2, 1, 0, 1633488569, 1633488569);
INSERT INTO `$prefix$admin_menu` VALUES (41, 1, 32, 1, 1, 323, '', '', '', '//', '网站模版管理', 'icon-wangzhanmobanguanli', '', 1, 1, 0, 1633488609, 1633488609);
INSERT INTO `$prefix$admin_menu` VALUES (42, 1, 41, 2, 1, 411, 'TempleteLibrary', 'index', '', '/TempleteLibrary/index', '模版库', '', '', 1, 1, 0, 1633488696, 1633488696);
INSERT INTO `$prefix$admin_menu` VALUES (43, 1, 41, 2, 1, 422, 'Theme', 'index', '', '/Theme/index', '模版管理', '', '', 1, 1, 0, 1633519385, 1633519385);
INSERT INTO `$prefix$admin_menu` VALUES (44, 1, 43, 1, 2, 431, 'Theme', 'save', '', '/Theme/save', '模版下载', '', '', 2, 1, 0, 1633519412, 1633519412);
INSERT INTO `$prefix$admin_menu` VALUES (45, 1, 43, 1, 1, 432, 'Theme', 'update', '', '/Theme/update', '模版更新', '', '', 2, 1, 0, 1633519424, 1633519424);
INSERT INTO `$prefix$admin_menu` VALUES (46, 1, 43, 1, 1, 432, 'Theme', 'delete', '', '/Theme/delete', '模版删除', '', '', 2, 1, 0, 1633519439, 1633519439);
INSERT INTO `$prefix$admin_menu` VALUES (47, 1, 43, 1, 1, 433, 'Theme', 'install', '', '/Theme/install', '系统模版', '', '', 2, 1, 0, 1633519520, 1633519520);
INSERT INTO `$prefix$admin_menu` VALUES (48, 1, 43, 1, 1, 434, 'Theme', 'installTheme', '', '/Theme/installTheme', '模版安装', '', '', 2, 1, 0, 1633519564, 1633519564);
INSERT INTO `$prefix$admin_menu` VALUES (49, 1, 43, 1, 1, 435, 'Theme', 'active', '', '/Theme/active', '模版启用', '', '', 2, 1, 0, 1633519627, 1633519627);
INSERT INTO `$prefix$admin_menu` VALUES (50, 1, 43, 1, 1, 436, 'Theme', 'uninstall', '', '/Theme/uninstall', '模版卸载', '', '', 2, 1, 0, 1633519672, 1633519672);
INSERT INTO `$prefix$admin_menu` VALUES (51, 1, 43, 1, 1, 437, 'Theme', 'allFilePath', '', '/Theme/allFilePath', '模版文件目录', '', '', 2, 1, 0, 1633520228, 1633520228);
INSERT INTO `$prefix$admin_menu` VALUES (52, 1, 43, 1, 1, 438, 'Theme', 'allFiles', '', '/Theme/allFiles', '所有文件', '', '', 2, 1, 0, 1633520293, 1633520293);
INSERT INTO `$prefix$admin_menu` VALUES (53, 1, 43, 1, 1, 439, 'Theme', 'tmpFile', '', '/Theme/tmpFile', '模版文件', '', '', 2, 1, 0, 1633520353, 1633520353);
INSERT INTO `$prefix$admin_menu` VALUES (54, 1, 53, 1, 1, 531, 'ThemeFile', 'read', '', '/ThemeFile/read', '模版文件源码查看', '', '', 2, 1, 0, 1633520526, 1633520526);
INSERT INTO `$prefix$admin_menu` VALUES (55, 1, 53, 1, 1, 532, 'ThemeFile', 'update', '', '/ThemeFile/update', '模版文件源码更新', '', '', 2, 1, 0, 1633520662, 1633520662);
INSERT INTO `$prefix$admin_menu` VALUES (56, 1, 32, 1, 1, 324, '', '', '', '//', '内容管理', 'icon-neirongguanli', '', 1, 1, 0, 1633521143, 1633521143);
INSERT INTO `$prefix$admin_menu` VALUES (57, 1, 56, 2, 1, 561, 'Module', 'index', '', '/Module/index', '模型管理', '', '', 1, 1, 0, 1633521177, 1633521177);
INSERT INTO `$prefix$admin_menu` VALUES (58, 1, 57, 1, 1, 571, 'Module', 'save', '', '/Module/save', '模型添加', '', '', 2, 1, 0, 1633521229, 1633521229);
INSERT INTO `$prefix$admin_menu` VALUES (59, 1, 57, 1, 1, 572, 'Module', 'update', '', '/Module/update', '模型编辑', '', '', 2, 1, 0, 1633521245, 1633521245);
INSERT INTO `$prefix$admin_menu` VALUES (60, 1, 57, 1, 1, 573, 'Module', 'delete', '', '/Module/delete', '模型删除', '', '', 2, 1, 0, 1633521264, 1633521264);
INSERT INTO `$prefix$admin_menu` VALUES (61, 1, 57, 1, 1, 574, 'Module', 'read', '', '/Module/read', '模型详情', '', '', 2, 1, 0, 1633521275, 1633521275);
INSERT INTO `$prefix$admin_menu` VALUES (62, 1, 56, 1, 1, 562, 'ModuleField', 'index', '', '/ModuleField/index', '模型字段', '', '', 2, 1, 0, 1633525422, 1633525422);
INSERT INTO `$prefix$admin_menu` VALUES (63, 1, 62, 1, 1, 621, 'ModuleField', 'save', '', '/ModuleField/save', '模型字段添加', '', '', 2, 1, 0, 1633525442, 1633525442);
INSERT INTO `$prefix$admin_menu` VALUES (64, 1, 62, 1, 1, 622, 'ModuleField', 'update', '', '/ModuleField/update', '模型字段编辑', '', '', 2, 1, 0, 1633525464, 1633525464);
INSERT INTO `$prefix$admin_menu` VALUES (65, 1, 62, 1, 1, 623, 'ModuleField', 'delete', '', '/ModuleField/delete', '模型字段删除', '', '', 2, 1, 0, 1633525481, 1633525481);
INSERT INTO `$prefix$admin_menu` VALUES (66, 1, 62, 1, 1, 624, 'ModuleField', 'read', '', '/ModuleField/read', '模型字段详情', '', '', 2, 1, 0, 1633525499, 1633525499);
INSERT INTO `$prefix$admin_menu` VALUES (67, 1, 56, 2, 1, 563, 'Category', 'index', '', '/Category/index', '栏目管理', '', '', 1, 1, 0, 1633525698, 1633525698);
INSERT INTO `$prefix$admin_menu` VALUES (68, 1, 67, 1, 1, 671, 'Category', 'save', '', '/Category/save', '栏目添加', '', '', 2, 1, 0, 1633525723, 1633525723);
INSERT INTO `$prefix$admin_menu` VALUES (69, 1, 67, 1, 1, 672, 'Category', 'update', '', '/Category/update', '栏目编辑', '', '', 2, 1, 0, 1633525736, 1633525736);
INSERT INTO `$prefix$admin_menu` VALUES (70, 1, 67, 1, 1, 673, 'Category', 'delete', '', '/Category/delete', '栏目删除', '', '', 2, 1, 0, 1633525786, 1633525786);
INSERT INTO `$prefix$admin_menu` VALUES (71, 1, 133, 1, 1, 1331, 'Content', 'save', '', '/Content/save', '内容新增', '', '', 2, 1, 0, 1633526383, 1633526383);
INSERT INTO `$prefix$admin_menu` VALUES (72, 1, 133, 1, 1, 1332, 'Content', 'update', '', '/Content/update', '内容编辑', '', '', 2, 1, 0, 1633526396, 1633526396);
INSERT INTO `$prefix$admin_menu` VALUES (73, 1, 133, 1, 1, 1333, 'Content', 'delete', '', '/Content/delete', '内容删除', '', '', 2, 1, 0, 1633526428, 1633526428);
INSERT INTO `$prefix$admin_menu` VALUES (74, 1, 56, 2, 1, 565, 'Tag', 'index', '', '/Tag/index', '标签管理', '', '', 1, 1, 0, 1633526488, 1633526488);
INSERT INTO `$prefix$admin_menu` VALUES (75, 1, 74, 1, 1, 741, 'Tag', 'save', '', '/Tag/save', '标签添加', '', '', 2, 1, 0, 1633526511, 1633526511);
INSERT INTO `$prefix$admin_menu` VALUES (76, 1, 74, 1, 1, 742, 'Tag', 'update', '', '/Tag/update', '标签编辑', '', '', 2, 1, 0, 1633526537, 1633526537);
INSERT INTO `$prefix$admin_menu` VALUES (77, 1, 74, 1, 1, 743, 'Tag', 'delete', '', '/Tag/delete', '标签删除', '', '', 2, 1, 0, 1633526544, 1633526544);
INSERT INTO `$prefix$admin_menu` VALUES (78, 1, 32, 1, 1, 326, 'Nav', 'index', '', '/Nav/index', '菜单列表', '', '', 2, 1, 0, 1633526626, 1633526626);
INSERT INTO `$prefix$admin_menu` VALUES (79, 1, 78, 1, 1, 781, 'Nav', 'save', '', '/Nav/save', '菜单添加', '', '', 2, 1, 0, 1633526648, 1633526648);
INSERT INTO `$prefix$admin_menu` VALUES (80, 1, 78, 1, 1, 782, 'Nav', 'update', '', '/Nav/update', '菜单编辑', '', '', 2, 1, 0, 1633526661, 1633526661);
INSERT INTO `$prefix$admin_menu` VALUES (81, 1, 78, 1, 1, 783, 'Nav', 'delete', '', '/Nav/delete', '菜单删除', '', '', 2, 1, 0, 1633526675, 1633526675);
INSERT INTO `$prefix$admin_menu` VALUES (82, 1, 32, 2, 1, 325, 'NavCate', 'index', '', '/NavCate/index', '前台菜单管理', 'icon-qiantaicaidanguanli', '', 1, 1, 0, 1633526770, 1633526770);
INSERT INTO `$prefix$admin_menu` VALUES (83, 1, 82, 1, 1, 821, 'NavCate', 'save', '', '/NavCate/save', '分类添加', '', '', 2, 1, 0, 1633526884, 1633526884);
INSERT INTO `$prefix$admin_menu` VALUES (84, 1, 82, 1, 1, 822, 'NavCate', 'update', '', '/NavCate/update', '分类编辑', '', '', 2, 1, 0, 1633526890, 1633526890);
INSERT INTO `$prefix$admin_menu` VALUES (85, 1, 82, 1, 1, 823, 'NavCate', 'delete', '', '/NavCate/delete', '分类删除', '', '', 2, 1, 0, 1633526900, 1633526900);
INSERT INTO `$prefix$admin_menu` VALUES (86, 1, 0, 1, 1, 3, '', '', '', '//', '询盘管理', '', '', 1, 1, 0, 1633527118, 1633527118);
INSERT INTO `$prefix$admin_menu` VALUES (87, 1, 86, 2, 1, 861, 'Inquiry', 'index', '', '/Inquiry/index', '询盘列表', 'icon-xunpanliebiao', '', 1, 1, 0, 1633527207, 1633527207);
INSERT INTO `$prefix$admin_menu` VALUES (88, 1, 87, 1, 1, 871, 'Inquiry', 'update', '', '/Inquiry/update', '询盘更新', '', '', 2, 1, 0, 1633527303, 1633527303);
INSERT INTO `$prefix$admin_menu` VALUES (89, 1, 87, 1, 1, 872, 'Inquiry', 'delete', '', '/Inquiry/delete', '询盘删除', '', '', 2, 1, 0, 1633527326, 1633527326);
INSERT INTO `$prefix$admin_menu` VALUES (90, 1, 86, 2, 1, 862, 'InquiryCate', 'index', '', '/InquiryCate/index', '询盘分类', 'icon-xunpanfenlei', '', 1, 1, 0, 1633527908, 1633527908);
INSERT INTO `$prefix$admin_menu` VALUES (91, 1, 90, 1, 1, 901, 'InquiryCate', 'save', '', '/InquiryCate/save', '询盘分类添加', '', '', 2, 1, 0, 1633527954, 1633527954);
INSERT INTO `$prefix$admin_menu` VALUES (92, 1, 90, 1, 1, 902, 'InquiryCate', 'update', '', '/InquiryCate/update', '询盘分类编辑', '', '', 2, 1, 0, 1633528000, 1633528000);
INSERT INTO `$prefix$admin_menu` VALUES (93, 1, 90, 1, 1, 903, 'InquiryCate', 'delete', '', '/InquiryCate/delete', '询盘分类删除', '', '', 2, 1, 0, 1633528023, 1633528023);
INSERT INTO `$prefix$admin_menu` VALUES (94, 1, 86, 2, 1, 863, 'InquiryEmail', 'index', '', '/InquiryEmail/index', '询盘收件箱', 'icon-xunpanshoujianxiang', '', 1, 1, 0, 1633528118, 1633528118);
INSERT INTO `$prefix$admin_menu` VALUES (95, 1, 94, 1, 1, 941, 'InquiryEmail', 'save', '', '/InquiryEmail/save', '询盘收件箱添加', '', '', 2, 1, 0, 1633528143, 1633528143);
INSERT INTO `$prefix$admin_menu` VALUES (96, 1, 94, 1, 1, 942, 'InquiryEmail', 'update', '', '/InquiryEmail/update', '询盘收件箱编辑', '', '', 2, 1, 0, 1633528165, 1633528165);
INSERT INTO `$prefix$admin_menu` VALUES (97, 1, 94, 1, 1, 943, 'InquiryEmail', 'delete', '', '/InquiryEmail/delete', '询盘收件箱删除', '', '', 2, 1, 0, 1633528173, 1633528173);
INSERT INTO `$prefix$admin_menu` VALUES (98, 1, 0, 1, 1, 4, '', '', '', '', '营销管理', '', '', 1, 1, 0, 1633528319, 1633528319);
INSERT INTO `$prefix$admin_menu` VALUES (99, 1, 98, 1, 1, 981, '', '', '', '', 'SEO', 'icon-SEO', '', 1, 1, 0, 1633528488, 1633528488);
INSERT INTO `$prefix$admin_menu` VALUES (100, 1, 99, 2, 1, 991, 'Keyword', 'index', '', '/Keyword/index', '关键词管理', '', '', 1, 1, 0, 1633528599, 1633528599);
INSERT INTO `$prefix$admin_menu` VALUES (101, 1, 100, 1, 1, 1001, 'Keyword', 'save', '', '/Keyword/save', '关键词添加', '', '', 2, 1, 0, 1633528624, 1633528624);
INSERT INTO `$prefix$admin_menu` VALUES (102, 1, 100, 1, 1, 1002, 'Keyword', 'update', '', '/Keyword/update', '关键词编辑', '', '', 2, 1, 0, 1633528636, 1633528636);
INSERT INTO `$prefix$admin_menu` VALUES (103, 1, 100, 1, 1, 1003, 'Keyword', 'delete', '', '/Keyword/delete', '关键词删除', '', '', 2, 1, 0, 1633528648, 1633528648);
INSERT INTO `$prefix$admin_menu` VALUES (104, 1, 99, 2, 1, 992, 'InnerChart', 'index', '', '/InnerChart/index', '内链管理', '', '', 1, 1, 0, 1633528703, 1633528703);
INSERT INTO `$prefix$admin_menu` VALUES (105, 1, 104, 1, 1, 1041, 'InnerChart', 'save', '', '/InnerChart/save', '内链添加', '', '', 2, 1, 0, 1633528729, 1633528729);
INSERT INTO `$prefix$admin_menu` VALUES (106, 1, 104, 1, 1, 1042, 'InnerChart', 'update', '', '/InnerChart/update', '内链编辑', '', '', 2, 1, 0, 1633528747, 1633528747);
INSERT INTO `$prefix$admin_menu` VALUES (107, 1, 104, 1, 1, 1043, 'InnerChart', 'delete', '', '/InnerChart/delete', '内链删除', '', '', 2, 1, 0, 1633528759, 1633528759);
INSERT INTO `$prefix$admin_menu` VALUES (108, 1, 132, 1, 1, 1321, 'Advertisement', 'save', '', '/Advertisement/save', '广告添加', '', '', 2, 1, 0, 1633528838, 1633528838);
INSERT INTO `$prefix$admin_menu` VALUES (109, 1, 132, 1, 1, 1322, 'Advertisement', 'update', '', '/Advertisement/update', '广告编辑', '', '', 2, 1, 0, 1633528863, 1633528863);
INSERT INTO `$prefix$admin_menu` VALUES (110, 1, 132, 1, 1, 1323, 'Advertisement', 'delete', '', '/Advertisement/delete', '广告删除', '', '', 2, 1, 0, 1633528914, 1633528914);
INSERT INTO `$prefix$admin_menu` VALUES (111, 1, 99, 2, 1, 994, 'Link', 'index', '', '/Link/index', '友情链接管理', '', '', 1, 1, 0, 1633528988, 1633528988);
INSERT INTO `$prefix$admin_menu` VALUES (112, 1, 111, 1, 1, 1111, 'Link', 'save', '', '/Link/save', '友情链接添加', '', '', 2, 1, 0, 1633529087, 1633529087);
INSERT INTO `$prefix$admin_menu` VALUES (113, 1, 111, 1, 1, 1112, 'Link', 'update', '', '/Link/update', '友情链接编辑', '', '', 2, 1, 0, 1633529099, 1633529099);
INSERT INTO `$prefix$admin_menu` VALUES (114, 1, 111, 1, 1, 1113, 'Link', 'delete', '', '/Link/delete', '友情链接删除', '', '', 2, 1, 0, 1633529111, 1633529111);
INSERT INTO `$prefix$admin_menu` VALUES (115, 1, 111, 1, 1, 1114, 'Link', 'copy', '', '/Link/copy', '复制其他站点友链', '', '', 2, 1, 0, 1633529250, 1633529250);
INSERT INTO `$prefix$admin_menu` VALUES (116, 1, 98, 1, 1, 982, '', '', '', '//', '统计分析', 'icon-shujufenxi', '', 1, 1, 0, 1633529747, 1633529747);
INSERT INTO `$prefix$admin_menu` VALUES (117, 1, 116, 2, 1, 1161, 'SEM', 'baidu', '', '/SEM/baidu', '百度', '', '', 1, 1, 0, 1633529780, 1633529780);
INSERT INTO `$prefix$admin_menu` VALUES (118, 1, 116, 2, 1, 1161, 'SEM', 'google', '', '/SEM/google', '谷歌', '', '', 1, 1, 0, 1633529814, 1633529814);
INSERT INTO `$prefix$admin_menu` VALUES (119, 1, 0, 1, 1, 5, '', '', '', '//', '招聘管理', '', '', 1, 1, 0, 1633529846, 1633529846);
INSERT INTO `$prefix$admin_menu` VALUES (120, 1, 119, 2, 1, 1191, 'Job', 'index', '', '/Job/index', '岗位管理', 'icon-zhiweiguanli', '', 1, 1, 0, 1633529880, 1633529880);
INSERT INTO `$prefix$admin_menu` VALUES (121, 1, 120, 1, 1, 1201, 'Job', 'save', '', '/Job/save', '岗位添加', '', '', 2, 1, 0, 1633529922, 1633529922);
INSERT INTO `$prefix$admin_menu` VALUES (122, 1, 120, 1, 1, 1202, 'Job', 'update', '', '/Job/update', '岗位编辑', '', '', 2, 1, 0, 1633529940, 1633529940);
INSERT INTO `$prefix$admin_menu` VALUES (123, 1, 120, 1, 1, 1203, 'Job', 'delete', '', '/Job/delete', '岗位删除', '', '', 2, 1, 0, 1633529954, 1633529954);
INSERT INTO `$prefix$admin_menu` VALUES (124, 1, 119, 2, 1, 1192, 'JobCate', 'index', '', '/JobCate/index', '岗位分类', 'icon-gangweifenlei', '', 1, 1, 0, 1633530098, 1633530098);
INSERT INTO `$prefix$admin_menu` VALUES (125, 1, 124, 1, 1, 1241, 'JobCate', 'save', '', '/JobCate/save', '岗位分类添加', '', '', 2, 1, 0, 1633530122, 1633530122);
INSERT INTO `$prefix$admin_menu` VALUES (126, 1, 124, 1, 1, 1242, 'JobCate', 'update', '', '/JobCate/update', '岗位分类编辑', '', '', 2, 1, 0, 1633530134, 1633530134);
INSERT INTO `$prefix$admin_menu` VALUES (127, 1, 124, 1, 1, 1243, 'JobCate', 'update', '', '/JobCate/update', '岗位分类删除', '', '', 2, 1, 0, 1633530143, 1633530143);
INSERT INTO `$prefix$admin_menu` VALUES (128, 1, 119, 2, 1, 1193, 'JobCity', 'index', '', '/JobCity/index', '城市管理', 'icon-chengshiguanli', '', 1, 1, 0, 1633530203, 1633530203);
INSERT INTO `$prefix$admin_menu` VALUES (129, 1, 128, 1, 1, 1281, 'JobCity', 'save', '', '/JobCity/save', '城市添加', '', '', 2, 1, 0, 1633530231, 1633530231);
INSERT INTO `$prefix$admin_menu` VALUES (130, 1, 128, 1, 1, 1282, 'JobCity', 'update', '', '/JobCity/update', '城市编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (131, 1, 128, 1, 1, 1283, 'JobCity', 'delete', '', '/JobCity/delete', '城市删除', '', '', 2, 1, 0, 1633530261, 1633530261);
INSERT INTO `$prefix$admin_menu` VALUES (132, 1, 99, 2, 1, 993, 'Advertisement', 'index', '', '/Advertisement/index', '广告管理', '', '', 1, 1, 0, 1633531310, 1633531310);
INSERT INTO `$prefix$admin_menu` VALUES (133, 1, 56, 2, 1, 564, 'Content', 'index', '', '/Content/index', '栏目内容管理', '', '', 1, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` VALUES (134, 1, 32, 1, 1, 565, 'SlideCate', 'index', '', '/SlideCate/index', '幻灯片管理', 'icon-huandengpianshezhi', '', 1, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` VALUES (135, 1, 32, 2, 1, 566, 'Slide', 'index', '', '/Slide/index', '幻灯片列表', '', '', 2, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` VALUES (136, 1, 134, 1, 1, 1341, 'SlideCate', 'save', '', '/Slide/save', '幻灯片添加', '', '', 2, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` VALUES (137, 1, 134, 1, 1, 1342, 'SlideCate', 'update', '', '/SlideCate/update', '分类编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (138, 1, 134, 1, 1, 1343, 'SlideCate', 'read', '', '/SlideCate/read', '分类详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (139, 1, 134, 1, 1, 1344, 'SlideCate', 'delete', '', '/SlideCate/delete', '分类删除', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (140, 1, 135, 1, 1, 1351, 'Slide', 'save', '', '/Slide/save', '幻灯片保存', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (141, 1, 135, 1, 1, 1352, 'Slide', 'update', '', '/Slide/update', '幻灯片', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (142, 1, 135, 1, 1, 1353, 'Slide', 'read', '', '/Slide/read', '幻灯片详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (143, 1, 135, 1, 1, 1354, 'Slide', 'delete', '', '/Slide/delete', '幻灯片删除', '', '', 2, 1, 0, 1633530242, 1633530242);
COMMIT;
-- 后台菜单

-- 幻灯片表
CREATE TABLE IF NOT EXISTS `$prefix$slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '幻灯片ID',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `slide_cate_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `lang` varchar(255) DEFAULT 'zh',
  `title` varchar(120) DEFAULT NULL COMMENT '幻灯片标题',
  `sub_title` varchar(120) DEFAULT NULL COMMENT '幻灯片副标题',
  `url` varchar(120) DEFAULT NULL COMMENT '幻灯片地址',
  `attachment` int(11) DEFAULT NULL COMMENT '幻灯片图片',
  `description` varchar(255) DEFAULT NULL COMMENT '分类简介',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='幻灯片列表';

-- ----------------------------
-- Records of $prefix$slide
-- ----------------------------
BEGIN;
COMMIT;


-- ----------------------------
-- Table structure for $prefix$slide_cate
-- ----------------------------
CREATE TABLE IF NOT EXISTS `$prefix$slide_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '幻灯片分类',
  `seller_id` int(11) NOT NULL DEFAULT '1' COMMENT '商户ID',
  `website_id` int(11) DEFAULT NULL COMMENT '网站ID',
  `lang` varchar(255) DEFAULT 'zh',
  `title` varchar(120) DEFAULT NULL COMMENT '分类名称',
  `description` varchar(255) DEFAULT NULL COMMENT '分类简介',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='幻灯片分类';

-- ----------------------------
-- Records of $prefix$slide_cate
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for $prefix$visit_log
-- ----------------------------
CREATE TABLE IF NOT EXISTS `$prefix$visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL DEFAULT '1',
  `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `domain` varchar(255) DEFAULT NULL COMMENT '域名',
  `ip` varchar(255) DEFAULT NULL COMMENT '访问IP',
  `url` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `visited_time` date DEFAULT NULL COMMENT '访问日期',
  `visited_area` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `visited_device` varchar(255) DEFAULT NULL COMMENT '访问设备',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='前台用户访问日志表';

-- ----------------------------
-- Records of $prefix$visit_log
-- ----------------------------
BEGIN;
COMMIT;

-- 主表表增加模版
ALTER TABLE `$prefix$sub_content` ADD `detail_tpl` varchar(255) DEFAULT NULL COMMENT '模版' AFTER `sort`;
ALTER TABLE `$prefix$sub_content` ADD `sub_title` varchar(255) DEFAULT NULL COMMENT '副标题' AFTER `title`;
-- 主表表增加模版

-- 内链表增加标签类型
ALTER TABLE `$prefix$inner_chart` ADD `type` tinyint(2) DEFAULT '1' COMMENT '内链类型 1 站点标签 2 通用标签' AFTER `website_id`;
ALTER TABLE `$prefix$inner_chart` ADD `count` tinyint(3) DEFAULT NULL COMMENT '内链类型 1 站点标签 2 通用标签' AFTER `type`;
-- 内链表增加标签类型

-- 内链文章关联表
CREATE TABLE IF NOT EXISTS `$prefix$inner_chart_sub_content` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
`seller_id` int(10) unsigned DEFAULT NULL COMMENT '商户ID',
`inner_chart_id` int(10) unsigned DEFAULT NULL COMMENT '内链 id',
`sub_content_id` int(10) unsigned DEFAULT NULL COMMENT '内容 id',
`total` tinyint(3) DEFAULT NULL COMMENT '内容内链数量',
PRIMARY KEY (`id`) USING BTREE,
KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章内链关联表';
-- 内链文章关联表


-- 栏目表增加单页面内容字段
ALTER TABLE `$prefix$category` ADD `sub_title` text COMMENT '副标题' AFTER `title`;
ALTER TABLE `$prefix$category` ADD `content` text COMMENT '内容' AFTER `sub_title`;
-- 栏目表增加单页面内容字段

-- 附件表增加描述字段
ALTER TABLE `$prefix$attachment` ADD `description` varchar(255) DEFAULT NULL COMMENT '描述' AFTER `name`;
-- 附件表增加描述字段

-- 新增附件栏目表
CREATE TABLE IF NOT EXISTS `$prefix$attachment_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件分类ID',
  `seller_id` int(11) unsigned DEFAULT '1' COMMENT '商户ID',
  `parent_id` int(11)  DEFAULT '0' COMMENT '父栏目ID',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `title` varchar(255) DEFAULT NULL  COMMENT '分类名称',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='附件分类表';
-- 新增附件栏目表

-- 附件表增加分类ID
ALTER TABLE `$prefix$attachment` ADD `attachment_cate_id` int(11) DEFAULT NULL COMMENT '分类ID' AFTER `name`;
-- 附件表增加分类ID

-- 添加后台菜单
INSERT INTO `$prefix$admin_menu` VALUES (144,1, 1, 2, 1, 14, 'AttachmentCate', 'index', '', '/AttachmentCate/index', '附件管理', 'icon-fujianguanli', '', 1, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` VALUES (145,1, 144, 1, 1, 1441, 'AttachmentCate', 'save', '', '/AttachmentCate/save', '附件分类添加', '', '', 2, 1, 0, 1633592819, 1633592819);
INSERT INTO `$prefix$admin_menu` VALUES (146,1, 144, 1, 1, 1442, 'AttachmentCate', 'update', '', '/AttachmentCate/update', '附件分类编辑', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (147,1, 144, 1, 1, 1443, 'AttachmentCate', 'read', '', '/AttachmentCate/read', '附件分类详情', '', '', 2, 1, 0, 1633530242, 1633530242);
INSERT INTO `$prefix$admin_menu` VALUES (148,1, 144, 1, 1, 1444, 'AttachmentCate', 'delete', '', '/AttachmentCate/delete', '附件分类删除', '', '', 2, 1, 0, 1633530242, 1633530242);
UPDATE `$prefix$admin_menu` SET `is_menu` = 2 WHERE `id` = 17;
-- 添加后台菜单

-- 栏目表增加单页面内容字段
ALTER TABLE `$prefix$category` ADD `is_search` tinyint(2) DEFAULT 2 COMMENT '栏目是否搜索' AFTER `status`;
-- 栏目表增加单页面内容字段

-- 模版文件大内容字段表添加模版ID字段
ALTER TABLE `$prefix$big_field` ADD `theme_id` int(11) DEFAULT NULL COMMENT '模版ID' AFTER `seller_id`;
-- 模版文件大内容字段表添加模版ID字段

-- 标签字段添加排序和首字母文件
ALTER TABLE `$prefix$tag` ADD `sort` tinyint(3) DEFAULT NULL COMMENT '排序' AFTER `status`;
ALTER TABLE `$prefix$tag` ADD `first_letter` char(1) DEFAULT NULL COMMENT '排序' AFTER `status`;
-- 标签字段添加排序和首字母文件

-- 访问记录表添加来源站点
ALTER TABLE `$prefix$visit_log` ADD `referrer` varchar (255) DEFAULT NULL COMMENT '来源网址' AFTER `seller_id`;
-- 访问记录表添加来源站点

-- 标签表修改tag_title字段
ALTER TABLE `$prefix$tag` add `unique_tag` varchar (255) DEFAULT NULL COMMENT '标签唯一标识' AFTER `title`;
-- 标签表修改tag_title字段

-- 站点表添加parent_id字段
ALTER TABLE `$prefix$website` add `parent_id` int (11) DEFAULT 0 COMMENT '父站点' AFTER `domain`;
-- 站点表添加parent_id字段

-- 站点表添加parent_id字段
ALTER TABLE `$prefix$category` add `parent_map` int (11) DEFAULT 0 COMMENT '附站点栏目ID映射' AFTER `parent_id`;
ALTER TABLE `$prefix$category` add `has_map` tinyint (2) DEFAULT 2 COMMENT '是否映射父栏目 1 是 2 否' AFTER `parent_map`;
-- 站点表添加parent_id字段

-- 分片上传字段状态修改
UPDATE `$prefix$sys_setting` SET `status` = 1 WHERE `id` = 3;
UPDATE `$prefix$sys_setting` SET `status` = 1 WHERE `id` = 12;
-- 分片上传字段状态修改

-- 站点表添加parent_path字段
ALTER TABLE `$prefix$category` add `parent_path` varchar(255) COMMENT '附站点栏目ID映射' AFTER `parent_map`;
-- 站点表添加parent_path字段

-- 日志表修改agent字段长度
ALTER TABLE `$prefix$admin_opt_log` MODIFY COLUMN `agent` varchar(255);
-- 日志表修改agent字段长度

-- 导航表添加no_follow字段
ALTER TABLE `$prefix$nav` add `no_follow` tinyint (2) DEFAULT 2 COMMENT '是否添加no follow属性 1 是 2 否' AFTER `target`;
-- 导航表添加no_follow字段

-- 配置表添加百度翻译配置
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (99, 1, 78, 'others', '百度翻译', 'baidu_translate', '', '', 1, 784, 0, 0);
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (100, 1, 99, 'others', 'APP ID', 'baidu_translate_appid', '', '', 1, 991, 0, 0);
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (101, 1, 99, 'others', 'Token', 'baidu_translate_token', '', '', 1, 992, 0, 0);
-- 配置表添加百度翻译配置

-- 配置表添加sitemap自动提交
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (102, 1, 78, 'others', 'sitemap密钥', 'sitemap_token', '', '', 1, 785, 0, 0);
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (103, 1, 102, 'others', '百度密钥', 'baidu_sitemap_token', '', '', 1, 1021, 0, 0);
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (104, 1, 102, 'others', '谷歌密钥', 'google_sitemap_token', '', '', 1, 1022, 0, 0);
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (105, 1, 102, 'others', '360密钥', 'three_sitemap_token', '', '', 1, 1023, 0, 0);
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (106, 1, 102, 'others', '搜狗密钥', 'sogou_sitemap_token', '', '', 1, 1024, 0, 0);
-- 配置表添加sitemap自动提交

-- suwork 询盘提交地址
INSERT INTO `$prefix$sys_setting` (`id`,`seller_id`, `parent_id`, `group`, `form_title`, `title`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES (107, 1, 79, 'others', 'Suwork Api', 'suwork_api', '', '', 1, 99, 0, 0);
-- suwork 询盘提交地址

-- 模版文件表添加 real_path 字段
ALTER TABLE `$prefix$theme_file` add `real_path` varchar(255) DEFAULT NULL COMMENT '模版路径' AFTER `file`;
-- 模版文件表添加 real_path 字段

-- 角色站点关联表
CREATE TABLE IF NOT EXISTS `$prefix$role_website` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `seller_id` int(11) DEFAULT NULL COMMENT '商户ID',
    `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
    `website_id` int(11) DEFAULT NULL COMMENT '站点ID',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色站点关联表';

-- 模版文件表添加 real_path 字段
ALTER TABLE `$prefix$role` add `kid_auth` varchar(500) DEFAULT NULL COMMENT '子权限' AFTER `auth`;
-- 模版文件表添加 real_path 字段

-- 图片增加字段
ALTER TABLE `hc_new_close`.`hc_attachment`
    ADD COLUMN `reduce_img` varchar(255) NULL DEFAULT '' COMMENT '缩减图片' AFTER `url`;
-- 图片增加字段