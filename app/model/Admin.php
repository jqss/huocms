<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelEmptyException;
use app\exception\ModelException;

/**
 * @mixin \think\Model
 */
class Admin extends Model
{

    protected $hidden = ['password'];

    public function role(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany('Role','admin_role');
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getAdminList(array $where = [],int $limit = 10):array
    {
        try{
           $res =  $this->where($where)->order('id desc')->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getAdmin(array $where = [],$with = []): array
    {
        try{
            $res =  $this->with($with)->where($where)->find();
            if(empty($res)){
                throw new ModelEmptyException();
            }
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addAdmin($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res);
    }

    /**
     * @param array $where
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function updateAdmin(array $where,$param): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delAdmin($where): array
    {
        try{
            $res =  self::where($where)->update($this->delData);
         }catch(\Exception $e){
             throw new ModelException($e->getMessage());
         }
         return dataReturn($this->sucCode,$this->delMsg,$res);
    }
}
