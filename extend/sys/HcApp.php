<?php


namespace sys;


use think\App;
use think\helper\Str;

class HcApp extends App
{
    /**
     * 当前应用类库命名空间
     * @var string
     */
    protected $namespace = 'app';

    /**
     * 解析应用类的类名
     * @access public
     * @param string $layer 层名 controller model ...
     * @param string $name  类名
     * @return string
     */
    public function parseClass(string $layer, string $name): string
    {
        if (strpos($name, 'plugin') !== 0) {
            return parent::parseClass($layer, $name);
        }

        $namespace = 'plugins';
        $name = substr($name, 7);

        $name  = str_replace(['/', '.'], '\\', $name);
        $array = explode('\\', $name);
        $class = Str::studly(array_pop($array));
        $path  = $array ? implode('\\', $array) . '\\' : '';

        return $namespace . '\\' . $path . $layer . '\\' . $class;
    }
}
