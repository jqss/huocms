<?php


namespace app\model;


class InquiryEmail extends Model
{

    public function website(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(Website::class,'website_inquiry_email');
    }
    /**
     * @param $where
     * @param $limit
     * @return array
     */
    public function getInquiryEmailList($where, $limit,$with=[]): array
    {
        try {
            $res = $this->with($with)->where($where)->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn('-1', $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    public function getInquiryEmail($where): array
    {
        try {
            $res = $this->where($where)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * @param $param
     * @return array
     */

    public function addInquiryEmail($param): array
    {
        try {
            $res = self::create($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->addMsg,$res);
    }

    /**
     * @param $param
     * @return array
     */

    public function updateInquiryEmail($where,$param): array
    {
        try {
            self::where($where)->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->updateMsg);
    }

    /**
     * @param $where
     * @return array
     */

    public function deleteInquiryEmail($where): array
    {
        try {
            self::destroy($where);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->delMsg);
    }
}