<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'make:hc-model' => 'app\command\CreateModel',
        'make:hc-controller' => 'app\command\CreateController',
        'admin-view:link'     => 'app\command\CreateLink',
        'baiduTongji'     => 'app\command\BaiduTongji',
        'seo:check' => 'app\command\WebDiagnosis',
        'seo:check2' => 'app\command\WebDiagnosis2',
        'seo:check3' => 'app\command\WebDiagnosis3',
    ],
];
