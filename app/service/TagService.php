<?php


namespace app\service;


use app\model\Model;
use app\model\Tag;

class TagService
{
    public static function getTagTagList($param)
    {
        if(!empty($param['id'])){
            $ids = explode(',',$param['id']);
            $where[] = ['id','in',$ids];
        }
        $where = [
            ['seller_id','=',$param['seller_id']],
            ['website_id','=',$param['website_id']],
            ['is_del','=',1],
        ];
        $Tag = new Tag();
        if($param['is_page'] == 2){
            $data = $Tag -> getLimitCustomData($where,$param['sort'],$param['limit'],$param['field'],[],$param['offset'])['data'];
        }else{
            $data = $Tag -> getCustomDataList($where,$param['limit'],$param['sort'],$param['field'])['data'];
        }
        return $data;
    }
}
