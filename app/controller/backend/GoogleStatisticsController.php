<?php

namespace app\controller\backend;

use app\service\GoogleRequest;

class GoogleStatisticsController extends \app\BaseController
{
    public function read() {
        $googleStatistics = new GoogleRequest();
        $googleStatistics->start();

    }
}