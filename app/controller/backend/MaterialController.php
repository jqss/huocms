<?php

namespace app\controller\backend;

use app\model\PosterMaterial;
use app\model\SysSetting;
use app\service\upload\Upload;
use think\facade\Lang;

class MaterialController extends BaseController
{
    public function index()
    {
        $posterModel = new PosterMaterial();

        $list = $posterModel->getMaterialList([]);
        foreach ($list['data'] as $v) {
            $v['source'] = str_replace("\\", '/', $v['source']);
        }

        return json($list);
    }

    public function upload()
    {
        if (request()->isPost()) {
            set_time_limit(0);
            $file = request()->file('file');
            if (empty($file)) {
                return jsonReturn(-8, Lang::get('文件上传失败,请重新尝试'));
            }

            $imageSize = getimagesize($file);
            $seller_id = $this->admin['seller_id'];
            // 查看文件类型
            $fileName = $file->getOriginalName();
            $fileExt = $file->getOriginalExtension();
            $file_type = fileFormat($fileName);

            // 附件大小和类型验证
            // 获取上传配置
            $Settings = new SysSetting();
            $uploadSetting = $Settings->getAllCustomArrayData(['parent_id' => 1, 'group' => 'upload', 'status' => 1], 'id desc', 'id,group,title,value')['data'];
            $uploadSetting = getColumnForKeyArray($uploadSetting, 'title');
            $limitSize = $uploadSetting[$file_type . '_size']['value'] * 1024;     // byte
            $fileSize = $file->getSize();       // 单位byte
            if ($fileSize > $limitSize) {
                return jsonReturn(-1, Lang::get('文件过大，请修改上传限制或者替换小的文件'));
            }
            $extArr = explode(',', $uploadSetting[$file_type . '_ext']['value']);
            if (!in_array($fileExt, $extArr)) {
                return jsonReturn(-2, Lang::get('文件格式错误，请重新上传'));
            }

            $type = $this->getUploadType();
            $upload = new Upload();
            $upload->create($file, $seller_id, $type, $file_type);
            $res = $upload->getUploadFileInfo()['data'];
            $res['width'] = $imageSize[0];
            $res['height'] = $imageSize[1];
            $res['type'] = $fileExt;
            if (strpos($res['url'], 'http') === false) {
                $res['url'] = request()->domain() . '/' .  $res['url'];
            }

            return jsonReturn(0, Lang::get('上传成功'), $res);
        }

        return jsonReturn(-3, Lang::get('请求方法错误'));
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $posterModel = new PosterMaterial();
            return json($posterModel->addMaterial($param));
        }
    }

    public function del()
    {
        $id = input('param.id');
        $posterModel = new PosterMaterial();

        return json($posterModel->delMaterial($id));
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function getUploadType()
    {
        // 文件信息提取
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'group' => 'upload',
            'title' => 'storage'
        ];
        $place = new SysSetting();
        return $place->getSysSetting($where)['data']->toArray()['value'];
    }
}
