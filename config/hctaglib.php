<?php

return [
    // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
    'hotcontent' => ['attr'=>'','close'=>1],
    // sitemap 标签
    'sitemap' => ['attr'=> 'type','close'=>0],
    // 返回列表
    'backlist' => ['attr'=> '','close'=>0],
    // 上一篇
    'precontent' => ['attr'=>'','close'=>0],
    // 下一篇
    'nextcontent' => ['attr'=>'','close'=>0],
    // 相关推荐
    'recommend' => ['attr'=>'cid,moduleId','close'=>1],
    // 配置
    'setting' => ['attr=>name,type','close'=>0],
    // 面包屑
    'breadcrumb'       => ['attr' => 'cid', 'close' => 1],
    // 广告
    'advertise' => ['attr'=>'id','close'=>0],
    // 内容
    'contentpage' => ['attr'=>'page,limit,mainField,subField,isTop,isCommend','close'=>1],
    'content' => ['attr'=>'cid,mainField,subField,sort,limit,asPage,isTop,isCommend','close'=>1],
    'tagContent'   => ['attr'],
    'contentDetail' => ['attr'=>'id'],
    // 栏目
    'subcategory'    => ['attr' => 'pid', 'close' => 1],//非必须属性item
    'allsubcategory' => ['attr' => 'pid', 'close' => 1],//非必须属性item
    'categorylist'       => ['attr' => 'ids,where,order', 'close' => 1],//非必须属性item
    'category'         => ['attr' => 'id','close' => 1],//非必须属性item
    // 导航
    'navigation'   => ['attr'=>'nav-id,','close'=>1],
    'navigationmenu'   => ['attr'=>'cid,item','close'=>1],
    'navigationfolder'   => ['attr'=>'cid,item','close'=>1],
    // 导航数据
    'nav' => ['attr'=>'cid'],

    // 友情链接
    'link'  => ['attr'=>'','close'=>1],
    // 幻灯片
    'slide' => ['attr' =>'cid,item','close'=>1]
];