<?php
declare (strict_types = 1);

namespace app\listener;

use app\model\AdminLoginLog;

class AdminLoginListener
{
    /**
     * 事件监听处理
     *
     * @return mixed
     * @throws \app\exception\ModelException
     */
    public function handle($event)
    {
        (new AdminLoginLog())->addAdminLoginLog($event);
    }
}
