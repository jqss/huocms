<?php

namespace app\exception;

class ModelException extends BaseException
{
    protected $errCode = 50001;

    protected $errMsg = '模型方法错误';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }
        parent::__construct($msg,$code);
    }
}