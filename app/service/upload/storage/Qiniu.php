<?php

namespace app\service\upload\storage;
//require 'vendor\autoload.php';

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class Qiniu
{

    /**
     * @var string
     */
    private $accessKey;
    /**
     * @var string
     */
    private $secretKey;

    public function upload($file, $filePath, $param)
    {
        $accessKey = $param['accessKeyId'];
        $secretKey = $param['accessKeySecret'];
        $storageRoot = $param['storageRoot'];
        $auth = new Auth($accessKey, $secretKey);
        $bucket = $param['bucket'];
        // 生成上传Token
        $token = $auth->uploadToken($bucket);
        // 构建 UploadManager 对象
        $uploadMgr = new UploadManager();
        $fileResponse = $uploadMgr->putFile($token, $file, $filePath);
        $result = [
            'name' => $fileResponse[0]['key'],
            'path' => $param['protocolHeader'].$storageRoot.$fileResponse[0]['key'],
        ];

        return $result;

    }
}
