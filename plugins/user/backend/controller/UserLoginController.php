<?php


namespace plugins\user\backend\controller;


use app\controller\backend\BaseController;
use plugins\user\model\UserLogin;

class UserLoginController extends BaseController
{
    public function index()
    {
        $param = $this->request->only([
            'username' => '',
            'limit' => 15,
        ]);

        $where = [];
        if (!empty($param['username'])) {
            $where[] = ['username', 'like', "%{$param['username']}%"];
        }

        $userModel = new UserLogin();
        $list = $userModel->getList($where, ['avatar'], $param['limit'], 'id desc');

        return json(pageReturn($list));
    }

    public function del()
    {
        $param = $this->request->post([
            'id' => 0,
        ]);

        $userModel = new UserLogin();
        $res = $userModel->where(['id' => $param['id']])->delete();
        if ($res === false) {
            return jsonReturn(-1, lang('删除失败'));
        }
        return jsonReturn(0, lang('删除成功'));
    }
}
