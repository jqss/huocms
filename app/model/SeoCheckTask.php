<?php

namespace app\model;

class SeoCheckTask extends Model
{
    /**
     * 获取检测列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getCheckList($where, $limit)
    {
        try {

            $list = $this->where($where)->order('id desc')->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('成功'), $list);
    }

    /**
     * 获取任务信息
     * @param $id
     * @return array
     */
    public function getTaskInfoById($id)
    {
        try {

            $info = $this->where('id', $id)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('成功'), $info);
    }
}