<?php

namespace app\model;

class SubContentCheck extends Model
{
    public function admin(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Admin::class, 'check_user_id')->bind(['name']);
    }
}