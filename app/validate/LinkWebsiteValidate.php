<?php


namespace app\validate;


class LinkWebsiteValidate extends \think\Validate
{
    protected $rule = [
        'id' => 'require',
        'website_id'=> 'require|array',
        'link_id' => 'require|array',
        'name' => 'require',
        'url' => 'require|url',
        'sort' => 'require|integer',
    ];

    protected $message = [
        'id.require' => '主键不能为空',
        'website_id.require' => '网站id不能为空',
        'website_id.array' => '网站id必须为数组',
        'name'=> '友情链接名称不能为空',
        'url.require' => '友情链接地址不能为空',
        'url.url' => '友情链接地址无效',
        'sort.require' => '友情链接排序不能为空',
        'sort.integer' => '友情链接排序必须为整数',
    ];

    protected $scene = [
        'index' => ['seller_id', 'website_id.require'],
        'read' => ['id', 'seller_id',],
        'save' => [ 'website_id', 'name','url', 'sort'],
        'update' => ['website_id.require', 'link_id'],
        'update_sort' => ['id','website_id.require', 'link_id', 'sort'],
        'delete' => ['link_id.require'],
    ];
}