<?php

namespace plugins\api;

use app\Plugin;

class ApiPlugin extends Plugin
{
    public $info = [
        'name'        => 'api',//Demo插件英文名，改成你的插件英文就行了
        'title'       => 'api接口',
        'description' => 'app/小程序 api接口',
        'status'      => 1,
        'author'      => 'huocms',
        'version'     => '1.0.0',
        'demo_url'    => 'http://www.demo_huocms.com',
        'author_url'  => 'http://www.huocms.com'
    ];

    public $hasAdmin = 0;//插件是否有后台管理界面

    // 插件安装
    public function install()
    {
        return true;//安装成功返回true，失败false
    }

    // 插件卸载
    public function uninstall()
    {
        return true;//卸载成功返回true，失败false
    }
}
