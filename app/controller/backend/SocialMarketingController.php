<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\SocialMarketing;
use app\validate\SocialMarketingValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class SocialMarketingController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(SocialMarketing $socialMarketing): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
        ];
        $limit = 10;
        $limitParam = (int)input('limit');
        if($limitParam){
            $limit = $limitParam;
        }
        // TODO
        // 添加其他逻辑

        $socialMarketingList = $socialMarketing->getSocialMarketingList($where,$limit);
        return json(pageReturn($socialMarketingList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(SocialMarketing $socialMarketing): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            // 数据验证
            try{
                validate(SocialMarketingValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // TODO
            // 其他逻辑

            $res = $socialMarketing -> addSocialMarketing($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(SocialMarketing $socialMarketing): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            // TODO
            // 修改错误消息
            return jsonReturn(-1,'ErrorMsg');
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // TODO
        // 其他逻辑
        $res = $socialMarketing->getSocialMarketing($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(SocialMarketing $socialMarketing): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(SocialMarketingValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $socialMarketing -> updateSocialMarketing($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(SocialMarketing $socialMarketing): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
               // TO DO
               // 替换错误提示
                return jsonReturn(-1,'ErrorMsg');
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $socialMarketing->delSocialMarketing($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
